<?php

namespace gan4x4\Framework;

interface DataBase
{
     public function run_query($query);
     public function run_is_query($query);
     public function escape_string($string);
     public function getLastError();
     public function getLastInsertedId();
}