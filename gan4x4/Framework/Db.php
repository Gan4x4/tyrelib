<?php

namespace gan4x4\Framework;





abstract class Db implements DataBase {
    //put your code here
    
    protected $sql = null;
    protected $db_name= '';
    
    public function DbRunSomeQueryes($ql)
    {   
        if (! is_array($ql))
        {
            if (trim($ql) == '') return false;
            $ql = array($ql);
        };
        if (count($ql) == 0) return false;
        foreach ($ql as $query)
        {
            if ($this->run_query($query) == false) return false ;
        };
        return true;
    }
    
    
    public function get_table_info($table)
    {
        $table_info=array();
        $query = "SELECT * FROM `COLUMNS` WHERE `TABLE_SCHEMA` = '".$this->db_name."' AND `TABLE_NAME` = '".$table."'";
        //AND `TABLE_SCHEMA` = '".$db."'";
        $res=$this->run_is_query($query);
        if (count($res) == 0) return false;
        foreach($res as $line)
        {
            $table_info[$line['COLUMN_NAME']]['title']=$line['COLUMN_COMMENT'];
            $table_info[$line['COLUMN_NAME']]['type']=strtolower($line['COLUMN_TYPE']);
            $table_info[$line['COLUMN_NAME']]['default']=$line['COLUMN_DEFAULT'];
            // for complex object
            $table_info[$line['COLUMN_NAME']]['table']=$table;
        };
        return $table_info;
    }
    
    
    public function getLine($table,$id)
    {
        $query = "SELECT  *  FROM `$table`  WHERE id = ".$this->escape_string($id);
        $res =  $this->run_query($query);
        if (is_array($res) && count($res) > 0) return $res[0];
        return false;
    }
    
    public function runQuery($query)
    {
        return $this->run_query($query);
    }
    
    public function prepareValueToDb($val)
    {
        $value = 'NULL';
        if ($val !== null) {
            if ($val === false) $val = 0;
            $value = $this->escape_string($val);
        };
        
        
        return $value;
    }
    
    public function updateLine($table,$data)
    {
        $pair=array();
        
        //if (count($this->getLine($table,$data['id'])) == 0) 
        if ($this->getLine($table,$data['id']) === false)
        {
            $r = $this->insertLine($table,$data);
            if ($r == $data['id']) return true;
            return false;
        };
            
        
        foreach($data as $key=>$val) 
        {
            $pair[] = $key." = ". $this->prepareValueToDb($val);
        };    
        
        $query="UPDATE ".$table." SET ".self::create_commas_list($pair)." WHERE id = ".$this->escape_string($data['id']);
        if ( ! $this->run_query($query) !== false) return true;
        return false;
    }
    
    
    public function deleteLine($table,$id){
        $line_id = intval($id);
        //$query = "DELETE FROM ".$this->escape_string($table)." WHERE id = '".$line_id."'";
        $query = "DELETE FROM ".$table." WHERE id = '".$line_id."'";
        //error_log($query);
        $res = $this->run_query($query);
        return $res === false ? false : true; 
    }
    
    public function insertLine($table,$data)
    {
        //$old_last_id = getLastInsertedId();
        $v=$k=array();
        foreach($data as $key=>$val) 
        {
            //if ($key=='id') continue;
            $v[] = $this->prepareValueToDb($val);
            $k[]=$key;
        };    
        $query="INSERT INTO ".$table."( ".self::create_commas_list($k)." ) VALUES ( ".self::create_commas_list($v)." )";
        if ($this->run_query($query) !== false) {
            if (isset($data['id'])){
                $new_id = $data['id'];
            }
            else{
                $new_id = $this->getLastInsertedId();
            };
            //assert($old_last_id != $new_id);
            return $new_id;
        }
        return false;
    }
    
    public function clearTable($table)
    {
        $query = "TRUNCATE TABLE `$table`";
        $this->run_query($query);
    }
    
    
    public static function create_commas_list($arr,$add = ', ')
    {
        $ca=' ';
        $res='';
        if ((! is_array($arr) ) || (count($arr) ==0)) return false;
        foreach ($arr as $value)
        {
            $res.=$ca.$value;
            $ca=$add;
        }
        return $res;
    }
    
    
    
    
}

?>
