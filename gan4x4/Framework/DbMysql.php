<?php
namespace gan4x4\Framework;

class DbMysql extends Db implements DataBase
{
    private $is_name = 'information_schema';
    private $db_name = False;
    public function __construct($name,$user,$pass,$host='localhost',$add_query = '',$is = 'information_schema')
    {
       $this->sql = mysql_connect($host, $user, $pass);
        if ($this->sql)
        {
            if (! mysql_select_db($name))
            {
                //System::Log_error( "Could not select db ". mysql_error() );
                return false;
            };
            $this->DbRunSomeQueryes($add_query);
        }
        else{
            //System::Log_error( "Could not connect to mysql ". mysql_error() );
        };
        // Information schema
        $this->is_name = $is;
        $this->db_name = $name;
    }

    public static function fetchFromRes($res)
    {
        if ($res == False) return False;
        
        $data=array();
        if (is_resource($res))
        {
            $i=0;
            while ($line = mysql_fetch_array($res, MYSQL_ASSOC))
            {
                $data[$i]=$line;
                $i++;
            }
        }
        return $data;
    }
    
    
    public function run_query($query)
    {
        $res=mysql_query($query,$this->sql);
        if (! $res)
        {
            //System::Log_error( " Query not completed : ".$query." | " . mysql_error() ); 
            error_log($query);
            error_log($this->getLastError());
            return false;
        };
        
        return self::fetchFromRes($res);
    }
    
    public function run_is_query($query)
    {
        if (! mysql_select_db($this->is_name,$this->sql))
        {
            //System::Log_error( "Not connect connect to  ". $db." ".mysql_error() );
            return false;
        };
        $res=mysql_query($query,$this->sql);
        if (! $res)
        {
            System::Log_error( " Query not completed : ".$query." | " . mysql_error() ); 
            $res = false;
        };
        mysql_select_db($this->db_name,$this->sql);
        return self::fetchFromRes($res);
    }
    
    public function escape_string($string)
    {
        return "'".mysql_real_escape_string($string,$this->sql)."'";
    }
    
    public function getLastError()
    {
        return mysql_error();
    }
    
}


?>
