<?php

namespace gan4x4\Framework;

class DbObject {
    protected $tableInfo = array();
    protected $tableName;
    protected $data = array();
    protected $db = null;
    
    public function __construct($id,$table,&$db) {
        $this->db = $db;
        $this->tableName = $table;
        $this->readTableStructure();
        if ( ! is_array($id) ){
            $this->data = $this->db->getLine($table,$id); // must be moved to createById
        }
    }
    
    
    protected function readTableStructure(){
        $this->tableInfo = $this->db->get_table_info($this->tableName);
        if (empty($this->tableInfo)){
            throw new \Exception("Bad table name ".$this->tableName);
        }
    }
    
    
    protected function setData($row){
        $this->data = $row;
    }
    /*
    protected static function createByLine($line,$table){
        $this->db = &$db;
        foreach ()
    }
    */
    protected function getField($fieldName){
        if (! $this->hasField($fieldName)){
            throw new \Exception("Bad field name ".$fieldName);
        }
        return $this->data[$fieldName];
    }
    
    public function hasField($fieldName){
        return isset($this->tableInfo[$fieldName]);
    }
    
    public function getId(){
        return $this->getField('id');
    }
    
    public function getName(){
        return $this->getField('name');
    }
    
    public function getDb(){
        return $this->db;
    }
    
    
    public function toArray(){
        return $this->data;
    }
    
}
