<?php
namespace gan4x4\Framework;

class DbPdo extends Db implements DataBase {

    protected $is_sql = null;
    
    public function __construct($name,$user,$pass,$host='localhost',$add_query = '',$driver = 'mysql',$is = 'information_schema')
    {
        $this->db_name = $name;
        $this->sql = new \PDO($driver.':host='.self::getPortFromHost($host).';dbname='.$name.';charset=utf8', $user, $pass);
        $this->is_sql = new \PDO($driver.':host='.self::getPortFromHost($host).';dbname='.$is.';charset=utf8', $user, $pass);
    }
    
    public static function getPortFromHost($host_name)
    {
        $res = explode(':',$host_name);
        $al = count($res)-1; 
        if ($al<1) return $host_name;
        $pval = intval($res[$al]);
        if ($pval > 10 && $pval < 100000)
        {
            $i =strrpos($host_name,':');
            return substr($host_name,0,$i).";port=".$res[$al];
        }
        return $host_name;
    }
    
    
    protected function query($query,$db)
    {
        $res = $db->query($query,\PDO::FETCH_ASSOC);
        if ($res === false){
            error_log($query);
            error_log($this->getLastError());
            ob_start();
                debug_print_backtrace();
                $trace = ob_get_contents();
            ob_end_clean(); 
            //$trace =  debug_backtrace();
            error_log($trace);
            return false;
        }
        return $res->fetchAll();
    }
    
    public function run_query($query)
    {
        /*
        $res=$this->sql->query($query,PDO::FETCH_ASSOC);
        if ($res === False)
        {
//            System::Log_error( " Query not completed : ".$query." | " . $this->sql->errorInfo() );
            error_log($this->getLastError());
            return false;
        };
        return $res->fetchAll();
         * 
         */
        return $this->query($query, $this->sql);
        
    }
    
    public function run_is_query($query)
    {
        return $this->query($query, $this->is_sql);
        /*
        $res = $this->is_sql->query($query);
        if ($res === False)
        {
      //      System::Log_error( " Query not completed : ".$query." | " . errorInfo() ); 
            error_log($query);
            error_log($this->getLastError());
            return false;
        };
        return $res->fetchAll();
          
         */
    }
    
    public function runPreparedQuery($query,$data)
    {
        $stat = $this->sql->prepare($query);
        if ($stat->execute($data))
        {
            return $stat->fetchAll(\PDO::FETCH_ASSOC);
        };
        
        error_log($query);
        error_log($this->getLastError());
        return false;
    }
    
    

    public function escape_string($string)
    {
       return  $this->sql->quote($string);
    }
    
    public function getLastError()
    {
        $ei = $this->sql->errorInfo();
        return $ei[2];
    }
    
    public function import($sql)
    {
        $this->sql->setAttribute(\PDO::ATTR_EMULATE_PREPARES, 0);
        if ( $this->sql->exec($sql) === False)
        {
            return False;
        };
        return True;
    }

    public function getLastInsertedId()
    {
       return $this->sql->lastInsertId(); 
    }
    
}


?>
