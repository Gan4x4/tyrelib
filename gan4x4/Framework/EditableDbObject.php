<?php

namespace gan4x4\Framework;
use \Exception;

class EditableDbObject extends DbObject {
    
    public function getNewId()
    {
        $res = $this->db->runQuery("SELECT id FROM `$this->tableName` ORDER BY id ASC LIMIT 1");
        if ($res === False){
            return False;
        }
        if (count($res) == 0){
            return 1;
        }
        $new_id = intval($res[0]['id']) + 1;
        while (count($this->db->runQuery("SELECT id FROM `$this->tableName` WHERE id = '".$new_id."'")) > 0 ){
            $new_id +=1;
        }
        return $new_id;
    }
    
    function save(){
        if ($this->getId()){
            // Update
            $updateList = array();
            foreach($this->data as $field=>$value){
                $updateList[] = " $field = '$value'";
            }
            $valuesList = Utils::createCommasList($updateList);
            if (! $valuesList) {
                throw new Exception("Can't update object with empty data");
            }
            $query = "UPDATE `$this->tableName` SET $valuesList WHERE id = ".$this->id();
            return $this->db->runQuery($query);
        }
        else{
            // Insert
            $updateList = array();
            var_dump($this->tableInfo);
            foreach ($this->tableInfo as $field=>$propertys){
                if ($field == 'id') {
                    $newId = $this->getNewId();
                    $value = $newId;
                }
                elseif ($this->hasField($field)){
                    $value = $this->getField($field);
                }
                else {
                    $value = $propertys['default'];
                }
                $updateList[] = "'".$value."'";
            }
            $valuesList = Utils::createCommasList($updateList);
            $query = "INSERT INTO `$this->tableName` VALUES ($valuesList)";
            if ($this->db->runQuery($query) !== false){
                $this->setField('id', $newId);
                return true;
            }
            return false;
        }
        
    }
    
    
    protected function setField($fieldName,$value){
        if (! $this->hasField($fieldName)){
            throw new \Exception("Bad field name ".$fieldName);
        }
        $this->data[$fieldName] = $value;
    }
    
}
