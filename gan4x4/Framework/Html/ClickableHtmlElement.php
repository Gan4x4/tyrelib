<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace gan4x4\Framework\Html;

/**
 * Description of ClickableHtmlElement
 *
 * @author anton
 */
class ClickableHtmlElement extends HtmlElement{
    //put your code here
    private $link;
    
    function __construct($tag, $value = '', $attributes = null) {
        parent::__construct($tag, $value, $attributes);
        $link = new HtmlElement('a');
        $this->addNode($link);
        $this->link = $this->getElementsByTagName('a')->item(0);
    }
    
    function setLabel($name){
        $this->link->nodeValue = $name;
    }
    
    function setUrl($url){
        $this->link->setAttribute('href',$url);
    }
    
    
}
