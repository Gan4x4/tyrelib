<?php

namespace gan4x4\Framework\Html;
use \DOMDocument;
use \Exception;

class HtmlElement extends DOMDocument
{
    protected $root = null;
    function __construct($tag,$value='',$attributes = null) {
        parent::__construct('1.0','UTF-8');
        $element = DOMDocument::createElement($tag,$value);
        if (is_array($attributes))
        {
            foreach ($attributes as $key=>$value)
            {
                $element->setAttribute($key, $value);
            };
        };
        $this->root=$element;
        $this->appendChild($element);
    }
    
    public static function loadFromFile($filename){
        $instance = new DOMDocument('1.0','UTF-8');
        $instance->substituteEntities = TRUE;
        if (! file_exists($filename)){
            throw new Exception("File not found ".$filename);
        }
        $text = file_get_contents($filename);
        $instance->loadHTML('<meta http-equiv="Content-Type" content="text/html; charset=utf-8">'.$text);
        return $instance;
    }
    
    public function Show()
    {
       print trim($this->saveHTML());
    }
    
    public function getHtml()
    {
        return trim($this->saveHTML());
    }
    
    function addNode($node,$parent_id = null)
    {
       if ($parent_id != null)
        {
            $parent = $this->getElementById($parent_id);
            if (! $parent ) {
                throw new Exception ("Bad node id : ".$parent_id);
            }
        }
        else {
            $parent = $this->root;
        }
            
        $n = $this->importNode($node->root,true);
        $parent->appendChild($n);
    }
    
}

    
    





?>
