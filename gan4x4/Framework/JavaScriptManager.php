<?php
namespace gan4x4\Framework;
use Exception;
/*
    Java script librarys manager
 */

/**
 * Description of newPHPClass
 *
 * @author Gan4x4
 */
class JavaScriptManager {
    protected $defaultLibs;
    protected $allLibs = [];
    protected $activeLibs = [];
    /*
     * $libList - all avaliable libs
     * $default - libs that must NOT be included
     * 
     */
    function __construct($libList,$default = ['bootstrap','jquery']) {
        $this->defaultLibs = $default;
        $this->allLibs = $libList;
    }
    
    //function private getDe
            
    
    
    function add($name){
        if (isset($this->allLibs[$name])){
            foreach($this->allLibs[$name]['depends'] as $depName){
                if (! in_array($depName,$this->activeLibs) &&
                    ! in_array($depName,$this->defaultLibs)){
                    $this->activeLibs[] = $depName;
                }
            }  
            $this->activeLibs[] = $name;
        }
        else{
            //die("FFFF");
            throw new Exception("Attempt to add uncknown lib : ".$name);
        }
        //var_dump($this->activeLibs);
        //die();
    }
    
    function getCss(){
        $output = '';
        foreach ($this->activeLibs as $lib){
            foreach ($this->allLibs[$lib]['css'] as $css){
                $output .= "<link href='$css' rel='stylesheet'>\n";
            }
        }
        return $output;
    }
    
    function getJs(){
        $output = '';
        foreach ($this->activeLibs as $lib){
            foreach ($this->allLibs[$lib]['js'] as $js){
                $output .= "<script src='$js'></script>\n";
            }
        }
        return $output;
    }
    
    function getInitJs(){
        $output = '';
        foreach ($this->activeLibs as $lib){
            foreach ($this->allLibs[$lib]['init'] as $init){
                $output .= file_get_contents($_SERVER['DOCUMENT_ROOT'].$init);
            }
        }
        return $output;
    }
    
}
