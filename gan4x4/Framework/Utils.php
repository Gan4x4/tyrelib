<?php

namespace gan4x4\Framework;

class Utils {
    
    public static function createCommasList($arr,$add = ', ')
    {
        $ca = ' ';
        $res = '';
        if ((! is_array($arr) ) || (count($arr) ==0)){
            return false;
        }
        foreach ($arr as $value){
            $res .= $ca.$value;
            $ca = $add;
        }
        return $res;
    }
    
    public static function addQuote($arr,$qoute="'")
    {
        $output = array();
        foreach ($arr as $key=>$value) {
            $output[$key] = $qoute.$value.$qoute;
        }
        return $output;
    }
    
    /*
    private static function output(&$slots){
        $final = [];
        $keys = array_keys($slots);
        foreach ($keys as $key){
           $final[]= $key;
           if (count($slots[$key]) > 0){
               $final = array_merge($final,self::output($slots[$key]));
           }else{
               //$final .= '/';
           }
           //$final .= "/";
        }
        //$final .= "/";
        return $final;
    }
    */
    
    private static function output(&$slots){
        $final = '';
        $keys = array_keys($slots);
        $terminal = true;
        foreach ($keys as $key){
           $final .= $key;
           if (count($slots[$key]) > 0){
               $final .= ' '.self::output($slots[$key]);
               $terminal = false;
               
           }else{
               $final .= '/';
           }
        }
        // remove slash on last item
        $final = rtrim($final, '/');
        if ($terminal ) {
            $final .=', ';
        }
        return $final;
    }
    public static function createCompactString($array,$delimetr = '; '){
        //var_dump($array);
        $slots = [];
        // build suffix tree
        foreach ($array as $element){
            $parts = explode(' ', $element);
  
            $current = &$slots;
            foreach ($parts as $part){
                if (! isset($current[$part])){
                    $current[$part] = [];
                }
                $current = &$current[$part];
            }
        }
        // Write tree to string
        $output = '';
        foreach ($slots as $brand=>$subs){
            $output[] = $brand .' '.rtrim(self::output($subs),', ');
        }
        return implode($delimetr,$output);
    }
    
    public static function addDayPostfix($i){
        if ($i == 0) {
            return '';
        }
        $postfix = "день";
        
        $str = strrev(strval(intval($i)));
        if (strlen($str) > 1 && $str{1} == '1'){
            //если предпоследня цифра 1, то "дней"
            $postfix = "дней";
        }
        elseif ($str{0} == '0' || intval($str{0}) >= 5 ){
            //иначе если последняя цифра 0 или >=5, то "дней"
            $postfix = "дней";
        }
        elseif (in_array($str{0},['2','3','4'])){
            $postfix = "дня";
        }
        
        return $i." ".$postfix;
    }
    
}
