<?php
namespace gan4x4\Market;
use gan4x4\Market\Good;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Busket
 *
 * @author anton
 */
class Busket {
    private $busketKey = null;
    private $session = null;
    private static $delimeter = ';';
    private $goods = []; 
    
    public function __construct($key,&$session) {
        $this->busketKey = $key;
        $this->session = &$session;
        $this->readGoodsListFromCode();
    }
     
    private function get_market_order_code()
    {
        if (isset($this->session[$this->busketKey])){
            return $this->session[$this->busketKey];
        }
        return '';
    }
    
    private function set_market_order_code($value)
    {   
        $this->session[$this->busketKey] = $value;
        if (! setcookie($this->busketKey,$value,time()+7200,'/')) {
            error_log("Saving busket in cookie not succesfull"); 
        }
    }
    
    private function readGoodsListFromCode(){
        $rawCode = $this->get_market_order_code();
        $zakaz=explode(";",$rawCode);
        
        foreach($zakaz as $item){
            $id = substr($item,0,6);
            $amount =substr($item,6,1);
            // TO check
            if (! empty($id) && $amount > 0){
                $this->goods[$id] = $amount;
            }
        }
    }
    
    private function goods2code()
    {
        $tmpContainer = [];
        foreach ($this->goods as $id =>$amount){
            $tmpContainer[] = $id.$amount;
        }
        return implode(self::$delimeter, $tmpContainer);
    }
    
    // public
    
    public function clear(){
        $this->set_market_order_code('');
    }
    
    public function save(){
        $code = $this->goods2code();
        $this->set_market_order_code($code);
    }
    
    function getAmount($goodId)
    {
        if (isset($this->goods[$goodId])){
            return $this->goods[$goodId];
        }
        return false;
    }
    
    
    
    function getGoodIds(){
        return array_keys($this->goods);
    }
    function getGoodsIdsAndAmountArray(){
        
        return $this->goods;
    }
    
    function getMaxDelay(){
        $delay = 0;
        $goodIds = $this->getGoodIds();
        foreach ($goodIds as $id){
            $good = new Good($id);   
            $delay += $good->getDeliveryDelay();
        }
        return $delay;  
    }
    
    function addGood($id,$amount){
        $this->goods[$id] = $amount;
    }
    
    function isEmpty(){
        return count($this->goods) == 0;
    }
}
