<?php
namespace gan4x4\Market;

class Car extends \Db_object{
    private $brand = null;
    public static $tuneList = array(0 => 'Стандарт', 1 => 'Туризм',2 => 'Экстрим');
    
    public function __construct($id) {
        parent::__construct($id, 'cars');
        $this->brand = new \Db_object($this->get_field_value('brand_id'),'car_brand');
    }
    
    public function getBrand()
    {
        return $this->brand;
    }
    
    public function Name()
    {
        return $this->getBrand()->name()." ".parent::name();
    }
}
