<?php
namespace gan4x4\Market;

abstract class CatalogEntity extends \complex_db_object implements Validator{
    protected $error;
    public static $allowedTypes = array('Шины'=>'t','Диски'=>'d','Подвеска'=>'sa');
    public static $sininimField = 'sinonims';
    protected $sites = array();
    public static $commonPictureDir = '/pic/';
    
    protected abstract function getPictures();
    protected abstract function getDirForPicture();
    
    function __construct($input,$table,$table_name='',$moreFields=''){
        $this->error = '';
        parent::__construct($input, $table, $table_name,$moreFields);
        assert(isset($this->datalist['id']));
        assert($this->datalist['id']['title'] == 'id');
        assert(isset($this->datalist['name']));
    }
    
    // ============== Pictire section
    public static function setCommonPictureDir($path){
        static::$commonPictureDir = $path;
    }
    
    protected function getFullPictureDir(){
        return self::$commonPictureDir.$this->getDirForPicture();
    }
    // =======================================================
    
    
    //==================== View ==========================================================
    protected function getEditLink()
    {
        return "<a href='".$_SERVER['PHP_SELF']."?action=".ACT_EDIT."&id=".$this->id()."'> <img src='/design/edit.gif'></a>";
    }
    
    
// ==================== End view ==============================
    
// =============== Valiadate =====================================    
    public function validateName()
    {
        $name = $this->name();
        if (empty($name)){
            $this->error = "Имя не должно быть пустым";
        }
        return empty($this->error);
    }
    
    public function Validate($input = false){
        $this->validateName();
        $picCollection = $this->getPictures();
        if (! $picCollection->Validate($_FILES)){
            $this->error = $picCollection->getValidateErrorMessage();
        }
        return $this->error == '';
    }
    
    public function getValidateErrorMessage(){
        return $this->error;
    }
    
     protected function setError($message){
        $this->error = $message;
    }
    
    protected function resetError(){
        $this->error = '';
    }
    
    protected function isError(){
        return $this->error != '';
    }
    
    
// ==================== End Validate =============================
    
    public function Save()
    {
        if (! $this->Validate()){
            throw new \Exception("Данные содержат ошибку: ".$this->getValidateErrorMessage());
        }
        if (parent::Save()){
            $picList = $this->getPictures();
            if ($picList){
                $picList->deleteSelectedPictures($_POST);
                $picList->createFilesOnDisk($_FILES);
            }
            $this->clearCache();
        return true;
        }
        return false;

    }
    
    function Delete()
    {
        $pl = $this->getPictures();
        $pl->deleteFilesOnDisk();
        $result =  parent::Delete();
        $this->clearCache();
        return $result;
    }
    
    
    public function clearCache($cacheFilesToDel =  false){
        if (! is_array($cacheFilesToDel)) {
            return ;
        }
        foreach($cacheFilesToDel as $file){
            if (file_exists($file)){
                $success = unlink($file);
                assert($success,"Cache not cleared ".$file);
            }
        }
    }

    
    public static function joinById($select1,$select2){
        $join = array();
        foreach($select1 as $id =>$value){
            if (isset($select2[$id])){
                $join['id'] = array_merge($value,$select2[$id]);
            }
            else{
                $join['id'] = $value;
            }
        }
        return $join;
    }
    
    public static function getGoodIdList($select){
        $ids = array();
        //$keys = array_keys($select);
        //foreach($keys as $id ){
        //    $ids[] = "'".$id."'";
        //}
        foreach($select as $value ){
            $ids[] = "'".$value['id']."'";
        }
        return \System::create_commas_list($ids);
    }
    
    public static function selectGoodFast($info_table,$where,$tail = '')
    {
        $emptyArray = array();
        $info = \System::run_query("SELECT id FROM $info_table $where");
        if (count($info) > 0 ){
            $idList = self::getGoodIdList($info);
            //$goods=System::run_query("SELECT * FROM goods, $info_table  WHERE goods.id IN ($idList) AND goods.id=$info_table.id ".$tail);
            $goods=\System::run_query("SELECT * FROM goods  WHERE goods.id IN ($idList) ".$tail);
            //print "SELECT * FROM goods  WHERE goods.id IN ($idList) ".$tail;
            if (count($goods) >0  ){
                //var_dump($info);
                //var_dump($goods);
                return self::joinById($info, $goods);    
            }
        }
        return $emptyArray;
    }
        
    
    public function getSinonims(){
        $sin = $this->get_field_value(static::$sininimField);
        $result = array($this->name());
        if (! empty($sin)){
            $parts = explode(',',$sin);
            foreach($parts as $value){
                $result[] = trim($value);
            }
        }
        return array_unique($result);
    }
    
    
    
    public function getSearchPattern($field){
        $brandSinonims = $this->getSinonims();
        return \System::prepareNamesToSqlLike($brandSinonims,$field);
    }
    
    
  
}
