<?php

namespace gan4x4\Market\Et;

class EntitySelect {
    
    private $html;
    
    const CAR = 1;
    const TYRE = 2;
    const WHEEL = 3;
    
    const TYPE = 'type';
    const BRAND = 'brand';
    const MODEL = 'model';
    const GOOD = 'good';
    
    protected $productTypes = array(self::CAR=>"Автомобиль",self::TYRE=>"Шина",self::WHEEL=>"Диск");
    protected $fields = array (self::TYPE=>-1,self::BRAND=>-1,self::MODEL=>-1,self::GOOD=>-1); 
    protected $realFields = array();
    private $type = null;
    private $brand = null;
    private $model = null;
    private $good = null;
    
    
    public function __construct($values,$postfix = '',$entitys = null) {
        global $paths;
        if ($entitys == null ){
            $entitys = $this->productTypes;
        }
        
        // Generate real fields
        foreach ($this->fields as $key=>$value){
            $this->realFields[$key] = $key.$postfix;
            if (isset($values[$key])){
                $this->fields[$key] = $values[$key];
            }
        }
        
        // replace field names becouse we can have multiple selects on page
        $toBeReplaced = array();
        foreach ($this->fields as $key=>$value){
            $toBeReplaced[] = '{'.$key.'}';
        }
        $rawHtml = file_get_contents($paths['blocks']."html/entity_select.html");
        $this->html = str_replace($toBeReplaced,  $this->realFields,$rawHtml);
        
        $this->insertSelect(self::TYPE,$entitys);
        
        $brands = get_sel_data($this->fields[self::TYPE],'','','');
        $this->insertSelect(self::BRAND,$brands);
        
        $models=get_sel_data($this->fields[self::TYPE],$this->fields[self::BRAND],'','');
        $this->insertSelect(self::MODEL,$models);
        
        $goods=get_sel_data($this->fields[self::TYPE],$this->fields[self::BRAND],$this->fields[self::MODEL],'');
        
        $this->insertSelect(self::GOOD,$goods);
        
    }
    
    
    private function insertSelect($name,$data ){
        $body = '<option value="-1" SELECTED >Не выбран</option>'."\n";
        if (! empty($data)){
            $body .= select_box_body($data,$this->fields[$name]);
        }
        $this->html = str_replace("{".$name."_select}",  $body,$this->html);
    }
    
    public function get($key){
       // print "K: ".$key;
        return $this->fields[$key];
    }
    
    public function getHtml(){
        return $this->html;
    }
    
    public function hasType(){
        return $this->get(self::TYPE) > 0;
    }
    
}
