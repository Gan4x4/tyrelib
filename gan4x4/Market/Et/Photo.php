<?php

namespace gan4x4\Market\Et;


use gan4x4\Framework\DbObject;

class Photo extends DbObject{
    public function __construct($id,  &$db) {
        parent::__construct($id, 'market_pic', $db);
    }
    
    public function getWwwPath(){
        return $this->getField('link_thumb');
    }
    
}
