<?php

namespace gan4x4\Market\Filter;

class Filter {
    private $name;
    private $request;
    private $data = array();
   
    function __construct($name, $value2nameArray,$request) {
        $this->name = $name;
        $this->request = $request;
        $i = 0;
        // Generate num instead value for security purpose
        foreach($value2nameArray as $key=>$label){
            $this->data[$i]['key'] = $key;
            $this->data[$i]['label'] = $label;
            $i++;
        }
    }
    
    public function show($passiveView, $activeView= null){
        if ($activeView == null){
            $activeView = $passiveView;
        }
        foreach ($this->data as $index=>$value){
            if ($this->getValue() == $value['key']){
                //$style= "background-color:red; display:inline";
                $view = $activeView;
                
            }
            else{
                $view = $passiveView;
                $url = $this->request->generateUrl(array($this->name =>$index ));//."#diametr";
                $view->setUrl($url);
            }
            $view->setLabel($value['label']);
            $view->show();
            //print "<a href='$url'><div style='$style'>".$value['label']."</div></a>";
        }
    }
    
    public function getValue(){
        if ($this->request->isFieldFilled($this->name) && 
            isset($this->data[$this->request->getIntField($this->name)])){
            $key = $this->data[$this->request->getIntField($this->name)]['key'];
            return $key;
        }
        // Bad key or key not found
        return false;
    }
    
    
}
