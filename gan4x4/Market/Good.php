<?php

namespace gan4x4\Market;
use gan4x4\Market\Picture\PictureCollection;
use gan4x4\Market\Picture\OneDirThumbPicture;
use gan4x4\Market\PromoAction;

class Good extends CatalogEntity  { 

    protected static $non_changed_fields_table = "market_info_static_fields";
    protected static $fieldsShowOrder = array(
        'price1'=>1,
        'price2'=>2,
        'amount'=>3,
        'reserv1'=>4);
    protected $error;
    public $perfix = false;
           
    public function __construct($id,$table='',$table_name='',$moreFields=''){
        $table='goods';
        
        if (is_string($id)){
             $this->perfix= substr($id,0,2);
        }

        if (is_array($id) && isset($id['_type'])){
            $this->perfix = $id['_type'];
            unset($id['_type']);
        }

        parent::__construct($id,$table,'',$moreFields);
        $this->disableSomeFields(array('image','cat','g_date'));
        $this->sortFieldsByOrder(static::$fieldsShowOrder);
     }
     
    
// ====================== Db_Object improvements ================
    protected function disableSomeFields($fieldsToHide){
        foreach ($fieldsToHide as $field) {
            $this->datalist[$field]['title'] = '';  
        }
    }
    
    protected function hideSomeFields($fieldsToHide){
        foreach ($fieldsToHide as $field) {
            $this->datalist[$field]['htm_type'] = HIDDEN;  
        }
    }
     
    static function sortByPredefinedOrder($a,$b){
        $al = $a['order'];
        $bl = $b['order'];
        if ($al == $bl) {
            //if ($a['title'] == )
            return self::sortByTitle($a,$b);
        }
        return ($al > $bl) ? +1 : -1;
    }
    
    protected function sortFieldsByOrder($fieldsShowOrder){
        foreach($this->datalist as $key=>$value){
            if (isset($fieldsShowOrder[$key])){
                $this->datalist[$key]['order'] = $fieldsShowOrder[$key];
            }
            else {
                $this->datalist[$key]['order'] = 9999;
            }
        }
        $this->sortFields("sortByPredefinedOrder");
    }
// ====================== Db_Object improvements ================
     
    protected function getDirForPicture() {
       $perfix = $this->getPerfix(); ;
       $subdir = $perfix{0}.'/';
       return $subdir;
    }
    
    
    public function getPictures(){
        $collection = new PictureCollection();
        $subdir=$this->getFullPictureDir();
        $picture = new OneDirThumbPicture($subdir, $this->id());
        $collection->add($picture);
        return $collection;
    }
    
    function getAvIcon(){
        global  $paths;
        $av = $this->good->get_field_value('amount');
        switch ($av) {
            case 0:
                    $icon = 'av0.gif';
                break;
            case 1:
                    $icon = 'av1.gif';
                break;
            case 2:
                    $icon = 'av2.gif';
                break;

            default:
                throw new Exception("Invalid amount field ".$av);
                break;
        }
        $image =  "http://www.extremetyre.ru/design/".$icon;
        return "<img src='$image' >";
    }
        
    function show_head($styles = '', $body = 100){
        $picCol = $this->getPictures();
        if ($picCol->getSize() > 0 ){
            print  $picCol->getFirst()->getImgSmall()."<br>";
            //print "<img src='".$picCol->getFirst()->getSmall()."'><br>";
        }
        print $this->name();
        if ($this->id()){
            $id = $this->id();
            //print "HERE";
          
            print " <a href='http://www.4x4typ.ru/market/product.php?id=".$this->id()."'  target=new> <img src='/design/view.gif' ></a>";
            print " <a href='good.php?action=".ACT_EDIT."&id=".$this->id()."'> <img src='/design/edit.gif'></a> ";
            if ($this->good){
                print $this->getAvIcon();
                print " ".$this->good->get_field_value('amount_d')." шт.";
            }
        }
    }
    
    
    public static function createByRequest($input){
        //print "SB<br>";
        $request = new Request($input);
        //$request->setField('_type',$request->getType());
        $reqData = $request->getRawData();
        $instance = new static($reqData);
        //$instance = new static($input);
        foreach ($instance->datalist as $key=>$value){
            if (isset($reqData[$key])){
                $instance->set_field_value($key,$reqData[$key]);
            }
        }
        
        
        
        return $instance;
    }
    
    
         
    public static function getNotChangedFields($table)
    {
        //$table .= '_info';
        $query = "SELECT name FROM ".self::$non_changed_fields_table." WHERE `table` = ".\System::escape_string($table);
        $res = \System::run_query($query);
        if ($res !== False && count($res > 0))
        {
            $ret = array();
            foreach ($res as $value)
            {
                $ret[] = trim($value['name']);
            };
            return $ret;
        }
        return false;
    }
    
    protected function init_add_date()
    {
        $tables = $this->getTables();
        $non_changed = array();
        foreach ($tables as $table)
        {
            $x = self::getNotChangedFields($table);
            foreach ($x as $field)
            {
                if ($table != $this->table_name)
                {
                    $new_key = $table.'_'.$field;
                }
                else
                {
                    $new_key = $field;
                };
                $d = \System::get_db_data_by_field($table,$field);
                $x = array();
                foreach ($d as $key=>$value)
                {
                    $x[$value] = $value;   
                };
                $non_changed[$new_key] = array( SELECT,$x);
            };
        };
        
        
        
       
       $actions[0] = "Не участвует";
       $actions = array_merge($actions,PromoAction::getAllActionNamesAndId());
        
        $hform_data = array(
            'amount'=>array(
                SELECT,
                array(0=>'Недоступен',
                      1=>'В продаже',
                      2=>'Поставка под заказ')
                ),
            'reserv1'=>array(
                SELECT,
                $actions
                      
                )
            );
        
        $hform_data = array_merge($hform_data,$non_changed);
        foreach($hform_data as $key => $value)
        {
            if (isset($this->datalist[$key]))
            {
                $this->datalist[$key]['htm_type'] = $value[0]; 
                $this->datalist[$key]['sqldata'] = $value[1]; 
            }
            else
            {
                return False;
            };
                
        };
        return true;
         
        
    }
    
    
     public function getAddFormBody($input = ''){
        $body = parent::getAddFormBody();
        $pics = $this->getPictures()->getLoadHtml();
        $typeAdd = "\n<input type='hidden' name='_type' value='".$this->getPerfix()."' />";
        return $body.$pics."<br>\n".$typeAdd; 
                 
    }
        
    public function show_add_form($input = false,$back_url = '')    {
        $currPerfix = $this->getPerfix();
        //print $currPerfix;
        if (empty($currPerfix)){
            print "<b>Current id is invalid ".$currPerfix."</b>";
        }
        parent::show_add_form($input,$back_url);
        
       
        $backUrl = $this->getReturnUrl();
        print '<input type=button value = "Назад к списку" onClick="location.href=\''.$backUrl.'\'">';
    }   
    
    public function getReturnUrl(){
        $type = $this->getPerfix();
        return  'goodlist.php?type='.$type;
    }
    
    function getType(){
        if (! $this->id()){
            throw new \Exception("Id not set, we can't get type! ");
        }
        $string = $this->id();
        return $string{0};
    }
    
    protected function getGoodsType(){
        return $this->getType();
    }
   /*
    private function validateBigImageSizes(){
         // check sizes   
        $img = $this->getBigImage();
        foreach (self::$picValidSizes as $size){
            $w = $size[WIDTH]; 
            if ( $w > 0 && $w > $img->getWidth()){
                $this->error = "Ширина изображения должна быть не менее $w px. ";
            }
            
            $h = $size[HEIGTH]; 
            if ( $h > 0 && $h > $img->getHeigth()){
                $this->error = "Высота изображения должна быть не менее $h px. ";
            }
        }
    }
    
    private function validateBigImage(){
        try{
            $img = $this->getBigImage();    
        } catch (Exception $ex) {
            $this->error = "Image has invalid format or extension";
            return;
        }
        $this->validateBigImageSizes();
        if (! $img->isCornersWhite()){
            $this->error = 'Изображение должно иметь белый фон! ';
        }
    }
    */
// =========================== end of pic ========================
    
//======================= Validator  ===============================
    
    protected function fillAutoFields(){
        
        //if (! $this->id()){
        //    $this-> = $this->generateNewPerfix();
        ////    $this->perfix = $newPerfix;
        //}
        
        // Set opt price
        $optPrice = $this->get_field_value('price2');
        if ($optPrice == 0){
            $price = $this->get_field_value('price1');
            $newOptPrice = $price * 0.9;
            $this->set_field_value('price2',$newOptPrice);
        }
        
    }
    
    public function Validate($input = false){
        
        $this->fillAutoFields();
        parent::Validate($input);
        $this->error = '';
        //die("GGG ".$this->name());
       
        
        if ($this->get_field_value('amount') > 0 ){
            
            if ($this->get_field_value('price1') <=0 ){
                $this->error = "У товаров доступных для заказа должна быть розничная цена";
            }
            
             if ($this->get_field_value('price2') <=0 ){
                $this->error = "У товаров доступных для заказа должна быть оптовая цена";
            }
            
            
            if ($this->get_field_value('price2') > $this->get_field_value('price1')){
                $this->error = "Розничная цена не может быть больше оптовой.";
            }
    
        }
        
        //if ($this->isBigPictUploaded($input)){
        //    $this->validateBigImage($input[PIC_BIG]);
        //}
        
        
        //return false;
        return $this->error == '';
    }
    
    public function getValidateErrorMessage(){
        return "Error: ".$this->error;
    }
    
    protected function setError($message){
        $this->error = $message;
    }
    
    public function bindToPromoAction($actionId){
        $this->set_field_value('reserv1',$actionId);
    }
    
    public function getPromoActionId(){
        return $this->get_field_value('reserv1');
    }
    
    protected function resetError(){
        $this->error = '';
    }
    
    protected function isError(){
        return $this->error != '';
    }
    
    
    //override
    public function Save() {
        // get old price
        if ($this->id()){
            $oldObject = new static($this->id()); 
            if ($this->getOldPrice() != $oldObject->getPrice()){
                $this->set_field_value('old_price',$oldObject->getPrice());
            }
            // to do fill changed fields
        }
        return parent::Save();
    }
//======================= End Validator  ===============================
    
// ================ id and perfix ================================
     public function getPerfix(){
        if (empty($this->perfix)){
            $id = $this->id();
            $this->perfix = substr($id, 0, 2);
        }
        if (empty($this->perfix)){
            throw new Exception("Can't get perfix");
        } 
        return $this->perfix;
    }
    
    public function getNewId()
    {
	$pfx = $this->getPerfix();
        if (strlen($pfx) != 2){
            throw new \Exception("Perfix ($pfx)length not equal 2");
        }
	assert(strlen($pfx) == 2,"Perfix length not equal 2");
        $table = $this->table_name;
        $res = \System::run_query("SELECT id FROM ".$table." WHERE id LIKE '".$pfx."%' ");
	$id_list = array();
        //if ($bad_ids !=null) $id_list = array_merge ($id_list,$bad_ids);
	foreach ($res as $value) {
            $id_list[]=$value['id'];
        }
        if ($res === False) return False;
	$i = 1;
        do
        {
            $test_id=self::genId($pfx,$i);
            $i++;
            if ($i>9999){
                \System::Log_error("No free id with perfix ".$pfx);
                return False;
            }
        }
        while(in_array($test_id,$id_list));
        return $test_id;
    }
    
    
    public static function genId($pfx,$num)
    {
        if ( (strlen($pfx) != 2) || ($num>9999) || ($num < 0))
        {
            return False;
        };
        $res = $pfx .sprintf("%04d", $num);
        return $res;
    }
// ================ End id and perfix ================================
    
    
    public function getPrice()
    {
        return $this->get_field_value('price1');
    }
    
    public function getOldPrice()
    {
        return $this->get_field_value('old_price');
    }
    
    
    public function getOptPrice()
    {
        return $this->get_field_value('price2');
    }
    
    public function isDiscount()
    {
        return $this->get_field_value('reserv1') == 2;
    }
    
    public function isBest()
    {
        return $this->get_field_value('reserv1') == 1;
    }

    public function getVendorId(){
        return false;
    }
    
    public function getStock()
    {
        return $this->get_field_value('amount_d');
    }
    
    public function getDeliveryDelay(){
        return $this->get_field_value('wait');
    }
    
}
