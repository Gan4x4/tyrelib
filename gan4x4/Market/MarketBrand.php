<?php
namespace gan4x4\Market;
use gan4x4\Market\Picture\PictureSize;
use gan4x4\Market\Picture\PictureCollection;
use gan4x4\Market\Picture\OneSizePicture;
use Exception;
class MarketBrand extends CatalogEntity{
    //public static $wwwEntityDir = '/pic/brand/';
    //protected static $picValidSizes=array(PIC_SMALL=>array(90,68),PIC_NORMAL=>array(120,90));
    
    function __construct($input){
        $this->error = '';
        
        parent::__construct($input, 'market_brand', '');
        if (is_array($input)){
            $this->populateTypeByRequest($input);
        }
        $this->datalist['type']['title'] = ''; // Exclude from html - edit list
    }
    
    protected function getDirForPicture(){
        return 'brand';
    }
    
    /*protected function getEntityPicDir(){
        return $_SERVER['DOCUMENT_ROOT'].self::$wwwEntityDir.'brand/';
    }
    */
    public function getPictures(){
        $collection = new PictureCollection(2);
        $size68 = new PictureSize(PictureSize::PIC_SMALL,90,68);
        $size90 = new PictureSize(PictureSize::PIC_NORMAL,120,90);
        $picture68 = new OneSizePicture($this->getFullPictureDir(), $this->id(),$size68);
        $collection->add($picture68);
        $picture90 = new OneSizePicture($this->getFullPictureDir(), $this->id(),$size90);
        $collection->add($picture90);
        return $collection;
    }
    
    
    /*
    protected function getBasePathForView(){
        
    }
    */
    
    
    public function getLinkToSite($type){
        if ($type == 't' && $this->isMakeTyre() ){
            return " <a href='http://www.extremetyre.ru/tyre.htm?brand_id=".$this->id()." '  target=new> <img src='/design/view.gif' ></a>";
        }
        elseif ($type == 'd' && $this->isMakeWheel() ){
            return " <a href='http://www.extremetyre.ru/wheel.htm?brand_id=".$this->id()." '  target=new> <img src='/design/view.gif' ></a>";
        }
        else{
            //throw new \Exception("Only Tyre and Wheel Brands has pages on site");
            //return " <a href='http://www.4x4typ.ru/product.php?brand_id=".$this->id()." '  target=new> <img src='/design/view.gif' ></a>";
            return "";
        }
    }
    
    public function ShowLink(){
        $id = $this->id();
        
        print "<b>".$this->name()."</b>";
        print "<a href='".$_SERVER['PHP_SELF']."?action=".ACT_EDIT."&id=".$id."'> <img src='/design/edit.gif'></a>";
        print "(";
        if ($this->isMakeTyre()){
            print " <a href='http://www.extremetyre.ru/tyre.htm?brand_id=".$this->id()." '  target=new> <img src='/design/view.gif' ></a>";
            print " <a href ='model.php?brand_id=".$id."&_type=t' >Шины</a> ";    
        }
        
        if ($this->isMakeWheel()){
            print " <a href='http://www.extremetyre.ru/wheel.htm?brand_id=".$this->id()." '  target=new> <img src='/design/view.gif' ></a>";
            print " <a href ='model.php?brand_id=".$id."&_type=d' >Диски</a> ";    
        }
        print ")";
        
        
    }
    
    private function populateTypeByRequest($request){
        // Parse HtmlForm input to find goods type produced by brand
        $selectedTypes = array();
        foreach(self::$allowedTypes as $name=>$type){
            $key = "type_".$type;
            if (isset($request[$key])){
                $selectedTypes[] =$type;
            }
        }
        // Save finded type to field
        if (count($selectedTypes) > 0 ){
            $typesList = trim(\System::create_commas_list($selectedTypes));
            $this->set_field_value('type',$typesList);
        }
    }
    
    private function isHasType($type){
        //print "isHas: ".$this->get_field_value('type')." ";
        return strpos($this->get_field_value('type'),$type) !== false;
    }
    
    private function getTypeSelector($current = null){
        //var_dump(self::$allowedTypes);
        $allowedTypes = self::$allowedTypes;
        //Model::getAllowedTypes();
        foreach($allowedTypes as $name=>$type){
            if ($this->isHasType($type) || $current == $type){
                $checked = 'CHECKED';
            }
            else{
                $checked = '';
            }
            print " <input type='checkbox' name='type_".$type."' $checked>".$name." ";
        }
    }
    
    
    // Override
    public function getAddFormBody($input = ''){
        $commomInputs = parent::getAddFormBody($input);
        //$picSmall = $this->getPicLoadHtml(PIC_SMALL);
        //$picNormal = $this->getPicLoadHtml(PIC_NORMAL);
        $pictures = $this->getPictures()->getLoadHtml();
        $request = new Request($input);
        try {
            $type = $request->getType();
            $typeForReturn = "<input type='hidden' name='type' value='".$request->getType()."'>";
        } catch (Exception $exc) {
            $type = null;
        }

        $typeSelector =  "Виды продукции: ".$this->getTypeSelector($type);
        $delim =  "\n <br>\n";
       
        return $commomInputs.$typeForReturn.$delim.$typeSelector.$delim.$pictures.$delim."<hr>";
    }
    
    
    public function isMakeTyre(){
        return  $this->isHasType('t');
    }
    
    public function isMakeWheel(){
        return $this->isHasType('d');
    }
    
    
    public function Validate($input = false){
        //if (count($this->getGoodsType()) ==0 ){
        //    $this->error = "Brand must produce tyre or/and wheel.";
        //    return false;
        //}
        return parent::Validate($input);
    }
   
  
    
    // Override
    public function clearCache($cacheFilesToDel = false){
        $fn = Utils::get_cache_fn('tyre.htm','',"brand_id=".$this->id());
        $fn1 = Utils::get_cache_fn('tyre.htm','','');
        $cacheFilesToDel = array($fn,$fn1);
        parent::clearCache($cacheFilesToDel);
    }
    
    
    // Override
    protected function init_add_date()
    {
        parent::init_add_date();
        $this->datalist['majority']['htm_type'] = SELECT; 
        $this->datalist['majority']['sqldata'] = array(0=>"Основной",1=>"Второстепенный"); 
        
        $this->datalist['no_model']['htm_type'] = SELECT; 
        $this->datalist['no_model']['sqldata'] = array(0=>"Реальные модели",1=>"Модели фиктивные"); 
        
        return true;

        
    }
    
    
    
}
