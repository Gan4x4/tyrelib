<?php
namespace gan4x4\Market;

class MarketOrderSource extends Request{
    private static $table = 'market_order_source'; 
    
    // All this field must be added to a sql table !
    private static $mark = array('utm_');
    private static $yandexMarket = array('ymclid','frommarket');
    private static $yandexDirect = array('yclid'); // Utm fields findeds in $mark
    //private static $yandexDirect = array('ymclid','frommarket');
    //private static $voron = array('wds'); // Alexandr Vorontsov
    
    protected static $dbObject = null;
    //const UP2GO = 14;
    
    public function __construct($input = false) {
        global $session_ids;
        parent::__construct($input);
        $this->dbObject = new \Db_object('',static::$table);
        
        // save user prefs
        $sessionPrefs = array($session_ids['market_client_wish']);
        $this->saveDataOnClientSide($sessionPrefs);
        
        $sourceFields = $this->getInterestRequestFields();
        if (count($sourceFields) > 0){
            $this->saveDataOnClientSide($sourceFields);
        }
        $this->restoreFromClient();
    }
    
    public function getInterestRequestFields(){
        $fields = $this->dbObject->getFields();
        //var_dump($fields);
        $result = array();
        
        foreach($fields as $field){
            // Yandex Direct
            
            foreach (self::$mark as $prefix){
                if (strpos($field,$prefix) !== false){
                    $result[] = $field;
                }
            }
            // Yandex 
            if (in_array($field,array_merge(self::$yandexMarket,self::$yandexDirect))){
                $result[] = $field;
            }
        }
        
        
        return $result;
    }
    
    public function saveDataOnClientSide($sourceFields){
        foreach($sourceFields as $field){
            $value = $this->getStringField($field);
            if ($value){
                $this->saveOnClient($field,$value);
            }
        }
    }
    
    protected function saveOnClient($field,$value){
        $_SESSION[$field] = $value;
        setcookie($field, $value, time() + (86400 * 30), "/"); // 86400 = 1 day
    }
    
    protected function getFromClient($field){
        $result = false;
        $cookie = filter_var($_COOKIE[$field],FILTER_SANITIZE_STRING);
        if (! empty($_SESSION[$field])){
            $result = $_SESSION[$field];
        }elseif(! empty($cookie)){
            $result = $cookie;
        }
        return $result;
    }
    
    protected function restoreFromClient(){
        $fields = $this->getInterestRequestFields();
        foreach($fields as $field){
            $value = $this->getFromClient($field);
            if (! empty($value)){
                $this->dbObject->set_field_value($field,$value);
            }
        }
    }
    
    protected function isSourceSet(){
        $fields = $this->getInterestRequestFields();
        //error_log("Fields ".var_export($fields,true));
        foreach($fields as $field){
            $value = $this->dbObject->get_field_value($field);
            //print " $field => ".$value;
            //print "is empty ".empty($value)."<br>";
            if (! empty($value)){
                return true;
            }
        }
        return false;
    }
    
    
    private function hasParamsBy($owner){
        $fields = $this->getInterestRequestFields();
        foreach($fields as $field){
            if (  in_array($field, $owner)) {
                $value = $this->dbObject->get_field_value($field);
                if (! empty($value)){
                    return true;
                }
            }
        }
        return false;
        
    }
    /*
    public function isYandexMarket()
    {
        $fields = $this->getInterestRequestFields();
        foreach($fields as $field){
            if (  in_array($field, self::$yandexMarket)) {
                $value = $this->dbObject->get_field_value($field);
                if (! empty($value)){
                    return true;
                }
            }
        }
        return false;
    }
    
    public function isYandexDirect()
    {
        $fields = $this->getInterestRequestFields();
        foreach($fields as $field){
            if (  in_array($field, self::$yandexDirect)) {
                $value = $this->dbObject->get_field_value($field);
                if (! empty($value)){
                    return true;
                }
            }
        }
        return false;
    }
    */
    /*
    public function isVoron()
    {
        $fields = $this->getInterestRequestFields();
        //var_dump($fields);
        //die();
        foreach($fields as $field){
            if (  in_array($field, self::$voron) ) {
                    $value = $this->dbObject->get_field_value($field);
                if (! empty($value)){
                    return true;
                }
            }
        }
        return false;
    }
    */
    
    public function getSource(){
        if ($this->isSourceSet()){
            if ($this->hasParamsBy(self::$yandexDirect)){
                return Order::SOURCE_YANDEX_DIRECT; 
            }elseif ($this->hasParamsBy(self::$yandexMarket)){
                return Order::SOURCE_YANDEX_MARKET;
            }
        }
        return Order::SOURCE_SITE;   
    }
    

    public function getPhone(){
        $source = $this->getSource();
        return Order::getPhoneBySource($source);
    }
    
    public function getMail(){
        $source = $this->getSource();
        return Order::getMailBySource($source);
    }
    
    public function Save($order_id){
        if ($this->isSourceSet()){
            $this->dbObject->set_field_value('partner_id',Order::SOURCE_ADV);
            $this->dbObject->set_field_value('order_id',$order_id);
            $this->dbObject->Save();
            //print "Source info : ".$this->dbObject->id();
        }
    }
    
    public function dump(){
        $fields = $this->getInterestRequestFields();
        //var_dump($fields);
        foreach($fields as $field){
            print $field." =>".$this->getFromClient($field)."<br>\n";
        }
    }
    
    
    
}
