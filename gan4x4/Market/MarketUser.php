<?php

namespace gan4x4\Market;
use gan4x4\Market\ReadOnly\Car as Car;

class MarketUser {
    private static $instance = null;
    private $phpbbUser = null;
    private $marketUserId = null;
    private $marketUserObject = null;
    private $phpbbAuth = null; 
    private $session = null;
    private $sessionKeys = null;
    
    private function __construct(&$user,&$auth) {
        global $session_ids;
        $this->sessionKeys = $session_ids;
        $this->phpbbUser = &$user;
        $this->phpbbAuth = &$auth;
        $this->session = &$_SESSION;
    }
    
    
    
    public static function getInstance($user,$auth){
        if (self::$instance == null){
            self::$instance = new static($user,$auth);
        }
        return self::$instance;
    }

    public function isLogged(){
        $user = $this->phpbbUser;
//        var_dump($user->data['user_id']);
        return $user->data['is_registered'] && ($user->data['user_id'] > 1);
    }
    
    
    
    
    
    public function isInMarketUsersTable(){
        //global $user;
        if (! $this->id()){
            //print "No ID".var_dump($this->phpbbUser)."<br>".var_dump($user);
            return false;
        }
        $query= "SELECT * FROM market_users WHERE id = ".$this->id();
	$result = mysql_query($query) or die(mysql_error().$query);
	$rs=mysql_num_rows($result);
        if ($rs == 1 ) {
            return true;
        }
        assert(false,"Duplicate user id");
        
    }
    
    public function id(){
        if (! $this->isLogged()){
            return false;
        }
        else{
            return $this->phpbbUser->data['user_id'];
        }
    }
    
   
    
    public function updateName($name){
        if (self::isUserName($name) && $name != $this->getUserName()){
            $this->setUserName($name);
            $this->Save();
        }
    }
    
    
    public function registerNewUserByContacts($contacts,$name = ''){
        $goodContactFinded = false;
        foreach ($contacts as $contact){
            if (self::isPhone($contact)){
                $phone = $contact;
                $goodContactFinded = true;
            }elseif(self::isEmail($contact)){
                $mail = $contact;
                $goodContactFinded = true;
            } 
        }
        if ($goodContactFinded){
            $this->_registerUser($name,$phone,$mail); 
        }
        else{
            throw new \Exception("Not valud contacts found in: ".var_export($contacts,true));
        }
    }
    
    public function registerNewUser($contact,$name = ''){
        if (self::isPhone($contact)){
            $this->_registerUser($name,$contact); 
        }elseif(self::isEmail($contact)){
            $this->_registerUser($name,'',$contact);
        }
        else{
            throw new \Exception("Bad contact: $contact !");
        }
    }
    
    public function registerNewUserByFullData($request){
        $name = $request->getStringField('name');
        $phone = $request->getStringField('phone');
        $city = $request->getStringField('city');
        $mail = $request->getStringField('mail');
        $this->_registerUser($name,$phone,$mail,$city);
    }
    
    protected function _registerUser($name,$phone ='',$mail = '',  $city = ''){
        //print "name: $name";
        $nameForReg = $name;
        $code=self::get_new_code(6);
        if (! self::check_username($nameForReg)) {
            //$originalName = $name;
            $nameForReg = self::new_username($name);
        }
        if ($mail =='') {
            $mail = '_';
        }
        
        if ($city == '' ){
           $city =  $this->getTown();
        }
        $cp_data = array();
        $user_row = array(
            'username'              => $nameForReg,
            'user_password'         => phpbb_hash($code),
            '_free_pwd'             => $code,
            'user_email'            => $mail,
            'group_id'              => 2,
            'user_timezone'         => 0.00,
            'user_dst'              => $is_dst,
            'user_lang'             => 'ru',
            'user_type'             => 0,
            'user_actkey'           => '',
            'user_ip'               => $this->phpbbUser->ip,
            'user_regdate'		=> time(),
            'user_inactive_reason'	=> $user_inactive_reason,
            'user_inactive_time'	=> $user_inactive_time,
            '_phone'		=> $phone,
            '_adress'		=> $city,
            '_discount'             =>0,
            '_place'		=> 'A'
            );

        /*
         In this function incorporated phpbb_logic and sql for inserting 
         user line into market_users table it't ugly, bad  legacy code
        */
        $this->marketUserId=user_add($user_row, $cp_data); 
        $this->phpbbAuth->login($nameForReg, $code, TRUE, FALSE, 0);  // id go to $user
        assert('$this->isInMarketUsersTable()','User not put in market table');
        
        // rename user see comment before
        if ($nameForReg != $name){
            $mUser = new \Db_object($this->marketUserId,'market_users');
            $mUser->set_field_value('name',$name); 
            $mUser->save();
        }
    }
    
    public function getMarketUser(){
        if ($this->marketUserObject == null){
            if ($this->id() ){
                $this->marketUserObject = new \Db_object($this->id(),'market_users');
                if (! $this->marketUserObject->id()){
                    throw new \Exception("Call to market user before it created");
                }
            }
        }
        return $this->marketUserObject;
    }


    public function setUserName($name){
        $this->getMarketUser()->set_field_value('name',$name);
    }
    
    public function getUserName(){
        return $this->getMarketUser()->get_field_value('name');
    }
    
    
    public function setPhone($phone){
        $this->getMarketUser()->set_field_value('phone',$phone);
    }
    
    
    public function getTown(){
        if ( $this->isInMarketUsersTable() && $this->isLogged()){
            return $this->getMarketUser()->get_field_value('town');
        }else{
            return $this->getPreference('market_user_town');//$this->session['market_user_town'];
        }
    }
    
    public function setTown($town,$save = false){
        if ( $this->isInMarketUsersTable() && $this->isLogged()){
            $this->getMarketUser()->set_field_value('town',$town);
            $this->Save();
        }else{
            //$this->session['market_user_town'] = $town;
            $this->setPreference('market_user_town',$town);
        } 
    }
    
    
    public function getPreference($key){
        if (isset($this->sessionKeys[$key])){
             return $this->session[$this->sessionKeys[$key]];
        }else{
            throw new Exception("Bad preference key $key");
        }
    }
    
    
    public function setPreference($key,$value){
        //global $session_ids;
        if (isset($this->sessionKeys[$key])){
            $this->session[$this->sessionKeys[$key]] = $value;
        }
        //error_log("int : ".var_export($this->session,true));
        //error_log("glob : ".var_export($_SESSION,true));
    }
    
    
    public function getPhone(){
        return $this->getMarketUser()->get_field_value('phone');
    }
    
    public function getMail(){
        return $this->getMarketUser()->get_field_value('mail');
    }
    
    protected function setMail($mail){
        $this->getMarketUser()->set_field_value('mail',$mail);
    }
    
    protected function setAdress($adress){
        $this->getMarketUser()->set_field_value('adress',$adress);
    }
    
    /*
    protected function restoreCar(){
        
    }
    
    protected function saveCar(){
        if (is_object($this->car)){
            \System::run_query("DELETE FROM market_user_prefs WHERE pref_name = 'car' ");
            \System::run_query("INSERT INTO market_user_prefs user_id = ".$this->id().", pref_name = 'car' , value = '' ");
        }
        
	$result = mysql_query($query) or die(mysql_error().$query);
    }
    */
    
    public function savePreferences(){
        
    }
    
    public function Save(){
        if (! $this->getMarketUser()->Save()){
            throw new \Exception("Error on update market_user");
        }
        $this->savePreferences();
    }
    
    /*
    protected function updateUser($name,$phone,$mail,$city){
        $query="UPDATE market_users SET name='$DA[name]', phone='$DA[phone]', mail='$DA[mail]', adress='$DA[city]' WHERE id=".$muid;
        $result = mysql_query($query) or die(mysql_error().$query);
    }
     * 
     */
    /*
    public function getName(){
        return 
    }
    */
    
    protected static function check_username($name)
    {
	$name=trim($name);
	if ($name == '') {
            return FALSE;
        };
        $query= "SELECT * FROM phpbb_users WHERE username = '".$name."'";
	$result = mysql_query($query) or die(mysql_error().$query);
	$rs=mysql_num_rows($result);
	if ($rs>0)  { return FALSE; };
	
	//mb_internal_encoding("UTF-8");
	//$low_name = mb_strtolower($name,'utf-8');
	
	$query= "SELECT * FROM phpbb_users WHERE username_clean = '".utf8_clean_string($name)."'";
	//var_dump($query);
	$result = mysql_query($query) or die(mysql_error().$query);
	$rs=mysql_num_rows($result);
	if ($rs>0)  { return FALSE; };
	return TRUE;
    }
    
    
    protected static function new_username($shab)
    {
	$shab=strtolower($shab);
	if (empty($shab)) { 
            $shab='customer';
        }
//	$query= "SELECT * FROM phpbb_users WHERE username_clean LIKE '$shab%' ORDER BY username DESC";
	$query= "SELECT * FROM phpbb_users ORDER BY user_id DESC";
	$result = mysql_query($query) or die(mysql_error().$query);
	//print $query;
	$line = mysql_fetch_array($result, MYSQL_ASSOC);
	$num=strlen($shab);
	$pnum=substr($line['username_clean'],$num);
	$pnum=$line['user_id']+1;
	//print "$line =".$line['username_clean']." -- ".$num." pnum=".$pnum;

	return $shab.$pnum;
    }
    
    
    public static function get_new_code($len)
    {
	$rs=1;
	$res='';
	while ($rs>0)
	{
		for ($i=0;$i<$len;$i++)
		{
			$r[0]=chr(rand(48,57));
			$r[1]=chr(rand(65,90));
			$r[2]=chr(rand(97,122));
			$ind=rand(0,2);
			$res=$res.$r[$ind];
		};
		$query="SELECT * FROM market_orders WHERE code='".$res."'";
		$result = mysql_query($query) or die("Query failed".mysql_error().$query);
		$rs=mysql_num_rows($result);
	};

	return $res;
    }
    
    
    public function getSessionDuration(){
        return time() - $this->phpbbUser->data['session_start'];
        
    }
    
    public function canShowQuestionWindow(){
        //error_log("DIZ :".var_export($this->getPreference('market_user_show_adv'),true));
        if ($this->getPreference('market_user_show_adv') === true){
            return  $this->getSessionDuration()+1; // zero mean user cancel this option
        }
        else{
            return 0;
        }
        
    }
    
    
    
    public function isContact($contact){
        if (is_array($contact)){
            foreach ($contact as $value) {
                if (self::isPhone($value) || self::isEmail($value)){
                    return true;
                }
            }
            return false;
        }
        else{
            return (self::isPhone($contact) || self::isEmail($contact));    
        }
    }
    
     public function updateContacts($contacts){
        if (! array($contacts) && ! empty($contacts)){
            $contacts = array($contacts);
        }
        $contactsUpdated = false;
        foreach ($contacts as $contact){
            if ($this->setContact($contact)){
                $contactsUpdated = true;
            }
        }
        if ($contactsUpdated){
            $this->Save(); 
        }
    }
    
    public function setContact($contact){
        if (self::isPhone($contact) && $contact != $this->getPhone()){
            $this->setPhone($contact);
            return true;
        }elseif(self::isEmail($contact) && $contact != $this->getMail()){
            $this->setMail($contact);
            return true;
        }
        return false;
    }
    
    public static function isPhone($str)
    {
	if (preg_match ("/^\+?[-0-9\s+()]{7,25}$/",$str)) { 
            return true;
        }
	else {
            return false; 
        }
    }

    public static function isEmail($str)
    {
        if (trim($str)=='') { 
            return FALSE; 
        }
	if (preg_match ("/([a-z0-9])([-a-z0-9._])+([a-z0-9])\@([a-z0-9])([-a-z0-9_])+([a-z0-9])(\.([a-z0-9])([-a-z0-9_-])([a-z0-9])+)*/i",$str)){
            return TRUE; 
        }
	else { 
            return FALSE; 
        }
    }
    
    public static function isUserName($str){
        $oneName =  "[а-яА-ЯёЁa-zA-Z][а-яА-ЯёЁa-zA-Z0-9-_\.]{1,20}";
        if (preg_match ("/^($oneName)+\s*($oneName)?$/msiu",$str)){
            return TRUE; 
        }
	else { 
            return FALSE; 
        }
    }
    
    public function setCarId($id){
        $this->setPreference('market_user_car',$id);
    }
    
    public function getCarId(){
        $this->getPreference('market_user_car');
    }
    
    public function setCarTune($id){
        $this->setPreference('market_user_car_tune',$id);
    }
    
    public function getCarTune(){
        $this->getPreference('market_user_car_tune');
    }
    
    public function getCar($db){
        if ($this->getCarId()){
            $car = new Car($this->getCarId(),$db);
            $car->setTune($this->getTune());
            return $car;
        }
        else{
            return false;
        }
    }
    
    //public function getWish(){
        
    //}
    
}
