<?php
namespace gan4x4\Market;
use gan4x4\Market\Picture\PictureCollection;
use gan4x4\Market\Picture\WhiteCanvasResizablePicture;

abstract class Model extends CatalogEntity{ 
    private $brand = null;

    //protected static $picValidSizes=array(PIC_SMALL=>array(0,80),PIC_NORMAL=>array(300,0),PIC_BIG=>array(0,0));
    protected static $modelsTables = array('sa'=>'sa_models','t'=>'tyre_models','d'=>'wheel_models');
    protected static $modelsClasses = array('sa_models'=>'ShockAbsorberModel','tyre_models'=>'TyreModel','wheel_models'=>'WheelModel');
        
    
    function __construct($input,$table,$table_name = '')
    {
        parent::__construct($input, $table, $table_name);
        $this->datalist['brand_id']['htm_type']=HIDDEN;
        $this->set_field_value('av',strval(intval($this->get_field_value('av'))));
    }
    
    
    
    protected function getDirForPicture(){
        return 'models';
    }
    
    public function getPictures(){
        $collection = new PictureCollection();
        //$base = $this->getFullPictureDir();
        //$subdirs = $this->getGoodsType();
        //foreach ($subdirs as $subdir){
        $picture = new WhiteCanvasResizablePicture($this->getFullPictureDir(), $this->id());
        $collection->add($picture);
        //}
        return $collection;
    }
    
    
    private static function table2class($table){
        return __NAMESPACE__.'\\'.self::$modelsClasses[$table];
    }
    
    public static function getAllowedTypes(){
        return array_keys(self::$modelsTables);
    }
    
    public static function getTableByType($type){
        // Type is id part like t for tyres or sa for shock absorbers

        $table = Utils::findByPerfix($type,self::$modelsTables);
        if ($table == false){
            throw new \Exception("Bad model type ".$type);
        }
        return $table;
    }
    
    public static function getClassByType($type){
        $table = self::getTableByType($type);
        return self::table2class($table);
    }
    
    public static function createModel($id,$table){
        if (! isset(self::$modelsClasses[$table])){
            throw new \Exception("Bad model table ".$table);
        }
        $class = self::table2class($table);
        return new $class($id);
    }
    
    public function getBrand()
    {
        if ($this->brand == null){
            $this->brand = new MarketBrand($this->get_field_value('brand_id'));
        }
        if (! is_object($this->brand)){
            throw new \Exception("Brand for model not found");
        }
        return $this->brand;
    }
    
    public function getBrandName(){
        $brand = $this->getBrand();
        return $brand->name();
    }
    
    public function getType(){
       //return Request::getModelTable($id) 
        $table2key = array_flip(static::$modelsTables);
        if (isset($table2key[$this->table_name])){
            return $table2key[$this->table_name];
        }
        else{
            throw new \Exception("Table unset for model");
        }
    }
    
    protected function getEditLink(){
        return "<a href='".$_SERVER['PHP_SELF']."?action=".ACT_EDIT."&id=".$this->id()."&type=".$this->getType()."'> <img src='/design/edit.gif'></a>";
    }
    
    // Must be overrided
    protected function getViewLink(){
        return false;
    }
    
    
    public function getPict(){
        //$pictPath = $this->getPicFileWwwPath(PIC_SMALL);
        $pic = $this->getPictures()->getFirst();
        $pictPath = $this->getPictures()->getFirst()->getSmall();
        //var_dump($pic);
        //print $pictPath."<hr>";
        $picHtm='';
        //var_dump($picHtm);
        if ($pictPath){
            $picHtm = '<img src="'.$pictPath.'"> ';
        }
        return $picHtm;
    }
    
     
    public function ShowLink(){
        $id = $this->id();
        print "<a href ='productlist.php?model_id=".$id."&type=".$this->getType()."' >".$this->getPict().$this->name()."</a> ";
        print $this->getViewLink();
        print $this->getEditLink();
    }

    
    // Override
    public function getAddFormBody($input = ''){
        $commomInputs = parent::getAddFormBody($input);
        $brandName = "Бренд : ".$this->getBrandName();
        $type = "<input type='hidden' name='_type' value ='".$this->getType()."' >\n";
        $delim =  "\n <br>\n";
        $pics = $this->getPictures()->getLoadHtml();
        return $brandName.$delim.$type.$commomInputs.$pics.'<hr>'.$delim."<hr>";
    }
    
    public function Save(){
        $this->set_field_value('brand',$this->getBrandName());
        if ($this->id()) {
            // new model han't id before save
            $this->updateAv();
        }
        parent::Save();
                
    }
    
    public function setAv($enabled){
        assert($enabled ==1 || $enabled == 0);
        $this->set_field_value('av',$enabled);
    }
    
    public function getAv(){
        return $this->get_field_value('av');
    }
    
    public function getAvGoodCount(){
        $table = $this->getType()."_info";
//        $query = "SELECT id FROM ".$table.$table;
  //      $res = System::run_query($query);
        $sellingGoods = self::selectGoodFast($table,"WHERE model_id = ".$this->id(),' AND (amount > 0 || amount_d > 0) ');
        //var_dump($sellingGoods);
        return count($sellingGoods);
    }
    
    public function updateAv(){
        $goodsInPrice = $this->getAvGoodCount();
        if ( $goodsInPrice == 0){
            $this->setAv(0);
        }
        else{
            $this->setAv(1);
        }
    }
    
   
    
    protected function getModelsFromTable($table){
        $query = "SELECT * FROM $table WHERE brand_id= ".$this->getBrand()->id();
        if ($table == $this->table_name){
            $query .= " AND id <> ".$this->id();
        }
        $data = \System::run_query($query);
        $result =array();
        foreach($data as $line){
            $result[] = self::createModel($line['id'], $table);
        }
        return $result;
    }
    
    public function getOtherModelsOfBrand(){
        $models = array();
        //$modelTables = self::$modelsTables;
        //$modelTables[] = 'tyre_models_dust';
        foreach (self::$modelsTables as $table){
            $models = array_merge($this->getModelsFromTable($table),$models);
        }
        return $models;
    }
    
    public function getTable(){
        return $this->table_name;
    }
    
    
}
