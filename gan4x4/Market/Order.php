<?php

namespace gan4x4\Market;
use gan4x4\Market\Good;
use gan4x4\Market\PromoAction;

class Order extends \Db_object implements Validator{
    const SOURCE_SITE = 0;
    const SOURCE_PHONE = 1;
    const SOURCE_YANDEX_DIRECT = 2; 
    const SOURCE_YANDEX_MARKET = 3;
    const SOURCE_ADV = 4;
    
    const STATUS_BILL_SEND = 6;
    const STATUS_CHECK_BILL = 14;
    const STATUS_PAY_INFORM = 15; // Сообщил об оплате
    
   
    
    //const SOURCE_VORON = 5;

    protected $error;
    protected $request;
    protected $goods = array();
    protected static $sources = array(
        self::SOURCE_SITE=>"С сайта",
        self::SOURCE_PHONE=>"По основному телефону",
        self::SOURCE_YANDEX_DIRECT=>"Y.Direct",
        self::SOURCE_YANDEX_MARKET=>"Y.Market",
        self::SOURCE_ADV=>"По внешнему рекламному телефону",
        );
    
    
    
    
    protected static $phone2source = array('74959892259' => self::SOURCE_PHONE ,
        '74952551459' => self::SOURCE_ADV );
    protected static $mail2source = array('market4x4@yandex.ru' => self::SOURCE_PHONE ,
        'info@extremetyre.ru' => self::SOURCE_ADV );
    
    
    public function __construct($id = '') {
        parent::__construct($id,'market_orders');
    }
    
    public static function createEmptyOrderByUserAccount($marketUser){
        //global $user,$auth;
        global $def_sid_value,$session_ids;
        $instance = new static();
        $wish = isset($_SESSION[$session_ids['market_client_wish']]) ?  $_SESSION[$session_ids['market_client_wish']] : $def_sid_value['market_client_wish'] ;
        $instance->set_field_value('client_wish',$wish);
        $instance->set_field_value('user_id',$marketUser->id());
        $now = date ("Y-m-d");
        $tomorrow  = date('Y-m-d',mktime(0, 0, 0, date("m")  , date("d")+1, date("Y")));
        $defaults = array ('region' => 99,
            'type'=>1,
            'delivery'=>0,
            'pay'=>0,
            'status'=>1,
            'site'=>4, //ET
            'receive_date' =>$now,
            'change_date'=>$now,
            'hold_date'=>$tomorrow,
            'code'=>  MarketUser::get_new_code(6),
            'region' =>0,
            'car_id'=>0,
            'car_tune'=>0
            );

        foreach ($defaults as $key=>$value){
            $instance->set_field_value($key,$value);
        }
        //$instance->setSource($request->getSource());
        return $instance;
    }
    
    
    public static function createFullOrder($marketUser,$request,$goods){
        if (! $marketUser->isLogged()){
        // Auto create user
            $marketUser->registerNewUserByFullData($request);
        }
        $newOrder = Order::createEmptyOrderByUserAccount($marketUser);
        foreach($goods as $good){
            $newOrder->addGood($good['id'], $good['amount']);
        }
        $newOrder->fillOrderByRequest($request);
        return $newOrder;
    }
    
    public function fillOrderByRequest($request){
        if ($request->isFieldFilled('city')){
            $this->set_field_value('adress',$request->getStringField('city'));
        }
        foreach($this->datalist as $key=>$value){
            if ($request->isFieldFilled($key)){
                //print "Filled ".$key."<br>";
                if ( self::ekran($value['type'])){
                    $newVal = $request->getStringField($key);
                }
                else{
                    $newVal = $request->getIntField($key);
                }
                $this->set_field_value($key,$newVal);
            }
        }
        
        //$holdDate = $request->getDate(1);
        $holdDate =  $request->getStringField('date');
        
        if ($holdDate != false){
            $this->set_field_value('hold_date',$holdDate);
        }
        $this->setSource($request->getSource());
    }
    
    public static function getPhoneBySource($source){
        assert('isset(self::$sources[$source])');
        $source2phone = array_flip(self::$phone2source);
        if ($source != self::SOURCE_SITE){
            return $source2phone[self::SOURCE_ADV];
        }
        else{
            return $source2phone[self::SOURCE_PHONE];
        }
        //var_dump($source2phone);
        //var_dump($source);
        //return $source2phone[self::SOURCE_SITE];
    }
    
    public static function getMailBySource($source){
        assert('isset(self::$sources[$source])');
        $mail2phone = array_flip(self::$mail2source);
        if ($source != self::SOURCE_SITE){
            return $mail2phone[self::SOURCE_ADV];
        }
        else{
            return $mail2phone[self::SOURCE_PHONE];
        }
    }
    
    public function setSource($source){
        //$sourcesIds = array_keys($this->sources);
        assert('isset(self::$sources[$source])');
        //in_array($source,array(self::SOURCE_SITE,self::SOURCE_PHONE,self::SOURCE_UP2GO)));
        $this->set_field_value('source',$source);
    }
    
    public static function getSourceName($source){
        if (isset(self::$sources[$source])){
            return self::$sources[$source];
        }
        return "source_not_set ".$source;
        
    }
    
    public static function getSourceByPhone($phone){
        if (isset(self::$phone2source[$phone])){
            return self::$phone2source[$phone];
        }
        return false;
    }
    
    
    public static function getSourcesWithNames(){
        return self::$sources;
    }
   
    protected function validateName(){
        
    }
    
    public function Validate($input = false) {
        ;
    }
    public function getValidateErrorMessage(){
        return $this->error;
    }
    
    protected function setError($message){
        $this->error = $message;
    }
    
    protected function resetError(){
        $this->error = '';
    }
    
    protected function isError(){
        return $this->error != '';
    }
    
    private function findPromo($name){
        foreach ($this->goods as $line){
            if ($name != '' && $line->name() == $name) {
                return true;
            }
        }
        return false;
    }
    
    public function addPromo($id,$amount){
        
        if ($id > 0){
            $promo = new PromoAction($id);
            //$idAndLink = "#1 ".$promo->name()."</a>";
            //$promoOrderId = $promo->getIdForOrder();
            if ($promo->isActive() && 
                $promo->getMinAmount() <= $amount  &&
                $promo->getIdForOrder() != '' &&
                $this->findPromo($promo->getIdForOrder()) == false){
                //$this->addGoodByName($idAndLink);
                $goodLine = new \Db_object('',"market_og");
                $goodLine->set_field_value('good_id',$promo->getIdForOrder());
                $goodLine->set_field_value('amount',1);
                //error_log("Add promo ".$id." -- ".$promo->getIdForOrder()."  ".var_export($goodLine,true));
                $this->goods[] = $goodLine;
            }
        }
    }
    
    
    public function addGood($good_id, $amount = 1,$price = false){
        $goodLine = new \Db_object('',"market_og");
        //$goodLine->set_field_value('order_id',$this->id());
        
        $goodLine->set_field_value('amount',$amount);
        
        $priceLine = new \Db_object($good_id,"goods");
        if (! $price){
            // discount !
            $price = $priceLine->get_field_value('price1');
        }
        $goodLine->set_field_value('good_id',$good_id);
        //$goodLine->set_field_value('name','dummy');
        $goodLine->set_field_value('price',$price);
        $this->goods[$good_id] = $goodLine;
        $this->addPromo($priceLine->get_field_value('reserv1'),$amount);
    }
    
    public function addGoodByName($goodName, $amount = 1,$price = 0){
        $goodLine = new \Db_object('',"market_og");
        $goodLine->set_field_value('amount',$amount);
        $goodLine->set_field_value('price',$price);
        $goodLine->set_field_value('name',$goodName);
        //$goodLine->set_field_value('name','dummy');
        //$goodLine->set_field_value('price',$price);
        $this->goods[] = $goodLine;
    }
    
    
    // $MsoRequest = MarketOrderSource special type of Request for work vith Advertisiment data
    public function setRequest($MsoRequest){
        $this->setSource($MsoRequest->getSource());
        $this->request = $MsoRequest;
    }
    
    public function Save(){
        // May be source can set here
        $this->set_field_value('change_date',Date('Y-m-d'));
        $this->set_field_value('get_time',Date('Y-m-d H:i:s'));
        $this->set_field_value('change_time',"'".Date('H:i:s')."'");
        if ($this->getName() == ''){
            $this->setName($this->generateName());
        }
        if (parent::Save()){
            assert($this->id());
            if ($this->request != null){
                // Call MarketOrderSource for store adv data
                $this->request->Save($this->id());    
            }
            return $this->saveGoodsList();
        }
        return false;
        
    }
    
    private function saveGoodsList(){
        foreach($this->goods as $value){
            $value->set_field_value('order_id',$this->id());
            if (! $value->Save()){
                return false;
            }
        }
        return true;
    }
    
    private function generateName(){
        if ($this->get_field_value('type') == 2){
            $name = "Заказ ";
        }else{
            $name = "Запрос ";
        }
    
        if (count($this->goods) > 0){
            $first = array_values($this->goods)[0];
            //$first = $this->goods[0];
            if (trim($first->get_field_value('name')) == '' && strlen($first->get_field_value('good_id')) == 6 ){
                $good =  new \Db_object($first->get_field_value('good_id'),"goods");
                $name .= $good->get_field_value('name');
            }
            else{
                $name .=$first->get_field_value('name');
            }
            error_log(var_export($first,true));
            $name .= ' '.$first->get_field_value('amount').' шт.';
            
            if (count($this->goods) > 1){
             $name .= ' ... ';   
            }
            $name .= " ".$this->getSumm().' р.';
        }
        return $name;
        
    }
    
    
    public function getCode(){
        return $this->get_field_value('code');
    }
    
    public function getSumm(){
        $total = 0;
        foreach($this->goods as $goodLine){
            $price = $goodLine->get_field_value('price');
            $amount = $goodLine->get_field_value('amount');
            $total += $price*$amount;
        }
        return $total;
    }
    public function getName(){
        return $this->get_field_value('name');
    }
    
    public function setWish($wishId){
        return $this->set_field_value('client_wish',$wishId);
    }
    
    
    public function setName($name){
        return $this->set_field_value('name',$name);
    }
    
    public function setRecipientName($name){
        return $this->set_field_value('recipient_name',$name);
    }
        
    public function setPhone($phone){
        return $this->set_field_value('phone',$phone);
    }
    
    public function setTown($town){
        return $this->set_field_value('town',$town);
    }
    
    public function setMail($mail){
        return $this->set_field_value('mail',$mail);
    }
    
    public function setAbout($text){
        return $this->set_field_value('about',$text);
    }

    public function appendToAbout($text){
        return $this->setAbout($this->get_field_value('about')." \n ".$text);
    }

    public function setClientWish($wish){
        return $this->set_field_value('client_wish',$wish);
    }

    public function setType($type){
        return $this->set_field_value('type',$type);
    }
    
    
    
    
    public function pay($summ){
        if (in_array($this->get_field_value('status'),self::getPayWaitStatuses())) {
            $this->set_field_value('status',self::STATUS_PAY_INFORM); 
            if ($summ != $this->getSumm()){
                $this->appendToAbout("Полученно ".$summ." р.");
            }
        }
        else{
            $this->appendToAbout("Попытка оплаты ".$summ." ".Date("Y-m-d h:m:i"));
        }
    }
    
    public static function getPayWaitStatuses(){
       return [self::STATUS_BILL_SEND,self::STATUS_CHECK_BILL];
    }
    
    public function getStatus(){
        return $this->get_field_value('status');
    }
}
