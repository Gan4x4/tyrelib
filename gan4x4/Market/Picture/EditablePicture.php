<?php

namespace gan4x4\Market\Picture;
use gan4x4\Market\Validator;
use Exception;

class EditablePicture extends Picture implements Validator {
    protected static $defaultFieldName = 'image_';
    protected $error;
        
    protected function getPathForNewPicture($size){
        $fnBase = $this->getBaseFilename($size);
        return $_SERVER['DOCUMENT_ROOT'].$this->getWwwPrefix().'/'.$fnBase;
    }
    
    public function createThumbs($image){
        //if (! self::isPicUploaded($input)){
       //     throw new Exception('Bad input');
        //}
        //$image = new Image($input['tmp_name']);
        //$sizes = array(PictureSize::PIC_NORMAL,PictureSize::PIC_SMALL);
        foreach($this->sizes as $size){
            $thumbFilename=$this->getPathForNewPicture($size);
            //var_dump($size);
            $w =  $size->getWidth();
            $h =  $size->getHeigth();
            //var_dump($size);
            //print "Thumb . --".var_dump($thumbFilename)."- $w - $h <HR>";
            $image->writeThumb($thumbFilename,$w,$h);
        }
    }
    
    
   
    public function getHtmlLabel(){
        return "Если картинка достаточного размера и на белом фоне, <br>то остальные будут созданны автоматически на ее основе.";
    }
    
    public function getDefSizeForHtml(){
        return PictureSize::Small();
    }
    
    
    public function getLoadHtml($name = false,$delete = false){
        $field_name = $name ? $name : self::$defaultFieldName;
        $field_delete = '';
        if ($delete != false){
            $field_delete =  $delete;
        }
        $sizeLabel = "(<i>".$this->getHtmlLabel()."</i>)";
        $picHtm = "Изображение  $sizeLabel <input type=file name='".$field_name."'>";
       // $small = $this->getSize(PictureSize::PIC_SMALL);
        $pictPath = $this->getWwwPath($this->getDefSizeForHtml());
        //var_dump($pictPath);
        if ($pictPath){
            $deleteHtm = " <input type=checkbox name='".$field_delete."' > Удалить";
            $picHtm = '<img src="'.$pictPath.'"> '.$deleteHtm.'<br>'.$picHtm;
            
        }
        //else{
            //print "<b>!! $pictPath !!</b>";
        //}
            
        return $picHtm;
    }
    
    public function delete(){
        foreach ($this->sizes as $size){
            //$fnBase = $this->getBaseFilename($size);
            foreach (self::$extensions as $tryExt){
                $filename = $this->getPathForNewPicture($size).'.'.$tryExt;
                //print $filename;
                if (file_exists($filename) && unlink($filename)){
                    print "Deleted file ".$filename."<br>\n";
                }
            }
        }
    }
    
// =========== Validator
    public function Validate($image = false){
        $ext = $image->getExtension();
        if (! in_array($ext,self::$extensions)){
            $this->error = "Недопустимый формат изображения : ".$ext;
        }
        return empty($this->error);
    }
    public function getValidateErrorMessage(){
        return $this->error;
    }
   
    
}
