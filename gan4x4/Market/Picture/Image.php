<?php

namespace gan4x4\Market\Picture;

define("WIDTH", 0);
define("HEIGTH", 1);

class Image {
    private $image = false; // resource
    private $bgThreshold = 240;
    private $cornerDistance = 3;
    private $extension;
    
    
    function __construct($sourceImgFile) {
        if (!file_exists($sourceImgFile)){
            throw new \Exception("File not found: ".$sourceImgFile);
        }

        //$this->extractRealExtension($sourceImgFile);
        //if (! in_array(strtolower($ext),array('jpeg','jpg'))){
        //    throw new Exception("Invalid file extension: ".$ext." olny JPEG allowed");
        //}
        
        $this->readImage($sourceImgFile);
        /*
        $this->image = imagecreatefromjpeg($sourceImgFile);
        if (! $this->image){
            throw new \Exception("Image not created");
        }
        else{
            $this->extension = 'jpg';
        }
         * 
         */
    }
    
    private function readImage($file){
        $jpeg = ['jpg','jpeg','jpe'];
        $gif = ['gif'];
        $ext = strtolower(pathinfo($file,PATHINFO_EXTENSION));
        if ($ext == 'gif'){
            $this->image = imagecreatefromgif($file);
            $this->extension = 'gif'; 
        }
        else{
            // default behavior
            $this->image = imagecreatefromjpeg($file);
            $this->extension = 'jpg';
        }
        
        if (! $this->image){
            throw new \Exception("Image not created");
        }
    }

    private function checkColor($rgb){
        $r = ($rgb >> 16) & 0xFF;
        $g = ($rgb >> 8) & 0xFF;
        $b = $rgb & 0xFF;
        $mean = ($r+$b+$g)/3;
        //if ($r >= 250 && $g >= 250 & $b >= 250)
        return $mean >= $this->bgThreshold;
    }
    
    private function isBackground($x,$y){
        $color = imagecolorat($this->image, $x, $y);
        return $this->checkColor($color);
    }
    
    private function errImageEmpty(){
        throw new \Exception("Image are empty");
    }
    
    private function isRowHasImage($y){
        $w = $this->getWidth();
        for ($x = 0; $x<$w ; $x++){
            if (! $this->isBackground($x, $y)){
                return true;
            }
        }
        return false;
    }
    
    private function isColHasImage($x){
        $h = $this->getHeigth();
        for ($y = 0 ; $y < $h ; $y++){
             if (! $this->isBackground($x, $y)){
                return true;
            }
        }
        return false;
    }
    
    public function scanRowUp($startY = 0){
        $h = $this->getHeigth();
        for($y = $startY ; $y<$h ; $y++){
            if ($this->isRowHasImage($y)){
                return $y;
            }
        }
        $this->errImageEmpty();
    }
    
    
    public function scanRowDown($startY = false){
        if (! $startY){
            $h = $this->getHeigth();
            $startY = $h -1;
        }
        for($y = $startY;$y>=0;$y--){
            if ($this->isRowHasImage($y)){
                return $y;
            }
        }
        $this->errImageEmpty();
    }
    
    
    public function scanColLeft($startX = 0){
        $w = $this->getWidth();
        for($x = $startX;$x<$w;$x++){
           if ($this->isColHasImage($x)){
               return $x;
           }
        }
        throw new \Exception("Image are empty");
    }
    
    public function scanColRight($startX = false){
        $w = $this->getWidth();
        if (! $startX){
            $startX = $w -1;
        }
        for($x = $startX;$x>=0;$x--){
           if ($this->isColHasImage($x)){
               return $x;
           }
        }
        throw new \Exception("Image are empty");
    }
    
    public function getExtension(){
        return $this->extension;
    }
    
    
    // Interface
    
    
    public function getWidth(){
         return imagesx($this->image);
    }
    
    public function getHeigth(){
        return imagesy($this->image);
    }
    
    private function getCropedCopy(){
        $x1 = $this->scanColLeft();
        $y1 = $this->scanRowUp();
        $x2 = $this->scanColRight();
        $y2 = $this->scanRowDown();
        
        $newW = $x2 - $x1 + 1;
        $newH = $y2 - $y1 + 1;
        $dest = imagecreatetruecolor($newW, $newH);
        imagepalettecopy($dest,$this->image);
        imagecopy($dest, $this->image, 0, 0, $x1, $y1, $newW, $newH);
        return $dest;
    }
    
    public function selfAutoCrop(){
        $this->image = $this->getCropedCopy();
    }
            
            
    public function autoCrop($filename){
        $dest = $this->getCropedCopy();
        //imagecopy($dest, $this->image, 0, 0, $x1, $y1, $newW, $newH);
        imagejpeg($dest, $filename);
    }
    
    public function isCornersWhite(){
        $w = imagesx($this->image);
        $h = imagesy($this->image);
        
        $minDimension = $this->cornerDistance *2;
        if ($w <= $minDimension || $h <= $minDimension){
            throw new \Exception("Image too small");
        }
        
        $corners = array(
            array($this->cornerDistance,$this->cornerDistance),
            array($w - $this->cornerDistance,$this->cornerDistance),
            array($this->cornerDistance, $h - $this->cornerDistance),
            array($w - $this->cornerDistance,$h - $this->cornerDistance)
        );
        
        foreach ($corners as $corner){
            if (! $this->isBackground($corner[0], $corner[1])){
                return false;
            }
        }
        
        return true;
    }

    function writeThumb($filename,$wn = 0,$hn = 0){
        assert('! ($wn ==  0 && $hn == 0)',"Width and heigth is zero" );
	$w = $this->getWidth();
	$h = $this->getHeigth();
	
// Autosacale
	if ($wn == 0) { 
            $wn=$w*($hn/$h); 
        }
	if ($hn == 0) {
            $hn=$h*($wn/$w); 
        }
        
	if ($w <= $wn || $h <= $hn) {
            $sc=1; 
            $thumbWidth = $w;
            $thumbHeigth = $h;
        }
        else{
            $sc=0;
            $thumbWidth = $wn;
            $thumbHeigth = $hn;
        }
        

	$sm_im  = imagecreatetruecolor($thumbWidth,$thumbHeigth);
        if ($sm_im == false){
            throw new \Exception ("Cannot Initialize new GD image");
        }
        imagepalettecopy($sm_im,$this->image);
        $white_color = imagecolorallocate($sm_im, 255, 255, 255);
        imagefill($sm_im, 0, 0, $white_color);

	if ($sc ==0){
            imagecopyresampled($sm_im,$this->image,0,0,0,0,$wn,$hn,$w,$h);
	}
	else{
            imagecopy ($sm_im,$this->image,1,1,1,1,$w,$h);
        }
        
        if (pathinfo($filename,PATHINFO_EXTENSION) == false){
            $newFilename = $filename.".".$this->getExtension();
        }
        else{
            $newFilename = $filename;
        }
        
        if ($this->getExtension() == 'gif'){
            imagegif($sm_im,$newFilename); 
        }
        else{
            // default behavior
            imagejpeg($sm_im,$newFilename); 
        }
    }

    
    

    
    
}
