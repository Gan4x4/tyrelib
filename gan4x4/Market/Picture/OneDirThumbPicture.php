<?php

namespace gan4x4\Market\Picture;

class OneDirThumbPicture extends EditablePicture {

    
    // Override
    protected function getBaseFilename($size){
        $fnBase .= $this->filename;
        if ($size->getId() == PictureSize::PIC_NORMAL){
            $fnBase .=  '_b';    
        }
        return $fnBase;
    }
    
    protected function getWwwPath($size){
        $fnBase = $this->getBaseFilename($size);
        foreach (self::$extensions as $tryExt){
            $filename = $this->getWwwPrefix().''.$fnBase.'.'.$tryExt;
            //print $filename;
            if (self::isFileExists($filename)){
                $this->ext = $tryExt;
                return $filename;
            }
        }
        return "";
    }
    
}
