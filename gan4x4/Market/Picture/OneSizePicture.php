<?php

namespace gan4x4\Market\Picture;
use Exception;

class OneSizePicture extends EditablePicture {
    private $size;
    public function __construct($subdir, $name,$size) {
        if ( get_class($size) != __NAMESPACE__.'\PictureSize'){
            throw new Exception('Bad size parametr type ');
        }
        parent::__construct($subdir, $name);
        $this->size = $size;
        $this->sizes = array($size);
    }
    
    public function validateDimEqual($image){
        $w = $this->size->getWidth();
        if ( $w != $image->getWidth()){
            $this->error = "Ширина изображения должна быть $w а не ".$image->getWidth()." px. ";
        }
        $h = $this->size->getHeigth();
        if ( $h != $image->getHeigth()){
            $this->error = "Высота изображения должна быть $h а не ".$image->getHeigth()." px. ";
        }
        return empty($this->error);
    }
    
    public function Validate($image = false){
        $this->validateDimEqual($image);
        return empty($this->error);
    }
    
    
    public function getHtmlLabel(){
        return "Высота картинки ".$this->size->getHeigth()." ширина ".$this->size->getWidth();
    }
    
    public function getDefSizeForHtml(){
        return $this->size;
    }
}
