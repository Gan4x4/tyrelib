<?php

namespace gan4x4\Market\Picture;



class Picture {
    
    const PIC_SMALL = 'pic_small';
    const PIC_NORMAL = 'pic_normal';
    const PIC_BIG = 'pic_big';
    protected static $dir = '';
    //protected static $dir4size=array(self::PIC_SMALL=>'small',self::PIC_NORMAL=>'normal');
    //protected static $picValidSizes=array(PIC_SMALL=>array(0,80),PIC_NORMAL=>array(300,0),PIC_BIG=>array(0,0));
    protected static $extensions =  array('jpg','gif');
    protected $ext = null;
    protected $subdir = false;
    protected $filename = false;
    protected $sizes;
    protected $error;
    
    /*
    public static function init($wwwPictureDir){
        if (self::isFileExists($wwwPictureDir)){
            self::$dir = $wwwPictureDir;
        }
        else{
            throw new Exception("Path $wwwPictureDir not found");
        }
    }
    
    
    
    private function isStaticInitComplete(){
        return self::$dir === null;
    }
    */
    
    protected static function isFileExists($wwwPath){
        return file_exists($_SERVER['DOCUMENT_ROOT'].$wwwPath);
    }
    public function __construct($subdir,$name) {
        /*
        if (! self::isStaticInitComplete()){
            throw new Exception("You must call static init first");
        }
        */
        $this->subdir = $subdir;
        $this->filename = $name;
        $this->sizes =  PictureSize::getStandardSizes();
    }
    
    protected function getBaseFilename($size){
        // size not used here but need for overriding
        return $size->getDir().DIRECTORY_SEPARATOR.$this->filename;
    }
    
    protected function getWwwPrefix(){
        return self::$dir.$this->subdir;
    }
    
    protected function getExtension(){
        return ".".self::$ext;
    }
    
    protected function getWwwPath($size){
        $sizeDir = $size->getDir();
        foreach (self::$extensions as $tryExt){
            $filename = $this->getWwwPrefix().'/'.$sizeDir.'/'.$this->filename.'.'.$tryExt;
            //print $filename;
            if (self::isFileExists($filename)){
                $this->ext = $tryExt;
                return $filename;
            }
        }
        return "";
    }

    public function getSmall(){
        return $this->getWwwPath(PictureSize::Small());
    }
    
    public function getNormal(){
        return $this->getWwwPath(PictureSize::Normal());
    }

    private function getImg($path,$alt){
        return "<img src='$path' alt='$alt'>";
    }

    public function getImgSmall($alt =''){
        return $this->getImg($this->getSmall(), $alt);
    }
    
    public function getImgNormal($alt =''){
        return $this->getImg($this->getNormal(), $alt);
    }

    
}
