<?php

namespace gan4x4\Market\Picture;
use gan4x4\Market\Validator;
use Exception;

class PictureCollection implements Validator{
    private $data;
    private $max = 1;
    private static $fieldName = 'image_';
    private static $fieldDeletePerfix = 'delete_';
    private $error = '';
    
    
    public function __construct($max = 1) {
        $this->setMaxPic($max);
    }
    
    public static function isPicUploaded($inputPart){
        return isset($inputPart['name']) && 
        $inputPart['name'] != '' &&
        isset($inputPart['error']) &&
        $inputPart['error'] == UPLOAD_ERR_OK;
    }
    
    public function setMaxPic($max){
        $this->max = $max;
    }
    
    public function getSize(){
        return count($this->data);
    }
    
    public function add($picture){
        if (count($this->data) < $this->max){
            $this->data[] = $picture;
        }
        else{
            throw new Exception("Collection is full");
        }
    }
    
    private static function getHtmlFieldName($num){
        return self::$fieldName.$num;
    }
    
    private static function getHtmlDeleteFieldName($num){
        return self::$fieldDeletePerfix.self::getHtmlFieldName($num);
    }
    
    public function getLoadHtml(){
        $output='';
        $i= 0;
        //print "XXX";
        foreach($this->data as $pic){
            $field = self::getHtmlFieldName($i);
            $fieldDelete = self::getHtmlDeleteFieldName($i);
            $output .= $pic->getLoadHtml($field,$fieldDelete)."<br>\n";
            $i++;
        }
        return $output;
    }
    
    public function Validate($input = false){
        $i= 0;
        //var_dump($input);
        foreach($this->data as $pic){
            $field = self::getHtmlFieldName($i);
            if (isset($input[$field]) && self::isPicUploaded($input[$field])){
                $image = new Image($input[$field]['tmp_name']);
                if (! $pic->Validate($image)){
                    $this->error = $pic->getValidateErrorMessage();
                    return false;
                }
            }
            $i++;
        }
    }
    
    public function createFilesOnDisk($input,$crop = true){
        $i= 0;
        //var_dump($input);
        foreach($this->data as $pic){
            $field = self::getHtmlFieldName($i);
            if (isset($input[$field]) && self::isPicUploaded($input[$field])){
                //var_dump($input[$field]);
                //print "<hr>";
                $image = new Image($input[$field]['tmp_name']);
                $image->selfAutoCrop();
                //$pic->validateImage($image);
                $pic->createThumbs($image);
            }
            //$pic->$input('p_'.$i)."<br>\n";
            $i++;
        }
    }
    
    public function deleteFilesOnDisk(){
        foreach($this->data as $pic){
            $pic->delete();
        }
    }
    
    public function deleteSelectedPictures($input){
        $i= 0;
        foreach($this->data as $pic){
            $fieldDelete = self::getHtmlDeleteFieldName($i);
            if (isset($input[$fieldDelete])){
                $pic->delete();    
            }
        }
    }
    
    
    public function getFirst(){
        if (! is_object($this->data[0])){
            throw new Exception("Collection empty");
        }
        return $this->data[0];
    }

    public function getValidateErrorMessage() {
        return $this->error;
    }

    /*
    public function validate($input){
        $picture=$this->getFirst();
        $picture->validate($input);
    }
     * 
     */
}
