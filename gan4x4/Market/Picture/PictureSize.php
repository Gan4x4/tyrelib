<?php

namespace gan4x4\Market\Picture;



class PictureSize {
    const PIC_SMALL = 'pic_small';
    const PIC_NORMAL = 'pic_normal';
    const PIC_MEDIUM = 'pic_medium';
    const PIC_BIG = 'pic_big';
    
    const WIDTH = 0;
    const HEIGTH = 1;
    
    protected static $dir4size=array(self::PIC_SMALL=>'small',self::PIC_MEDIUM=>'medium',self::PIC_NORMAL=>'normal');
    protected static $picValidSizes=array(
        self::PIC_SMALL=>array(0,80),
        self::PIC_MEDIUM=>array(0,120),
        self::PIC_NORMAL=>array(300,0),
        self::PIC_BIG=>array(0,0));
    
    private $size = null;
    private $h = null;
    private $w = null;
    private $dir = null;
    
    
    public static function Small(){
        return new static(self::PIC_SMALL);
    }
    
    public static function Medium(){
        return new static(self::PIC_MEDIUM);
    }
    
    public static function Normal(){
        return new static(self::PIC_NORMAL);
    }
    
    public static function Big(){
        return new static(self::PIC_BIG);
    }
    
    public static function getStandardSizes(){
        return array(
                self::PIC_SMALL=>self::Small(),
                self::PIC_NORMAL=>self::Normal(),
                self::PIC_MEDIUM=>self::Medium()
            );
    }
    
    function __construct($size ,$w = null,$h = null,$dir = null) {
        $this->size = $size;
        $this->w = $w;
        $this->h = $h;
        $this->dir = $dir;
    }

    public function getWidth(){
        return $this->w == null ? self::$picValidSizes[$this->size][self::WIDTH] :$this->w;
    }
    
    public function getHeigth(){
        return $this->h == null ? self::$picValidSizes[$this->size][self::HEIGTH] :$this->h;
    }
    
    public function getDir(){
        return $this->dir == null ? self::$dir4size[$this->size] :$this->dir;
    }

    public function getId(){
        return $this->size;
    }


    
    
    


    //put your code here
}
