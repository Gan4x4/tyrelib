<?php


namespace gan4x4\Market\Picture;

class WhiteCanvasResizablePicture extends EditablePicture{
    
    
    private function validateImageSizes($image){
        // check sizes   
        $size = $this->sizes[PictureSize::PIC_NORMAL];
        $w = $size->getWidth(); 
        if ( $w > 0 && $w > $image->getWidth()){
            $this->error = "Ширина изображения должна быть не менее $w px. ";
        }
            
        $h = $size->getHeigth(); 
        if ( $h > 0 && $h > $image->getHeigth()){
            $this->error = "Высота изображения должна быть не менее $р px. ";
        }
    }
    
    public function Validate($image = false){
        $this->validateImageSizes($image);
        if (! $image->isCornersWhite()){
            $this->error = 'Изображение должно иметь белый фон! ';
        }
        return empty($this->error);
    }
}
