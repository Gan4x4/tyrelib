<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace gan4x4\Market;
use Exception;

class PriceCalculator
{
    
    const  BY_VALUE = 0;
    const  BY_INPUT = 1;
    const  BY_SELL = 2;
    private $good = null;
    private $parser = null;
    private $offerInfo = '';
    private $sellPrice = 0;
    private $inputPrice = 0;
    private $newOptPrice = 0;
    private $newPrice = 0;
    private $percent = 0;
    private $fix = 0;
    private $onlyUp = false;
    private $supplierFilter = false;
    private $maxWait = 0;
    private $minProfit = 0;
    
    
    public static function roundPrice($price)
    {
        $x=floor($price/50);
        return $x*50;
    }
    function __construct($id,$percent)
    {
        $this->good = Product::createTypedProduct($id);
        $this->percent= $percent;
        //die("GGG ".$this->good->getPrice());
    }
    
    function getParser()
    {
        if ($this->parser == null){
            $this->parser = new PriceParser($this->good);
        }
        return $this->parser;
    }
    
    function getGood(){
        return $this->good;
    }
    
    
    private function reset()
    {
        //print "Reset";
        $this->offerInfo = ''; 
        $this->inputPrice = 0;
        $this->sellPrice = 0; 
    }
    
    public function setOnlyUp($value){
        $this->onlyUp = $value;
    }
    
    public function setMaxWait($value){
        $this->maxWait = $value;
    }
    
    public function setMinProfit($value){
        $this->minProfit = $value;
    }
    
    // Constant that added (if negative substracted from price)
    public function setFix($value){
        $this->fix = intval($value);
    }
    
    public function getFix(){
        return $this->fix;
    }
    
    /*
    public function supplier($namePattern)
    {
        if (empty($namePattern)) return true;
        if (strpos($this->getOfferInfo(), $namePattern) !== false ) return true;
        return false;
    }
     * 
     */
    
    public function calculateBy($type)
    {
        switch ($type) {
            case self::BY_VALUE:
                $this->calculatePriceByValue();
                break;
        case self::BY_INPUT:
                $this->calculatePriceByInput();
                break;
            
        case self::BY_SELL:
                $this->calculatePriceBySell();
            break;
        
            default:
                throw new Exception("Invalid calculate type");
                break;
        }
    }
    
    private function getScale(){
        return $this->percent/100;
    }
    
    protected function caclulateOpt()
    {
        $newOptPrice = self::roundPrice($this->getPrice()*0.9);
        $realOptPrice = $this->checkMinimalProfit($newOptPrice);
        $this->newOptPrice = self::roundPrice($realOptPrice);
    }
    
    protected function setNewPrice($np)
    {
        if ($this->onlyUp == false || $np > $this->getOldPrice()){
            $this->newPrice = self::roundPrice($np);
        }
        else{
            $this->newPrice = $this->getOldPrice();
        }
            
    }
    
    
    function calculatePriceByValue()
    {
        $oldPrice = $this->getOldPrice();
        $newPrice = $oldPrice + $oldPrice*$this->getScale()+$this->getFix();
        $finalPrice = $this->checkMinimalProfit($newPrice);
        $this->setNewPrice($finalPrice);
        
        if ($this->percent > 0 || $this->getFix() != 0){
            $this->caclulateOpt();
        }
        return true;
    }
    
    
    
    private function fillFields(){
        $this->reset();
        $parser = $this->getParser();
        $this->inputPrice = $parser->getInputPrice($this->supplierFilter,$this->maxWait);
        $this->sellPrice = $parser->getSellPrice();
        $this->offerInfo =$parser->getInputPriceInfo();
        
    }
    
    function calculatePriceByInput()
    {
        $this->fillFields();
        if ($this->getInputPrice() == 0){
            $this->percent = 0;
            $this->calculatePriceByValue();
            return false;
        }
        $newPrice = $this->getInputPrice() + $this->getInputPrice()*$this->getScale()+$this->getFix();
        $finalPrice = $this->checkMinimalProfit($newPrice);
        $this->setNewPrice($finalPrice);
        $this->caclulateOpt();
        return true;
    }
    
    
    function calculatePriceBySell()
    {
        $this->fillFields();
        if ($this->getSellPrice() == 0){
            $this->percent = 0;
            $this->calculatePriceByValue();
            return false;
        }
        
        $newPrice = $this->getSellPrice() + $this->getSellPrice()*$this->getScale()+$this->getFix();
        $finalPrice = $this->checkMinimalProfit($newPrice);
        $this->setNewPrice($finalPrice);
        $this->caclulateOpt();
        return true;
    }
    
    private function checkMinimalProfit($newPrice){
        $input = $this->getInputPrice();
        if ( $this->minProfit &&
            $input &&
            $newPrice < ($input + $this->minProfit) ){
            return $input + $this->minProfit;
        }
        return $newPrice;        
    }
    
    function getOfferInfo()
    {
        return $this->offerInfo;
    }
    
    function getInputPrice()
    {
        
        return $this->inputPrice;
        
    }
    
    function getSellPrice()
    {
        //var_dump($this->sellPrice);
        //print "<br>";
        return $this->sellPrice;
    }
    
    function getOldPrice()
    {
        //var_dump($this->good);
        return $this->good->getPrice();
    }
    
    function getOldOptPrice()
    {
        return $this->good->getOptPrice();
    }

    function getPrice()
    {
        return $this->newPrice;
    }
    
    function getOptPrice()
    {
        
        if ($this->getOldOptPrice() == 0){
            $this->caclulateOpt();
        }
        
        if ($this->newOptPrice > 0){
            return $this->newOptPrice;
        }
        
        //if ($this->getOldOptPrice() == 0){
        //    return self::roundPrice($this->getPrice()*0.9);
        //}
        
        return $this->getOldOptPrice();
        
        //if ($this->getOldOptPrice() == 0){
        //    return $this->getPrice()*0.9;
        //}
        
    }
    
    function getRound()
    {
        //$r = $this->getScale();
        //print "R: ".$r;
        return ($this->percent != 0);
    }
    
    
    public function setSupplierFilter($supplier_filter){
        $this->supplierFilter = $supplier_filter;
    }
    
}
