<?php

namespace gan4x4\Market;
use gan4x4\Market\PriceCalculator;

class PriceChanger{
    
    private $goods = null;
    private $priceChange;
    private $percent = 0;
    private $fix = 0;
    private $only_up = false;
    private $reset_sale = false;
    private $supplier_filter = '';
    private $wait = 0;
    private $min_profit = 0;
    public $hidePromo = true;
    public $bootstrap = true;
    
    function __construct($request = false) {
        if ($request == false){
            $request = new Request();
        }
        
        $this->percent = $request->getIntField('percent');
        $this->fix = $request->getIntField('fix');
        $this->wait = $request->getIntField('wait');
        $this->priceChange = $request->getIntField('price_change_type');
        $this->only_up = $request->getIntField('only_up');
        $this->min_profit = $request->getIntField('min_profit');
        if (! $this->hidePromo){
            $this->reset_sale = $request->getIntField('reset_sale');
        }
        $this->supplier_filter = $request->getStringField('supplier_filter');
        
    }
    
    static function round_price($x){
        $x=floor($x/50);
        return $x*50;
    }
    
    private static function getAdd($price,$base)
    {
        if ($price > 0 && $base > 0){
           $absolut = $price - $base;
           $add = round(100*($price - $base)/$base,1);
           
           if ($add < 4){
               $add = "<div style='display:inline-block; padding: 5px 25px; font-size: x-small;'>(<span style='color: red; '> $add% <br> $absolut p.</span>)</div>";
           }
           else{
               $add = "<div style='display:inline-block; padding: 5px 2px; font-size: x-small;'>( $add% <br> $absolut p.) </div>";
           }
       }
       else{
           $add = '';  
       }
       return $add;
    }
    
    private static function show_price2($oldPrice,$newPrice,$name,$basePrice,$round = true)
    {
        if ($newPrice == 0){
            $newPrice = $oldPrice;
        }
        if ($round){
            $roundedNewPrice = self::round_price($newPrice);
        }
        else{
            $roundedNewPrice = $newPrice;
        }

        if ($oldPrice != $roundedNewPrice){
            $style = "style='color: red'";
        }
        else{
            $style = '';
        }
        
       
        return  $oldPrice." р. ".self::getAdd($oldPrice,$basePrice)."-> <input $style type='edit' size=4 name='$name' value='$roundedNewPrice'> ".self::getAdd($roundedNewPrice,$basePrice)."";
    }
    
    
    public function showFormFields(){
        
       print 'Цена <select name="price_change_type">';
                $priceChangeTypes = array(
                    PriceCalculator::BY_VALUE=>"Изменить цену на %",
                    PriceCalculator::BY_INPUT=>"Закупка +/-  %",
                    PriceCalculator::BY_SELL=>"РРЦ +/-  %",
                );
                print select_box_body($priceChangeTypes,$this->priceChange);
        print '</select>';
            
        print 'Процент <input type ="text" name ="percent" value="'.$this->percent.'" size="2"> % ';
        print "<br>\n";
        print 'Добавить константу <input type ="text" name ="fix" value="'.$this->fix.'" size="2"> р. <br>';

        $checked = $this->only_up ? "CHECKED" : '';
        print ' <input type ="checkbox" name ="only_up" '.$checked.'> Только повышать <br>';
        if (! $this->hidePromo){
            $checked = $this->reset_sale ? "CHECKED" : '';
            print '<input type ="checkbox" name ="reset_sale" > '.$checked.' Исключить из акций  <br>'; 
        }
        print 'Маска поставщика <input type ="text" name ="supplier_filter" size="50" maxlength="50"  value="'.$this->supplier_filter.'" > <br> ';
        $wait = $this->wait ? $this->wait : '';
        print "Срок поставки не более <input type='text' name='wait' value='$wait' size='2'> дн.<br>";    
        print "Минимальная наценка. (Не используется при изменении цены на %)";
        print '<input type ="text" name ="min_profit" size="10" maxlength="10"  value="'.$this->min_profit.'" > <br> ';
    }
    
    function setPriceChangeBy($priceChange){
        $this->priceChange  = $priceChange;
    }
    
    function setPercent($percent){
       $this->percent  = $percent;
    }

    
    
    function validateData()
    {
        /*
        if ($this->priceChange == PriceCalculator::BY_INPUT && $this->percent<14){
            return "Нельзя установить наценку ниже 14%. Если нарушить это правило, то маржа при оптовых продажах будет менее 4%";
        }
*/
        if ($this->priceChange == PriceCalculator::BY_SELL && $this->percent<-50){
            return "Нельзя торговать с таким убытком!";
        }

    }
    
    static function bootstrapStockPill($id,$amount){
        $stock = '<ul class="nav ">
                            <li name="stockPill" data="'.$id.'" >
                                <a href="javascript:void(0)">
                                    остатки 
                                    <span name="stock" class="badge">
                                        '.$amount.'
                                    </span>
                                </a>
                            </li>
                        </ul>';
        
        return $stock;
    }
    
    
    function getStockLink($id,$amount){
        if ($this->bootstrap){
            $stock = self::bootstrapStockPill($id,$amount);
        }
        else{
            $stock = "<a href = '/market/engine/orders/gginfo.php?id=$id' target='blank'>$amount</a>";
        }
        return $stock;
    }
    
    function showPriceChangeTable($goods,$update)
    {
        global $paths;
        if ($update) {
            $stl = "style='background-color : #FCC7C7' ";
        }
        $edit_butt = "";
        print "<table class='table' $stl cellspacing =8 >";
        print "<tr><td>#</td><td>Товар</td>";
        if (! $this->hidePromo){
            print '<td>акции</td>';
        }
        print "<td style='min-width : 19em;'>цена розн.</td><td style='min-width : 19em;'>цена опт.</td><td>закупка.</td><td style='min-width : 8em;' >РРЦ.</td><td>Поставщик</td><td>Остатки</td></tr>";
        $i =1;
        foreach ($goods as $line){
            $calculator = new PriceCalculator($line['id'],$this->percent);
            $calculator->setFix($this->fix);
            $calculator->setOnlyUp($this->only_up);
            $calculator->setSupplierFilter($this->supplier_filter);
            $calculator->setMaxWait($this->wait);
            $calculator->setMinProfit($this->min_profit);
            $calculator->calculateBy($this->priceChange);
            if (! $update){
                $edit_butt = '<a href="/market/engine/orders/catalog/product.php?action=2&ret_url='.urlencode($_SERVER['REQUEST_URI']).'&id='.$line['id'].' " target="blank"><img src= "'.$paths['market_local'].'/design/edit.gif"></a>';
            }
            print "<tr>";
            
            $goodId = $calculator->getGood()->Id();
            $enabled = '<input type ="checkbox" name ="'.$goodId.'_enabled" checked value="1" >';
            print "<td>".$enabled."</td> ";
            print "<td>".$line['name']." ".$edit_butt."</td> ";
            
            if (! $this->hidePromo){
                if ($line['reserv1'] == 2 ) {
                    $off = "скидка";
                }
                else{
                    $off = "";
                }
                print "<td>".$off."</td> ";
            }
            $inputPrice = $calculator->getInputPrice();  
            
            $sellPrice = $calculator->getSellPrice();
            $round = $calculator->getRound();
            $hiddenInput = "<input type='hidden' name='".$calculator->getGood()->Id()."_oldPrice' value='".$calculator->getOldPrice()."'>";
            
            print "<td>".$hiddenInput.self::show_price2( $calculator->getOldPrice(),$calculator->getPrice(),$calculator->getGood()->Id()."_price1",$inputPrice,$round)."</td> ";
            print "<td>".self::show_price2( $calculator->getOldOptPrice(),$calculator->getOptPrice(),$calculator->getGood()->Id()."_price2",$inputPrice,$round)."</td> ";

            $ip = $inputPrice == 0 ? '---' : $inputPrice." р.";
            print "<td>".$ip."</td>";

            $sp = $sellPrice == 0 ? '---' : $sellPrice." р.".self::getAdd($sellPrice,$inputPrice);
            print "<td>".$sp."</td>"; //РРЦ
            $stock = $this->getStockLink($line['id'],$line['amount_d']);
            print "<td>".$calculator->getOfferInfo()."</td>";
            print "<td>".$stock."</td>";
            print "</tr>\n";
            $i++;
        }
        print "</table>";

        
     
        //print "</div>";

            /*
         $error = validateData($priceChange,$percent);
            if ($error == false){
                print "<input type='button' value='Обновить' onClick='mainform.update.value=1; mainform.submit()'>";
            }
            else{
                print $error;
            }
        */

    }
    
    
    function updatePrices($input,$sites){
        print "<br>";
        $prices = array();
        foreach($input as $key=>$value){
            
            $goodId = substr($key, 0, 6);
            if (strlen($goodId) < 6 || ! isset($input[$goodId.'_enabled'])){
                continue;
            }

            if (strpos($key, '_price1') !== false){
                $prices[$goodId]['price1'] = $value;
            }elseif (strpos($key, '_price2') !== false){
                $prices[$goodId]['price2'] = $value;
            }
            elseif (strpos($key, '_oldPrice') !== false){
                $prices[$goodId]['old_price'] = $value;
            }
        }
    
    //var_dump($prices);     
        
        foreach ($sites as $value){
            if ($value['enabled'] ){
                connect_to_site($value['num']);
                print "Update site :".$value['name']." <br>";
                $afr = 0;
                foreach($prices as $id=>$prs){
                    $newPrice1 = $prs['price1'];
                    $newPrice2 = $prs['price2'];
                    $oldPrice = $prs['old_price'];
                    if ($this->reset_sale){
                        $resetAdd = ', reserv1 = 0 ';
                    }else{
                        $resetAdd ='';
                    }
                    
                    if ($oldPrice != $newPrice1){
                        $sqlUpdateOldPrice = "old_price = $oldPrice,";
                    }
                    else{
                        $sqlUpdateOldPrice = '';  
                    }
                    $query = "UPDATE goods SET $sqlUpdateOldPrice price1 = $newPrice1 , price2 = $newPrice2 $resetAdd WHERE id='$id' ";
                    //print $query."<br>";
                    $afr++;
                    $result = mysql_query($query) or die(mysql_error().$query);
                }
                print "Обновлено $afr позиций <hr>";
            }
            //error_log("Price _update : ".$query."\n",3,$paths['log']."price_update.log");
        }    
        
    }
    
}
