<?php

namespace gan4x4\Market;

class PriceParser {
    
    const BY_ID = 0;
    const BY_MODEL_AND_BRAND = 1;
    const BY_MODEL_OR_BRAND = 2;
    const BY_SIZE_ONLY = 3;
    const BY_DIRTY_SIZE = 4;
    const DAY = 86400;
    const SUPPLIER_NOT_FOUND = -1;
    
    private static $fields = 'market_supp_prices.price_wait as price_wait, market_supp.wait as wait, market_supp_prices_data.id AS id, market_supp_prices.change_date AS date, market_supp_prices_data.name AS name, market_supp_prices_data.brand AS brand, market_supp_prices_data.model AS model, market_supp_prices_data.sell_price AS sell_price, market_supp_prices_data.vendor_id AS vendor_id, market_supp_prices_data.our_price AS our_price, market_supp_prices_data.amount AS amount, market_supp.name AS sname, market_supp.id AS supp_id, market_supp.phone AS phone, market_supp_prices.name AS pname, market_supp_prices_data.supp_id AS supplier_good_id';
    private static $nameField ='market_supp_prices_data.name';
    private static $vendorIdField ='market_supp_prices_data.vendor_id';
    private static $OurIdField ='market_supp_prices_data.our_id';
    
    private $veryOld = 45; // Days
    private $sellPerDay = 2;
    private $data = array();
    private $g_info_data = array(); // Результаты звонков к поставщикам
    private $deliveryDelay = PHP_INT_MAX;
    //private $callNone = false; // Самый последний заказ был отвечен как "НИГДЕ НЕТ"
    //private $statData = null;
    private $dbList = array();
    private $good;
    
    private $cheapestOfferLine = null;
    
    
    
    public function __construct($good,$sites = null) {
        if ( ! is_object($good) || ! $good->id()){
            throw new \Exception("Bad good: ".var_export($good,true));
        }
        $class = get_class($good);
        if ($class == 'gan4x4\Market\Good'){
            throw new \Exception("Unsupported class : ".$class);
        }
        $this->good = $good;
        $this->initDbList($sites);
        $this->Reset();
        
    }
    
    private function initDbList($sites){
        if ($sites == null){
            $this->dbList[]= \System::getDb();
            //print "A";
        }else{
            
            $this->dbList = \System::siteList2DbList($sites);
            //print "B";
        }
        //var_dump($this->dbList);
    }
    
    private function getAge($line){
        $loadDate = strtotime($line['date']);
        $age = round((time()-$loadDate)/self::DAY);
        return $age;
    }
    
    
    private function isFirstNewer($info,$line){
        //$ginfo = $this->getGinfo();
        if (empty($info)) {
            return false;
        }
        //$gDate = $this->g_info_data['change_date']; 
        $gInfoDate = strtotime($info['date']);
        $lineLoadDate = strtotime($line['date']);
        return $gInfoDate > $lineLoadDate;
    }
    /*
    private function analyzeGinfo($amount,$line){
        $resultAmount = $amount;
        $ginfo = $this->getGinfo();
        if ($this->isFirstNewer($ginfo,$line)){
            if ($ginfo['amount'] == 0){
                // All goods selled
                $resultAmount = 0;
            }
            elseif ($ginfo['amount'] > $amount){
                // We have more actual information
                $resultAmount = $ginfo['amount'];
            }
        }
        return $resultAmount;
    }
    
    private function analyzeStat($amount,$line){
        $resultAmount = $amount;
        $gInfoAmount = $this->g_info_data[$line['supplier']];
        
        
        $stat = $this->getOrdersStat();
        if ($this->isFirstNewer($stat,$line)){
            if ($stat['supplier'] == -1){
                // All goods selled
                $resultAmount = 0;
            }
            elseif ($stat['supplier'] > 0 && $stat['amount'] >$amount ){
                // We have more actual information
                $resultAmount = $stat['amount'];
            }
        }
        return $resultAmount;
    }
    */
    private function getInfoFromStat($line){
       // $result = $line['amount'];
        //var_dump($this->g_info_data);
        if (isset($this->g_info_data[self::SUPPLIER_NOT_FOUND])){
            $notFound = $this->g_info_data[self::SUPPLIER_NOT_FOUND];
            // Exists order where supplier = -1 and not ginfo uploaded after in
            if ($this->isFirstNewer($notFound,$line)){
                //print "RESET";
                return 0;
            }
        }
        
        $ginfo = ( isset($line['supplier']) && isset($this->g_info_data[$line['supplier']]) ) ? $this->g_info_data[$line['supplier']] : '';
        if (! empty($ginfo) && $this->isFirstNewer($ginfo,$line) ){
            return $ginfo['amount'];
        }
        return false;
    }
    
    
    private function getAmountFromLine($line){
        return intval($line['amount']);
    }
    
    
    private function getInputPriceFromLine($line){
        //var_dump($line);
        //print "</hr>";
        if (intval($line['our_price']) != 0){
            return intval($line['our_price']);
        }
        return false;
    }
    
    private function getSellPriceFromLine($line){
        
        if (intval($line['sell_price']) != 0){
             //   var_dump($line);
       // print "</hr>";
            return intval($line['sell_price']);
        
        }
        return false;
    }
    
    
    private function getHeuristicAmountFromLine($line,$useAge = true,$coeff = 1){
        $initialAmount = intval($line['amount']);
        if ($useAge && ($initialAmount > 3)){
            $age = $this->getAge($line);
            $age--;
            $selled = $age > 1 ? ($age-1)*$this->sellPerDay : 0;
            $amount = $initialAmount > $selled ?  $initialAmount - $selled : 0;
            
        }
        else{
            $amount = $initialAmount;
        }
        
        $weighedAmount = round($amount*$coeff);
        $callAmount = $this->getInfoFromStat($line,$weighedAmount);
        
        $finalAmount = $callAmount === false ? $weighedAmount : $callAmount;
        /*
        if ($callAmount == 0){
            $finalAmount = 0;
        }
        elseif($callAmount > $weighedAmount){
            $finalAmount = $callAmount; 
        }else{
            $finalAmount = $weighedAmount;
        }
        */
        
        //$amount = $this->analyzeGinfo($amount,$line);
        //$amount = $this->analyzeStat($amount,$line);
        //print " AMT = ".$amount." =>".round($amount*$coeff);
        return $finalAmount;        
    }
    
    
    private function getPartCoeff($part){
        switch ($part) {
            case self::BY_ID:
                return 1;
            case self::BY_MODEL_AND_BRAND:
                return 0.9;
            default:
                return 0;
        }
    }
    
    
    private function getCheapestLine($supplierNamePattern ,$waitFilter, $partIdList = array(self::BY_ID,self::BY_MODEL_AND_BRAND)){
        $min = PHP_INT_MAX; //
        $result = false;
        foreach($partIdList as $partId){
            if ($this->data[$partId] == null){
                continue;
            }
            
            $part = $this->data[$partId];
            
            
            foreach ($part as $line){
                //var_dump($line);
                
                
                if ($supplierNamePattern != false && 
                    strpos($line['sname'], $supplierNamePattern) === false){
                    //print "Bypass <b>GGGG</b>".$line."<hr>";
                    continue;
                }
                
                if ($waitFilter > 0 && self::getDeliveryDelayFromLine($line) > $waitFilter){
                    continue;
                }
                        
               $price = $this->getInputPriceFromLine($line);
               
                if ($price == 0){
                   // Input price not set, but may be set sell (roznicha)
                    $price = $this->getSellPriceFromLine($line);
                }
                
                
               if ($price > 0 && $price < $min ){
                   $min = $price; 
                   $result = $line;
                   
               }
            }
        }
        if ($min == PHP_INT_MAX){
            return false;
        }
        return $result;
    }
    
    private static function getDeliveryDelayFromLine($line)
    {
        //var_dump($line);
        $d = max(intval($line['wait']), intval($line['price_wait']));
        return $d;
    }
    
    public function getDeliveryDelay()
    {
        if ($this->deliveryDelay == PHP_INT_MAX){
            return 0;
        }
        return $this->deliveryDelay;
    }
   
    
    
    private function getTotalAmount($absolute = false,$partIdList = array(self::BY_ID,self::BY_MODEL_AND_BRAND)){
        $total = 0;
        // Read statistical info
        if (! $absolute){
            $this->getGinfo();
            $this->getOrdersStat();
        }
        
        
        foreach($partIdList as $partId){
            if ($this->data[$partId] == null){
                continue;
            }
            $part = $this->data[$partId];
            foreach ($part as $line){
                if ($absolute){
                    $total += $this->getAmountFromLine($line);
                    
                    //$deliveryDate = $currDate > $deliveryDate ? $currDate : 
                }
                else{
                    $total += $this->getHeuristicAmountFromLine($line,true,$this->getPartCoeff($partId));
                }
                
                $currDelay = self::getDeliveryDelayFromLine($line);
                if ($currDelay < $this->deliveryDelay){
                    $this->deliveryDelay = $currDelay;
                }
            }
        }
        return round($total);
    }
    
    private static function getQuery($conditions,$limit = false){
        if ($limit !=false){
            $limitPart = '  LIMIT '.intval($limit);
        }
        else{
            $limitPart = '';
        }
        $query = "SELECT ".self::$fields." FROM market_supp_prices_data, market_supp_prices, market_supp 
                WHERE market_supp_prices.supplier_id = market_supp.id AND market_supp_prices_data.price_id=market_supp_prices.id AND
                ($conditions)
                GROUP BY market_supp_prices_data.id ORDER BY date DESC, wait ASC, amount DESC $limitPart";
        return $query;
    }

    
    private function getSizePattern(){
        return $this->good->getSearchPattern(self::$nameField);

    }
    
    private function getBrandPattern(){
        return $this->good->getModel()->getBrand()->getSearchPattern(self::$nameField);
    }
    
    private function getModelPattern(){
        return $this->good->getModel()->getSearchPattern(self::$nameField);
    }

    public function getGood(){
        return $this->good;
    }
    
    public function getAbsoluteAmount(){
        return $this->getTotalAmount(true);    
    }
    
    public function calculateAmount(){
        $this->Reset();
        $this->searchById();
        $this->searchByBrandAndModel();
        // Remove old
        return $this->getTotalAmount();
    }
    
    
    public function getInputPriceInfo()
    {
        //return var_export($this->cheapestOfferLine,true);
        return $this->cheapestOfferLine['name']." ".
                $this->cheapestOfferLine['vendor_id']." ".
                $this->cheapestOfferLine['sname']." ".
               // $this->cheapestOfferLine['our_price']." р. ".
                $this->cheapestOfferLine['amount']." шт. ".
                $this->cheapestOfferLine['date'];
        
        
    }
    
    public function getSellPrice()
    {
        if ($this->cheapestOfferLine == null){
            return false;
        }
        if (intval($this->cheapestOfferLine['sell_price']) != 0){
            
            //if ($this->good->id() == 'du0275'){
            //print "<hr>";
            //var_dump($this->cheapestOfferLine);
            //}
             $price = intval($this->cheapestOfferLine['sell_price']);
            return $price;
        }
        return false;
    }
    
    private function getCheapestPrice()
    {
        $price = $this->getInputPriceFromLine($this->cheapestOfferLine);
        return $price;
        //if (! $price){
        //    $price = $this->getSellPriceFromLine($this->cheapestOfferLine);
        //}
        //return $price;
        
    }
    
    
    public function getInputPrice($supplierNamePattern = false, $wait = 0 ){
        if ($this->cheapestOfferLine == null){
            $this->searchById();
            $this->searchByBrandAndModel();
            $this->cheapestOfferLine = $this->getCheapestLine($supplierNamePattern,$wait);
            
        } 
        //var_dump("Cheapest ".var_export($this->cheapestOfferLine,true));
        if ($this->cheapestOfferLine == null){
            return false;
        }
        
       
        return $this->getCheapestPrice();
        //
        //                return $this->getCheapestPrice();
    }
    
    function isPriceLow()
    {
        $input = $this->getInputPrice();
        $current = $this->good->getPrice();
        if ($input > 0 && $current > 0){
            if ($input > $current*0.86){
                return true;
            }
        }
        
        $sell = $this->getSellPrice();
        if ($sell> 0 && $current > 0){
            // РРЦ меньше нашей 
            if ($sell > $current && ! $this->good->isDiscount()){
                return true;
            }
        }
        
        return false;
    }
    
    
    function getRecomendedPrice($koeff = 1.15){
        if ($this->isPriceLow()){
            return intval($this->getCheapestPrice()*$koeff);
        }
        return $this->good->getPrice();
    }
    
    function getRecomendedOptPrice($koeff = 1.05){
        if ($this->good->getOptPrice() < $this->getCheapestPrice()*$koeff){
            return intval($this->getCheapestPrice()*$koeff);
        }
        return $this->good->getOptPrice();
    }
    
    
    public function Search(){
        $this->Reset();
        $this->searchById();
        $this->searchByBrandAndModel();
        $this->searchByBrandOrModel();
        $this->searchBySizeOnly();
        $this->searchByDirtySize();
        $tmp = $this->getTotalAmount();
        //var_dump($tmp);
        return $tmp;
    }
    
    public function searchById(){
        $condition = self::$OurIdField." = '".$this->good->id()."'"; 
        $vendor_id = $this->good->getVendorId();
        $ids = explode(',', trim($vendor_id));
        if (count($ids) > 0 ){
            foreach($ids as $id){
                $clearId = trim($id);
                if (empty($clearId)){
                    continue;
                }
               // $condition .= ' OR '.self::$vendorIdField." LIKE '%".$clearId."%'"; 
               // 
               $condition .= ' OR '.self::$vendorIdField." = '".$clearId."'"; 
                //$condition .= ' OR '.self::$vendorIdField." LIKE '%".$vendor_id."%'"; 
            }
        }
        $condition = "( $condition ) AND (".$this->getBrandPattern().")";
        $query = self::getQuery($condition);
        //print $query;
        $data =  \System::run_query($query);
        $this->setData(self::BY_ID, $data);
        
        //return count($data);
    }
    
    
    
    
    public function searchByBrandAndModel(){
        //die();
        $sizePattern =  $this->getSizePattern();
        
        //die();
        $modelPattern = $this->getModelPattern();
        $brandPattern = $this->getBrandPattern();
        
        $query = self::getQuery("($sizePattern) AND ($modelPattern) AND ($brandPattern)");
        //print $query;

        $resultWithDust = \System::run_query($query);
        $modelsToSkip = $this->getIgnoreModelsNames($this->good->getModel());
        foreach ($resultWithDust as $line){
            $skip = false;
            foreach($modelsToSkip as $name){
                $fullName =mb_strtolower($line['name']); 
                $badModel = mb_strtolower($name);
                if (strpos($fullName,$badModel) !== false){
                //if (preg_match("/$badModel/i", $fullName) == 1){
                    $skip = true;
                    //print "<b> Ignore ". $fullName." by $badModel </b><br>";
                }
                else{
                    //print "<b> Include ". $fullName." by $badModel </b><br>";
                }
            }
            if (! $skip){
                $this->addUniqueLine($line,self::BY_MODEL_AND_BRAND);
            }
        }
    }
    
    
    public function searchByBrandOrModel()
    {
        $sizePattern =  $this->getSizePattern();
        $modelPattern = $this->getModelPattern();
        $brandPattern = $this->getBrandPattern();
        $query = self::getQuery("($sizePattern) AND (($modelPattern) OR ($brandPattern))");
        //print $query;
        $result = \System::run_query($query);
        $this->setData(self::BY_MODEL_OR_BRAND, $result);
    }
    
    
    public function searchBySizeOnly()
    {
        $sizePattern =  $this->getSizePattern();
        $query = self::getQuery(" $sizePattern ");
        //print $query;
        $result = \System::run_query($query);
        $this->setData(self::BY_SIZE_ONLY, $result);
    }
    
    public function searchByDirtySize()
    {
        $dirtySizePattern =  $this->good->getDirtySearchPattern(self::$nameField);
        if ($dirtySizePattern != false){
            $query = self::getQuery(" $dirtySizePattern ");    
            //print $query;
            $result = \System::run_query($query);
            $this->setData(self::BY_DIRTY_SIZE, $result);
        }
    }
    
    
    
    public function getLinesCount(){
        
        return count($this->data);
    }
    
    private static function isLinesEqual($line1,$line2){
        if ($line1['id'] == $line2['id']){
            return true;
        }
        $ids =array('supp_id','name','vendor_id','our_price','amount');
        
        foreach($ids as $id){
            if (trim($line1[$id]) != trim($line2[$id])){
                return false;
            }
        }
        
        return true;
        
    }
    
    private function addUniqueLine($lineToAdd,$partToInsert){
        if (! isset($lineToAdd['id'])){
            throw new Exception("Bad line");
        }
        $id = $lineToAdd['id'];
        foreach ($this->data as $part){
            if ($part == null){
                continue;
            }
            foreach ($part as $line){
                //if ($line['id'] == $id){
                //    return;
                //}
                if (self::isLinesEqual($line,$lineToAdd)){
                    return;
                }
            }
        }

        if ($this->getAge($lineToAdd) < $this->veryOld){
            $this->data[$partToInsert][$id] = $lineToAdd;
        }
    }
    
    private function setData($part,$data){
        //assert('isset($this->data[$part])'," part = ".$part." ".var_export($data,true));
        foreach ($data as $line){
            $this->addUniqueLine($line, $part);
        }
        //$this->data[$part] = $data;
    }
        
    
    public function getData($part){
        //assert('isset($this->data[$part])');
        //var_dump($part);
        //var_dump($this->data[$part]);
        return $this->data[$part];
    }
    
    public function Reset(){
        $this->data = array(self::BY_ID=>null,self::BY_MODEL_AND_BRAND=>null,self::BY_MODEL_OR_BRAND=>null,self::BY_SIZE_ONLY=>null,self::BY_DIRTY_SIZE=>null);
        $this->g_info_data = array();
        //$this->orders_info_data = null; 
    }
    
    function getIgnoreModelsNames($currentModel){
        $otherModels = $currentModel->getOtherModelsOfBrand();
        $currentModelNames = $currentModel->getSinonims();
        $dustNames = array();
        foreach ($otherModels as $dustModel){
            $dustNames = array_merge($dustNames,$dustModel->getSinonims());
        }
        $clearedList = array();
        
        foreach ($dustNames as $dustName){
            foreach ($currentModelNames as $goodName){
                $current = mb_strtolower(trim($goodName));
                $dust = mb_strtolower(trim($dustName));
                if ( $current == '' || $dust =='' || $current ==  $dust){
                     continue;
                }
                if (strpos($dust, $current) !== false ){
                //if (preg_match("/$current/i",$dust)){
                    if (! in_array($dust, $clearedList) ) {
                        //print "<hr> ".$goodName." vs ".$dustName;
                        $clearedList[] = trim($dustName);
                    }
                } 
            }
        }
        return $clearedList;
    }
    
    
    
    public static function freeSearch($string,$limit = 100){
        //$sizePattern =  $this->getSizePattern();
        $filteredInput = filter_var(trim($string),FILTER_SANITIZE_STRING);
        //$pattern = strtr($filteredInput, ' ', '%');
        //$pattern = self::$nameField." LIKE '%".$pattern."%'";
        
        $parts = explode(' ', $filteredInput);
        $trimmedParts =  array();
        foreach($parts as $part){
            if (empty($part)){
                continue;
            }
            $trimmedParts[] = trim($part);    
        }
        if (count($trimmedParts) <1) {
            return array();
        }
        //$majorParts = array($bolts,$size,$et);
        $patternName = \System::prepareNamesToSqlLike(array_unique($trimmedParts),self::$nameField,' AND ');
        
        $idLikePart = array();
        foreach ($trimmedParts as $part){
            if (strlen($part) > 3){
                $idLikePart[] = $part;
            }
        }
        
        if (count($idLikePart) > 0 ){
            $patternId = \System::prepareNamesToSqlLike(array_unique($idLikePart),self::$vendorIdField,' OR ');    
            $query = self::getQuery(" ($patternName) OR ($patternId) ",$limit);
        }
        else{
            $query = self::getQuery(" ($patternName) ",$limit);
        }
        
        
        //print $query;
        return \System::run_query($query);
        //$this->setData(self::BY_SIZE_ONLY, $result);
        
    }
    
    
    private function addGinfo($date,$amount,$supplier){
        if ($supplier == 0 ){
            // Неизвестно
            return;
        }
        $candidate = array();
        $candidate['date'] = $date; 
        $candidate['amount'] = $amount; 
        if (! isset($this->g_info_data[$supplier]) ){
            // New supplier, add
            //$this->g_info_data = array();
            //print "ADD LINE ";
            $this->g_info_data[$supplier] = $candidate;
            //var_dump($this->g_info_data);
            return;
        }
      
        // Update older
        foreach($this->g_info_data as $supp=>$data){
            if ($supp == $supplier && $this->isFirstNewer($candidate,$data)){
                $this->g_info_data[$supplier] = $candidate;
                return;
            }
        }
    }
    
    
    // Поиск по g_info заказа
    // Анализ последних запросов к постащикам
    private function getGinfo(){
        
        
        $good_id = $this->good->id();
        $query= "SELECT * FROM market_ginfo WHERE good_id = '".$good_id."' AND amount > 1 AND date(`time`) > DATE_SUB( NOW(), INTERVAL 7 DAY)  ORDER BY `time` DESC LIMIT 1 ";
        $result = \System::run_query($query);
        foreach ($result as $data){
            $this->addGinfo($data['time'],$data['amount'],$data['supplier_id']);
        }
    }
    
    public function getOrdersStat(){
        //if ($this->orders_info_data === null){
        //    $good_id = 
        //    $this->orders_info_data = array();
        //$i = 0;
        //var_dump($this->dbList);
        foreach ($this->dbList as $db){
            $query=" SELECT * FROM market_og WHERE good_id = '".$this->good->id()."' AND change_date >  DATE_SUB( NOW(), INTERVAL 7 DAY) and supplier <> 0 ORDER BY change_date DESC LIMIT 1";
            //print $query."->".var_export($db,true);
            $result = \System::run_query($query,$db);
            //print "<br> Print X ".var_dump($result);
            if (count($result) > 0 ){
                $data = $result[0];
                $this->addGinfo($data['time'],$data['amount'],$data['supplier']);
            }
            //$this->orders_info_data[]  = array_merge($this->orders_info_data,$data);
        //$i++;
        }
        //}
        //return $this->orders_info_data;
        //var_dump($this->orders_info_data);
    }
    
}
