<?php


namespace gan4x4\Market;
use \gan4x4\Market\ReadOnly\Catalog;
use \Exception;

abstract class Product extends Good 
{

        private static $productClasses = array('t'=>'Tyre','d'=>'Wheel','sa'=>'ShockAbsorber');
        private static $uniqueTuple = array('vendor_id','model_id','brand_id');
        protected $model;

        abstract function generateNewPerfix();
        
	public function __construct($id) 
	{
           $t_info = self::GetInfoTable($id);
           if (is_array($id) && isset($id['_type'])){
               $t_info = self::GetInfoTable($id['_type']);
           }

           if ($t_info == False){
                $more_fields = False;
           }
           else{	
                $more_fields = array(array($t_info,'id'));
           }

           // File Load here

           parent::__construct($id,'','',$more_fields);
	}

    private function getFullName(){
        $key = $this->getInfoTableName().'_name';
        return $this->get_field_value($key);
    }
        
    private function setFullName($newname){
        $this->set_field_value($this->getInfoTableName().'_name',$newname);
    }
        
    
    // Override
    function name(){
        $normalName = parent::name();
        if (empty($normalName)){
            return $this->get_field_value('vendor_id');
        } 
        return $normalName;
    }
    /*
    protected function getModel(){
        
    }
    */
    protected function genFullName($model){
        $full_name =  $model->getBrand()->name()." ".$model->name();
        return $full_name;
    }
    
    
    public static function createByRequest($input){
        $instance=parent::createByRequest($input);
        assert($instance->getPerfix() != '');
        try {
            $instance->getModelId();
        } catch (Exception $exc) {
            // model not set
            $request = new Request($input);
            $request->setField('_type',$request->getType());
            $id = $request->getId('model_id');
            if ($request->getId('model_id')>0){
                //$model = new Model($request->getId('model_id'),$request->getTypeTable());
                $model = Model::createModel($request->getId('model_id'),$request->getTypeTable());
                $instance->setModelId($model->id());
                if ($instance->get_field_value('model') == ''){
                    $instance->set_field_value('model',$model->name());
                }
                $instance->setBrandId($model->getBrand()->id());
                if ($instance->getFullName() ==  ''){
                    $name = $instance->genFullName($model);
                    $instance->setFullName($name);
                }
            }
            else{
                throw $exc;
                //print "Bad";
            }
        }
        
        return $instance;
    }
        
   public static function getClassByType($type){
       
        if (Catalog::isSimple($type)){
            $class = 'Good';
        }
        else{
            $class = Utils::findByPerfix($type,self::$productClasses);
        }
        if ($class == false){
           //throw new Exception("Bad product type ".$type);    
            $class = 'Good';
        
        }
       return __NAMESPACE__.'\\'.$class;
    }
    
        
    public static function createTypedProduct($id){
        $type = substr($id,0,2);
        try{
            $class = self::getClassByType($type);    
           // print "Class = ".$class;
            return new $class($id);
        } catch (Exception $ex) {
            // TO DO Create simple product
            throw $ex;
        }
        
        /*
        switch ($type) {
            case 't':
                return new Tyre($id);
            case 'd':
                return new Wheel($id);
            case 'sa':
                return new ShockAbsorber($id);
            default:
                // TO DO Create simple product
                return null;
        }
         */
    }
    
    // Override    
    public function getVendorId(){
        $vendorId = $this->getValue('vendor_id');
        return $vendorId;
    }
    
    protected function getInfoTableName(){
        //var_dump($this->linked_fields);
        $linked_tables = array_keys($this->linked_fields);
        if (isset($linked_tables[0]) && count($linked_tables) ==1){
            assert(strpos($linked_tables[0], '_info') !== false);
            return $linked_tables[0];    
        }
        throw new Exception("Info table not set");
    }
        
    // For info table only
    protected function getInfoTableKey($field){
        $key = $this->getInfoTableName().'_'.$field;
        //var_dump($key);
        //assert(isset($this->datalist[$key]),'Field not exist '.$key);
        if (! isset($this->datalist[$key])){
            //var_dump($this->datalist);
            throw new Exception("Bad key $key");
        }
        return $key;
    }
    
    protected function getValue($field){
        $key = $this->getInfoTableKey($field);
        $value = $this->get_field_value($key);
        return trim($value); 
    }
    
    protected function setValue($field,$value){
        $key = $this->getInfoTableKey($field);
        $this->set_field_value($key,trim($value));
    }
    
    
    public function getModel(){
        if (! $this->model){
            $type = $this->getPerfix();
            $modelClass = Model::getClassByType($type);
            $this->model = new $modelClass($this->getModelId());
        }
        $this->model->cloneDbList($this);
        return $this->model;
    }
    
    
    public function getModelId(){
        $id = $this->getValue('model_id');
        if (! $id) {
            throw new Exception("Model id not set ".var_export($this->datalist['id'],true));
        }
        assert($id,"Model id not set!");
        return $id;
    }
    
    
    public function setModelId($model_id){
        //die("MI".$model_id);
        //$info_table = $this->getInfoTableName();
        //$key = $this->getInfoTableKey('model_id');
        $key = 'model_id';
        if ($this->setValue($key,$model_id) == false);
        $this->model = null;
        //var_dump($this->datalist);
        ///assert($this->get_field_value($key) == $model_id);
    }
     
    
    public function setBrandId($brand_id){
        //$this->set_field_value('brand_id',$brand_id);
        $info_table = $this->getInfoTableName();
        $key = $info_table.'_brand_id';
        if (! $this->set_field_value($key,$brand_id)){
            throw new Exception("Key not set ".$key);
        }
        //var_dump($this->datalist);
        assert($this->get_field_value($key) == $brand_id);
        
        
    }
    
    
    protected function fillAutoFields(){
        
        if (! $this->id()){
            $newPerfix = $this->generateNewPerfix();
            $this->perfix = $newPerfix;
        }
        /*
        // Set opt price
        $optPrice = $this->get_field_value('price2');
        if ($optPrice == 0){
            $price = $this->get_field_value('price1');
            $newOptPrice = $price * 0.9;
            $this->set_field_value('price2',$newOptPrice);
        }
        */
        parent::fillAutoFields();
    }
    
    
    public static function GetInfoTable($id)
    {
        //print "XXX: ".var_dump($id);
        if (is_array($id)){
            if (isset($id['id']) && $id['id'] !=''){
                $id = $id['id'];
            }
            else{
                return False;
            }
        }
       
        $postfix = '_info';
        $tn='';

        if ((strlen($id) == 2) && ($id{0}.$id{1} == 'tl')) {
            return false;
        }
        if (\Db_objects_list::get_table_info($id{0}.$postfix)) {
            $tn = $id{0}.$postfix;
        }
        elseif (\Db_objects_list::get_table_info($id{0}.$id{1}.$postfix)){
            $tn = $id{0}.$id{1}.$postfix;
        }
        if ($tn == '') {
            return false;
        }
        return $tn;
    }

    public function getCatalogParts($file){
            $res = array();
            $fp = fopen ($file,"r");
            while ($data = my_getcsv ($fp, 1000, ";")){
                $code = $data[0];
                $info = $data[1];
                
                if (strlen($code) == 1 ){
                    $part = $info;
                }
                elseif (strlen($code) == 2 ){
                    $len = strlen($this->getPerfix());
                    if (substr($code,0,$len) != $this->getPerfix()){
                        // Bypass uncompatible 
                        continue;
                    }
                    $res[$code]=$part." :: ".$info;
                }
            }
            fclose($fp);
            asort($res);
            return $res;
	}
	
        
    // Override
    public function show_add_form($input = false,$back_url = '')    {
        $currPerfix = $this->getPerfix();
        //print $currPerfix;
        if (strlen($currPerfix)>1 && ($this->generateNewPerfix() != $currPerfix)){
            print "<b>Current id is invalid ".$this->generateNewPerfix()."</b>";
        }
        parent::show_add_form($input,$back_url);
        //$backUrl = $this->getReturnUrl();
        //print '<input type=button value = "Назад к списку" onClick="location.href=\''.$backUrl.'\'">';
    }   
    
    // Override
    public function getReturnUrl(){
        $type = $this->getPerfix();
        $allowedTypes = Model::getAllowedTypes();
        if (in_array($type,$allowedTypes)){
            $clearedType = $type; 
        }else{
            $clearedType = $type{0};
        }
        return  'productlist.php?model_id='.$this->getMOdelId().'&type='.$clearedType;
    }
    
    
    
    protected function changeId(){
        assert(false,"Depricated");
        $testPerfix = $this->generateNewPerfix();
        $currentPerfix = $this->getPerfix();
        assert($testPerfix != $currentPerfix);
        if ($testPerfix != $currentPerfix){
            assert($testPerfix{0} == $currentPerfix{0});
            print "<b> Need to change ID, its bad ...</b>";
            $oldId = $this->id();
            $this->perfix = $testPerfix;
            $newId = $this->getNewId();
            $lastQueryPart = "SET id = '$newId' WHERE id = '$oldId'";
            $queryUpdateGoods = "UPDATE `$this->table_name` ".$lastQueryPart;
            if ($this->runMultiDbQuery($queryUpdateGoods)){
                $queryUpdateInfo = "UPDATE `".$this->getInfoTableName()."` ".$lastQueryPart;
                if (! $this->runMultiDbQuery($queryUpdateInfo)){
                    throw new Exception("Info table not updated ".$queryUpdateInfo);
                }
                //$queryCarToGoodHGistory = "UPDATE `car_to_good_history` ".$lastQueryPart;
            }
            else{
                throw new Exception("Info table not updated ".$queryUpdateGoods);
            }
            //print $query1."<br>";
            //print $query2;
            $this->datalist['id']['value'] = $newId; 
        }
    }
    
    
    public function Save(){
        $success = parent::Save();
        if ($success){
            $model = $this->getModel();
            $modelAv = $model->getAv();
            $goodAv = $this->get_field_value('amount');
            if ( $goodAv == 0 && $modelAv == 1  ){
                $model->updateAv();
                $model->cloneDbList($this);
                $model->Save();
                //$tmp = $model->getAvGoodCount();
                //die("XX ".$tmp);
                //$model->setAv(1);
                //$model->Save();
            }
            elseif ( $goodAv != 0 && $modelAv == 0 ){
                print "Enable Model!<br>";
                $model->setAv(1);
                $model->cloneDbList($this);
                $model->Save();
            }
            $this->clearCache();
        }
        
        // test perfixChange
        //if ($this->generateNewPerfix() != $this->getPerfix()){
        //    $this->changeId();
        //}
        
        
        return $success;
    }
    
    public function clearCache($cacheFilesToDel =  false){
        $fn = Utils::get_cache_fn('product.htm','',"id=".$this->id());
        if (file_exists($fn)){
            $success = unlink($fn);
            assert($success,"Cache not cleared ".$fn);
        }
        $model = $this->getModel();
        $model->clearCache();
        
    }
    

    
    public function getDirtySearchPattern($field){
        return false;
    }
    
    
    public function dumpGood(){
        $tableKeys = array_keys(\Db_objects_list::get_table_info('goods'));
        $result = '';
        foreach($tableKeys as $key){
            $val = $this->get_field_value($key);
            if ($val == ''){
                $val = $this->datalist[$key]['default'];
            }
            $result .= $val.";";
        }
        return $result;
    }
    
    
    public function isHasDuplicate(){
        $table = $this->getInfoTableName();
        $data = $this->extactData($table);
        $fields = array_merge(self::$uniqueTuple, static::$uniqueTuple);
        //var_dump($fields);
        $pairs = self::getKeyValuePairListForSql($fields,$data);
        $where = \System::create_commas_list($pairs," AND ");
        $query = "SELECT * FROM $table WHERE ".$where;
        $queryResult = \System::run_query($query);
        if (count($queryResult) == 0){
            return false;
        }
        return $queryResult;
    }
    
    
    protected function setDefaultValues(){
        foreach($this->datalist as $key=>$value){
            if ( (! isset($this->datalist[$key]['value']) || $this->datalist[$key]['value'] == '' ) && 
                  isset($this->datalist[$key]['default'])  ){
                $this->datalist[$key]['value'] = $this->datalist[$key]['default'];
            }
        }
    }
    
}

?>
