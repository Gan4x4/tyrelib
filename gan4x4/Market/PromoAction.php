<?php
namespace gan4x4\Market;

// complex_db_object used as parent only for saving to multiple db
class PromoAction extends \complex_db_object implements Validator{
    public function __construct($id){
        parent::__construct($id,'market_promo_action');
    }
    
    public static function getAll($filter = ''){
        $rawData = \System::run_query('SELECT * FROM market_promo_action '.$filter);
        $objects = [];
        foreach ($rawData as $line){
            $objects[] = new static($line);
        }
        return $objects;
    }
    
    
    
    public static function getAllActionNamesAndId(){
        $promos = self::getAll();
        $ids = [];
        foreach($promos as $promo){
            $ids[$promo->id()] =  $promo->name();
        }
        return $ids;
        
    }
    
    public static function getActive(){
        return self::getAll(' WHERE `active` = 1 AND `start` <= CURDATE()  AND CURDATE() <= `finish` ');
    }
    
    public static function getActionByOrderId($orderId){
        if (empty($orderId)){
            return false;
        }
        $rawData = \System::run_query("SELECT * FROM market_promo_action WHERE id_for_order ='$orderId' ");
        error_log(var_export($rawData,true));
        if (count($rawData) == 1){
            return new static($rawData[0]);
        }
        return false;
    }
    
    
    // override
    public function Show_head()
    {
        //parent::Show_head();
        print "<a href='view.php?id=".$this->id()."'>".$this->name()."</a> <a href='edit.php?action=".ACT_EDIT."&id=".$this->id()."'> <img src='/design/edit.gif'></a>";
    }
     
    
    public function Validate($input = false){
        // stub
        return true;
    }
    public function getValidateErrorMessage(){
        // stub
        return '';
    }
    
    public function getAbout(){
        print $this->get_field_value('about');
    }
    
    public function getRawGoods()
    {        
        if (! $this->id()){
            throw new Exception("Can't get goods for PromoAction without ID");
        }
        $query = "SELECT * FROM goods WHERE reserv1 = ".$this->id();
        return \System::run_query($query);
        
    }
           
    
    public function getGoodIds(){
        $data = $this->getRawGoods();
        foreach ($data as $line){
            $result[] =$line['id'];
        }
        return $result;        
    }
    
    public function getGoods(){
        $result = [];
        $data = $this->getGoodIds();
        if ( count($data) == 0) {
            return $result;
        }
        foreach ($data as $id){
            $result[] = new Good($id);
        }
        return $result;
    }
    
    
    private static function getCompatibleIdForOrder(){
        $query = "SELECT * FROM goods WHERE id LIKE 'um%'";
        $data = \System::run_query($query);
        $result=[0=>'Нет'];
        foreach ($data as $line){
            $result[$line['id']] = $line['id']." ".$line['name'];
        }
        return $result;   
    }
    
    //override
    protected function init_add_date()
    {
        parent::init_add_date();
        $this->datalist['active']['htm_type'] = SELECT;
        $this->datalist['active']['sqldata'] = [0=>"Отключена",1=>"Активна"];
        
        $this->datalist['show_old_price']['htm_type'] = SELECT;
        $this->datalist['show_old_price']['sqldata'] = [0=>"Нет",1=>"Да"];
        
        $this->datalist['free_moscow_delivery']['htm_type'] = SELECT;
        $this->datalist['free_moscow_delivery']['sqldata'] = [0=>"Нет",1=>"Да"];
        
        $this->datalist['id_for_order']['htm_type'] = SELECT;
        $this->datalist['id_for_order']['sqldata'] = self::getCompatibleIdForOrder();
        
        return true;
    }
    
    
    
    public function getStartDate(){
        //error_log(" start: ".$this->getField('start'));
        return strtotime($this->get_field_value('start'));
        
    }
    
    public function getFinishDate(){
        //error_log(" finish: ".$this->getField('finish'));
        return strtotime($this->get_field_value('finish'));
    }
    
    
     public function isDateInAction($today){
        $start = $this->getStartDate();
        $finish = $this->getFinishDate();
        if ($start > $today || $finish < $today){
            return false;
        }
        return true;
    }
    
     public function isActive(){
        if ($this->get_field_value('active') == 0 ){
            return false;
        }
        
        $today = mktime(0,0,0);
        if (! $this->isDateInAction($today)){
            return false;
        }
        return true;
    }
    
    
     public function getMinAmount(){
        return $this->get_field_value('min_amount');
    }
    
    
    public function getIdForOrder(){
        return $this->get_field_value('id_for_order');
    }
    
    public function getYandexMarketBid(){
        return $this->get_field_value('ymarket_bid');
    }
    
    public function getYandexMarketSalesNotes(){
        return $this->get_field_value('ymarket_sales_notes');
    }
}

