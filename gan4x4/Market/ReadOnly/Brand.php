<?php

namespace gan4x4\Market\ReadOnly;
use \Exception;

class Brand extends CatalogEntity{
    public $url = "";
    
    public function __construct($id,$db) {
        $clearId = intval($id);
        parent::__construct($clearId,'market_brand',$db);
    }
    
    public function getDirForPicture() {
        return 'brand';
    }
    
    
    public function isModelsReal(){
        // Inversion becouse mysql can't work with default value '1' only zero :(
        return ! $this->getField('no_model'); 
    }
    
    
    public function getAboutText()
    {
        return $this->getField('about');
    }
    
    public function getUrl(){
        return $this->url."?brand_id=".$this->getId();;
    }
    
    public function isMajor(){
        return $this->getField('majority') == 0;
    }
 /*   
    public function getPicture(){
        return new  Picture('/pic/brand',$this->getId());
    }
   */ 
    
    public function getTypes(){
        $types = explode(',', $this->getField('type'));
        array_walk($types, function(&$v){ 
            $v=trim($v);}
        );
        return $types;
    }
    
    protected function checkType($type){
        if (! in_array($type,$this->getTypes())){
           throw new Exception("Bad id for type ".$type." not found");
        }
        
    }
    
}
