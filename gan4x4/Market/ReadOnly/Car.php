<?php
namespace gan4x4\Market\ReadOnly;

use gan4x4\Framework\DbObject;
use gan4x4\Market\Et\Photo;

class Car extends DbObject{
    
    static $tune_list = [ 0 => 'Стандарт', 1 => 'Туризм',2 => 'Экстрим'];
    private $brand = null;
    private $tune = 0;
    public function __construct($id,  &$db) {
        parent::__construct($id, 'cars', $db);
    }
    
    public function getBrand()
    {
        if ($this->brand == null){
            $this->brand = new DbObject($this->getField('brand_id'),'car_brand',$this->db);    
        }
        //var_dump($this->brand);
        //die();
        return $this->brand;
        
    }
    
    public function setTune($tune)
    {
        $this->tune = $tune;
    }
    
    public function getTuneName(){
        return self::$tune_list[$this->tune];
    }
    
    public function getFullName(){
        return $this->getBrand()->getName()." ".$this->getName();
    }
    
    public function getThumbs(){
        $query="SELECT market_pic.id FROM  market_pic_binding, market_pic WHERE  market_pic.id=market_pic_binding.pic_id AND "
                . "market_pic_binding.type = 1 AND market_pic_binding.model_id = ".$this->getId()." ORDER BY RAND()";
        $result = $this->db->run_query($query);
        $output = [];
        foreach($result as $line){
            //var_dump($line);
            $output[] = new Photo($line['id'],$this->db);    
            //$output = $line['link_thumb'];
        }
        return $output;
        
        //$rs=mysql_num_rows($result);
    }
    
    
    
    
}
