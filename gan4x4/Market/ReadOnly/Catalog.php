<?php

namespace gan4x4\Market\ReadOnly;

class Catalog {
    private $data;
    private static $simpleSections = array('dz');
    public function __construct($csvFilePath) {
        if (!file_exists($csvFilePath)){
            throw new \Exception("Invalid file name ".$csvFilePath);
        }
        $this->data = self::getCatalogParts($csvFilePath);
        if (count($this->data) == 0){
            throw new Exception("Invalid data in file ".$csvFilePath);
        }
    }
    
    private static function getCatalogParts($file)
    {
        $res = array();
        $fp = fopen ($file,"r");
        while ($data = my_getcsv ($fp, 1000, ";")) {
            $code = $data[0];
            $info = $data[1];
            if (strlen($code) == 1 )
            {
                $part = $info;
                $res[$code]= $info;
            }
            elseif (strlen($code) == 2 )
            {
                $res[$code]=$part." :: ".$info;
            }
        }
        fclose($fp);
        asort($res);
        return $res;
    }
    
    public function getPartOfCatalog($filter = null){
        if ($filter == null){
            $filter = array();
        }
        //var_dump($this->data);
        $result = array();
        foreach ($filter as $key){
            if (isset($this->data[$key])){
                $result[$key] = $this->data[$key];
            }
        }
        return $result;
    }
    
    
    public function id2name($id){
        if (! isset($this->data[$id])){
            throw new \Exception("Bad id");
        }
        return $this->data[$id];
    }
    
    public static function isSimple($key){
        return in_array($key,self::$simpleSections);
    }
    
    
    public static function validateDb($db){
        // Find dublicated ids 
        $result = 'ОК';
        $duplicates = $db->runQuery('select id, name, count(id) AS cnt FROM goods group by id  having cnt > 1');
        if (count($duplicates) > 0){
            $result = "goods table has duplicates: <br>";
            foreach ($duplicates as $line){
                foreach ($line as $key=>$value){
                    $result .= $key .' => '.$value." ";
                }
                $result .= "<br>\n";
            }
        }
        
        // to do
        // tyre in goods without line in t info
        // SELECT * FROM goods WHERE NOT EXISTS (SELECT * FROM  `t_info` WHERE t_info.id = goods.id AND t_info.model_id = 178 )
        
        
        // find tyres where model name not iqual model name in t_info
        //$query = "SELECT t_info.model, tyre_models.name FROM t_info, tyre_models WHERE t_info.model <> tyre_models.name AND tyre_models.id = t_info.model_id GROUP BY tyre_models.name";
        
        
        return $result;
    }
    
   
}
