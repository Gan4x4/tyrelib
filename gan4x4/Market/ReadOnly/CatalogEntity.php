<?php

namespace gan4x4\Market\ReadOnly;
use gan4x4\Framework\DbObject;
use gan4x4\Market\Picture\Picture;

abstract class CatalogEntity extends DbObject  {
    
    public function __construct($id,$table,&$db) {
        parent::__construct($id,$table,$db);
        //var_dump($this->db);
    }
    
    protected static $sininimField = 'sinonims';
    
    protected abstract function getDirForPicture();
    //public abstract function getUrl();
    
    private static $commonPictureDir = '/pic/';
    
    public static function setCommonPictureDir($path){
        static::$commonPictureDir = $path;
    }
    
    private function getFullPictureDir(){
        return self::$commonPictureDir.$this->getDirForPicture();
    }
    
    public function getPicture(){
        return new Picture($this->getFullPictureDir(),$this->getId());
    }
    
    public function getSinonims(){
        $sin = $this->getField(static::$sininimField);
        if (in_array($sin, array('----','-----'))){
            $sin = '';
        }
        $result = array($this->getName());
        if (! empty($sin)){
            $parts = explode(',',$sin);
            foreach($parts as $value){
                $result[] = trim($value);
            }
        }
        return array_unique($result);
    }
        
    public function getUrl(){
        return '';
    }
}
