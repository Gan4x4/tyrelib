<?php

namespace gan4x4\Market\ReadOnly;
use gan4x4\Framework\DbObject;
use gan4x4\Market\ReadOnly\Model;


class Certificate extends DbObject  {
    
    public function __construct($id,&$db) {
        parent::__construct($id,'certificates',$db);
    }
    
    public static function getAllActual($types,$db)
    {
        if (! is_array($types)){
            $types = array($types);
            
        }
        //$types = Utils::addQuote($types);
        //$typeList = Utils::createCommasList($types);
        //$query = "SELECT cert_id AS id FROM certificate_to_model_binding WHERE type IN ($typeList) GROUP BY cert_id";
        $query = "SELECT id FROM certificates WHERE expire_date > NOW()";
       
        $data = $db->runQuery($query);
        $certList = array();
        if ($data !==false){
            foreach ($data as $cert){
                $certList[] = new self($cert['id'],$db);
            }
            return $certList;
        }
        return false;
    }
    
    public function getModels($type)
    {
        //$typeList= Utils::createCommasList($type);
        $query = "SELECT * FROM certificate_to_model_binding WHERE cert_id = ".$this->getId()." AND type = '$type' ";
        $data = $this->db->runQuery($query);
        if ($data !== false){
        $models = array();
            foreach ($data as $line){
                $models[] = Model::createByType($line['model_id'],$line['type'], $this->db);
            }
            return $models;
        }
        return false;
    }
    
    public function getUrl() {
        return $this->getField('url');
    }
    
    public function getExpireDate() {
        return $this->getField('expire_date');
    }
    
        
}
