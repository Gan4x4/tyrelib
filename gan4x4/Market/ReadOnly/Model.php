<?php

namespace gan4x4\Market\ReadOnly;

abstract class Model extends CatalogEntity{
    //put your code here
    public $url = "wheel.htm";
    abstract function getBrand();
    abstract function getType(); // t or d
    
    
    public function __construct($id,$table,&$db) {
        $clearId = intval($id);
        parent::__construct($clearId,$table,$db);
       
    }
    
    public static function createByType($id,$type,&$db){
        switch ($type) {
            case 't':
                    return new TyreModel($id,$db);

            
            case 'd':
                    return new WheelModel($id,$db);

            default:
                throw new Exception("Undefined type ".$type);
        }
    }
   
    public function getAboutText(){
        return $this->getField('about');
    }  
    
    public function isReal()
    {
        return $this->getBrand()->isModelsReal();
    }
    
    
    public function getCertificateUrl(){
        
        $type = $this->getType();
        $modelId = $this->getId();
        $query =  "SELECT cert.url FROM certificates AS cert, certificate_to_model_binding AS bind ";
        $query .= " WHERE cert.id = bind.cert_id AND bind.model_id = $modelId AND bind.type = '$type' AND cert.expire_date > NOW() ORDER BY cert.expire_date DESC";
        $certUrls = $this->getDb()->runQuery($query);
        //print $query;
        //var_dump($certUrls);
        //die();
        if (count($certUrls) > 0){
            return $certUrls[0]['url'];
        }
        return false;
    }
    
    public function getUrl(){
        return $this->url."?model_id=".$this->getId();;
    }
    
    public function isAvaliable(){
        return $this->getField('av') == 1;
    }
    
    public function getFullName(){
        return $this->getBrand()->getName()." ".$this->getName();
    }
    
    public function getOriginalLink(){
        return $this->getField('original_link');
    }
}
