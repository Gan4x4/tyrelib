<?php

namespace gan4x4\Market\ReadOnly;
use gan4x4\Framework\DbObject;
use gan4x4\Market\ReadOnly\PromoAction;
use Exception;
/**
 * Description of Product
 *
 * @author anton
 */
abstract class Product extends CatalogEntity{

    protected $info = null;
    protected $model = null;
    protected $promo = false;
    protected static $sininimField = 'reserv3';
    
    public function __construct($id,  &$db) {
        parent::__construct($id, 'goods', $db);
        $infoTable = $this->getInfoTableName();
        $this->info = new DbObject($id,$infoTable,$db); 
        //die($id." -- ".$infoTable);
        //var_dump($this->info);
    }
    
    abstract protected function getInfoTableName();
    abstract public function getModel();
    abstract public function getTypeName();
    
    protected function getDirForPicture() {
        
    }
    
    public function getPicture(){
        return $this->getModel()->getPicture();
    }
    
    
    public static function createTypedProduct($id,&$db){
        $type = $id{0};
        switch ($type) {
            case 't':
                return new Tyre($id,$db);
            case 'd':
              
                return new Wheel($id,$db);
//            case 'sa':
//                return new ShockAbsorber($id);
            default:
                throw new Exception("Unknown type ".$type);
        }
    }
 
    public function getSinonims(){
        
        $result = array(); 
        $baseSin = parent::getSinonims();
        $baseSin[] = $this->getVendorId();
        $brandSin = $this->getModel()->getBrand()->getSinonims();
        if ($this->getModel()->isReal()){
            $modelSin = $this->getModel()->getSinonims();
        }
        else
        {
            $modelSin = array('');
        }
        foreach ($brandSin as $brand){
            foreach ($modelSin as $model){
                foreach ($baseSin as $base){
                    $result[] = $brand." ".$model." ".$base;
                }
            }
        }
        
        if ($this->info->hasField('original')){
            $result[] = $this->info->getField('original');
        }
        $result[] = $this->info->getField('name');
        return array_unique($result);
    }
    
    
     public function getVendorId(){
        return $this->info->getField('vendor_id');
    }
    
    
    public function getBrandAndModelName()
    {   
        $brand = $this->getModel()->getBrand();
        if ($this->getModel()->isReal()) { 
            $model =  $this->getModel()->getName();
        }
        else{
            $model = $this->getVendorId();
        }
        return $brand->getName()." ".$model;
    }
    
    public function getFullName() {
        try{
            $perfix =  $this->getBrandAndModelName();
        }
        catch (Exception $e){
            // This good hasn't brand or model
            $perfix = '';
        }
        return $perfix.' '.$this->getName();
    }

    public function enoughAmount(){
        return false;
    }
    
    public function getStock(){
        return $this->getField('amount_d');
    }
    
    public function onDemandOnly(){
        return $this->getField('amount') == 2;
    }
    
    public function canOrder(){
        if ($this->getField('amount') == 0) {
            // if good in stock, and has price this is mistake
            if ($this->getField('amount_d') > 0 && 
                $this->getPrice() > 0){
                error_log('Sieems than good '.$this->getFullName()."(".$this->getId().") has disabled wrongly. It has a price (".$this->getPrice().") and amount (".$this->getField('amount_d').")" );
                //return true;
            }
            return false;
        }elseif ($this->getPrice() == 0){
            error_log('good '.$this->getFullName()."(".$this->getId().") enabled but hasn't price.  ");
            return false;
        }
        return true;
    }
    
    public function getPrice(){
        return $this->getField('price1');
    }
    
    public function getDeliveryDelay(){
        return $this->getField('wait');
    }
    /*
    public function inPromoAction(){
        return $this->getId() == 'tb2371';
        
    }
    */
    
    
    public function getPromoAction(){
        if ($this->getField('reserv1') > 0){
            $this->promo = new PromoAction($this->getField('reserv1'),$this->getDb());
        }
        return $this->promo;
        
    }
    
    public function getActivePromoAction($town = false){
        $promo = $this->getPromoAction();
        //error_log(var_export($promo,true));
        if ($promo !== false && $promo->isActive($town)){
            return $promo;
        }
        return false;
    }
    
    
    public function isShowOldPrice(){
        $promo = $this->getPromoAction();
         if ($promo != false && 
             $promo->isShowOldPrice() &&
                $this->getOldPrice() != 0 &&
                $this->getOldPrice() > $this->getPrice()   
            ){
             return true;
        }
        return false;             
    }
    
    
    public function getOldPrice(){
        return $this->getField('old_price');
    }
    
    public function getEconomy(){
        return $this->getOldPrice() - $this->getPrice();
    }
    
    // must be owerriden in goods with dimensions
    public function getValue(){
        return 0;
    }
    
}
