<?php

namespace gan4x4\Market\ReadOnly;
use gan4x4\Framework\DbObject;
use gan4x4\Market\ReadOnly\Product;

class PromoAction  extends DbObject{
    const SELL = 1;
    const BEST = 2;
    const DELIVERY = 3;
    const GIFT = 4;
    private static $instances = [];
    private $currentTown = false;
    
    
    public function __construct($id,  &$db) {
        parent::__construct($id, 'market_promo_action', $db);
    }
    
    public static function getById($id,  &$db){
        if (! isset(self::$instances[$id])){
            $obj = new static($id, $db);
            if ($obj->getId() == $id){
                self::$instances[$id] = $obj;
            }
            else {
                throw new Exception ("Promo Id not found");
            }
        }
        return self::$instances[$id];
    }
    
    public static function getAll(&$db,$filter = ''){
        $rawData = $db->runQuery('SELECT * FROM market_promo_action '.$filter);
        $objects = [];
        foreach ($rawData as $line){
            $objects[] = new static($line['id'],$db);
        }
        return $objects;
    }
    
    public static function getActive(&$db){
        return self::getAll($db,' WHERE `active` = 1 AND `start` <= CURDATE()  AND CURDATE() <= `finish` ');
    }
    
    public static function getActionByOrderId($orderId,$db){
        if (empty($orderId)){
            return false;
        }
        $rawData = $db->runQuery("SELECT * FROM market_promo_action WHERE id_for_order ='$orderId' ");
        //error_log(var_export($rawData,true));
        if (count($rawData) == 1){
            return new static($rawData[0]['id'],$db);
        }
        return false;
    }
    
    public function setCurrentTown($town){
        $this->currentTown = $town;
    }
    
    public function hasTown($town){
        $actionTowns = explode(',',$this->getField('towns'));  
       
        foreach($actionTowns as $item){
            $clearItem = trim($item);
            if ($town == $clearItem  && $clearItem != ''){
                //var_dump($town);
                return true;
            }
        }
        return false;
    }
    
    public function isDateInAction($today){
        $start = $this->getStartDate();
        $finish = $this->getFinishDate();
        if ($start > $today || $finish < $today){
            return false;
        }
        return true;
    }
    
    
    public function isActive($town = false){
        if ($this->getField('active') == 0 ){
            return false;
        }
        
        $today = mktime(0,0,0);
        if (! $this->isDateInAction($today)){
            return false;
        }
        
        if($town == false){
            $town = $this->currenTown;
        }
        if ($town != false && 
            trim($this->getField('towns')) != '' &&
            ! $this->hasTown($town)){
            return false;
        }
        
        // TO DO analyze time of day
        
        return true;
    }
    
    public function getStartDate($raw = false){
        if ($raw){
            return $this->getField('start');
        }
        //error_log(" start: ".$this->getField('start'));
        return strtotime($this->getField('start'));
        
    }
    
    public function getFinishDate($raw = false){
        //error_log(" finish: ".$this->getField('finish'));
        if ($raw){
            return $this->getField('finish');
        }
        return strtotime($this->getField('finish'));
    }
    
    public function getIcon(){
        return $this->getField('promo_html');
    }
    
    
    public function getImagePath(){
        return $this->getField('image');
    }
    
    private function insertTownName($pattern){
        $replacement = ''; // if town empty
        if ($this->currentTown != false ){
            $replacement = ' в город '.$this->currentTown;
        }
        return str_replace('{town}', $replacement, $pattern);
        
    }
    
    public function getTooltipText(){
        if ($this->currentTown != false && $this->hasTown($this->currentTown)){
            return $this->insertTownName($this->getField('tooltip_text'));    
        }
        
        return $this->getField('tooltip_text');
        //str_replace('{towns}',' в города: '. $this->getField('towns'), $this->getField('about'));
        //return str_replace('{town}', $this->currentTown, $text);
    }
    
    // for product page
    public function getShortText(){
        //return "FFF";
        //return $this->getField('short_text');
        return $this->insertTownName($this->getField('short_text'));
    }
    
    public function getAbout(){
        $wTowns = str_replace('{towns}',' в города: '. $this->getField('towns'), $this->getField('about'));
        return $wTowns;
    }
    
    public function getBottomText(){
        
        return $this->getField('bottom_text');
    }
    
    
    public function getRawGoods($filter = '')
    {        
        if (! $this->getId()){
            throw new \Exception("Can't get goods for PromoAction without ID");
        }
        $query = "SELECT * FROM goods WHERE reserv1 = ".$this->getId().$filter;
        $data = $this->getDb()->runQuery($query);
        // check amount property
        $checkedData = [];
        foreach($data as $line){
            if ($line['amount'] != 0){
                $checkedData[] = $line;
            }else{
                error_log("Disabled good(".$line['id'].") in action ".$this->getName());   
            }
        }
        return $checkedData;
    }
    
    public function getTowns(){
        return $this->getField('towns');
    }
    public function freeMoscowDelivery(){
        return $this->getField('free_moscow_delivery') == 1;
    }
    
    public function getMinAmount(){
        return $this->getField('min_amount');
    }
    
    public function getIdForOrder(){
        return $this->getField('id_for_order');
    }
    
    public function isShowOldPrice(){
        return $this->getField('show_old_price') == 1;
    }
           
    /*
    public function getGoodIds(){
        $data = $this->getRawGoods();
        foreach ($data as $line){
            $result[] =$line['id'];
        }
        return $result;        
    }
    
    public function getGoods(){
        $result = [];
        $data = $this->getGoodIds();
        foreach ($data as $id){
            $result[] = Product::createTypedProduct($id,$this->getDb());
        }
        return $result;
    }
    */
    
    
    
}
