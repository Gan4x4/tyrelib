<?php
    namespace gan4x4\Market\ReadOnly;
    use gan4x4\Framework\DbObject;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Town
 *
 * @author anton
 */


class Town extends DbObject{
    public function __construct($id,  &$db) {
        parent::__construct($id, 'towns', $db);
    }
    
    public static function createByName($name,&$db){
        $clearName = trim($name);
        $query = "SELECT * FROM towns WHERE name = '$clearName'";
        $data = $db->runQuery($query);
        if (count($data) == 0){
            return false;
        }
        return new static($data[0]['id'],$db);
    }
    
    public function getDeliveryPrice(){
        return floatval($this->getField('price'));
    }
    
    public function isMoscow(){
        return $this->getName() == 'Москва';
    }
       
    
    public function calculateDeliveryPrice($value,$moscowDelivery){
        if ($this->isMoscow()){
            return $moscowDelivery;
        }
        if ($value){
            $deliveryCost = floor(($this->getDeliveryPrice()*$value*250*1.2+$moscowDelivery)/100)*100;
        }
        return $deliveryCost;
    }
}
