<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace gan4x4\Market\ReadOnly;
use gan4x4\Market\Size\TyreSize;
use \Exception;
/**
 * Description of Tyre
 *
 * @author anton
 */
class Tyre extends Product{
    private $size = null;
    
    protected function getInfoTableName() {
        return 't_info';
    }
    
    public function getSize(){
        //die("Orig ".$this->info->getField('original').var_export($this->info));
        if ($this->size == null){
            $this->size = TyreSize::parseSize($this->info->getField('original'));
        }
        return $this->size;
    }
    
    public function getName() {
        //return "XX";
        try{
            return $this->getSize()->getClearOriginal();    
        } catch (Exception $ex) {
            // If size can't be parsed
            error_log("Original size not parseble for ".parent::getName());
            return parent::getName();
        }
    }

    public function getModel() {
        assert('$this->info->hasField("model_id")',"Info table not contain field model_id");
        if ($this->model == null){
            $modelId = $this->info->getField("model_id");
            $this->model = new TyreModel($modelId,$this->getDb());
            assert('$this->model != null',"Model not created");
        }
        return $this->model;
    }

    
    
    public function getTypeName() {
        return "Шина";
    }

    
    
    //Override
    public function enoughAmount(){
        if ($this->getField('amount') ==1 && $this->getField('amount_d') > 9){
            return true;
        }
        return false;
    }
    
    public function getSpeedCode(){
        return $this->info->getField('code');
    }
    
    
    public function getValue(){
        try {
            $size = $this->getSize();
            return $size->getValue(); 
        } catch (Exception $exc) {
            return 0;
        }
    }
    
}
