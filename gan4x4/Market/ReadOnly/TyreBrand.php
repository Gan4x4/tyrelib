<?php

namespace gan4x4\Market\ReadOnly;


class TyreBrand extends Brand{
    public $url = "/tyre.htm";
    
    public function __construct($id,$db) {
       parent::__construct($id,$db);
       $this->checkType('t');
    }
    
    public static function getAll($db,$filter = '',$order = 'name' ){
        if ($filter != ''){
            $add = " AND ( $filter ) ";
        }
        else{
            $add = '';
        }
        $query = "SELECT id FROM market_brand WHERE type LIKE '%t%' $add ORDER BY $order";
        $tyreBrandIdList = $db->runQuery($query);
        $brandsList = array();
        foreach($tyreBrandIdList as $line){
            $brandsList[] = new TyreBrand($line['id'], $db);
        }
        return $brandsList;
    }
    
    public function getAllModels()
    {
        $query = "SELECT id FROM tyre_models WHERE av > -1 AND brand_id = ".$this->getId();
        $modelsIdList = $this->getDb()->runQuery($query);
        $modelsList = array();
        //var_dump($modelsIdList);
        foreach($modelsIdList as $line){
            $modelsList[] = new TyreModel($line['id'], $this->getDb());
        }
        return $modelsList;
    }
   
    public function isAvaliable(){
        $query = "SELECT * FROM tyre_models WHERE av = 1 AND brand_id = ".$this->getId() ;
        $result = $this->getDb()->runQuery($query);
        return count($result) > 0;
    }
}
