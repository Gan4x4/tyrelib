<?php

namespace gan4x4\Market\ReadOnly;

class TyreModel extends Model{
    public $url = "/tyre.htm";
    
    public function __construct($id,&$db) {
        $clearId = intval($id);
        parent::__construct($clearId,'tyre_models',$db);
        //var_dump($this->db);
    }
    /*
    public function getPicture(){
        return new  Picture('models/tyre',$this->getId());
    }
*/
    public function getDirForPicture() {
        return 'models/tyre';
    }
    public function getBrand() {
        return new TyreBrand($this->getField('brand_id'), $this->getDb());
    }
    
    function getAvaliableDisks(){
        
        //var_dump($this->db);
        return $this->db->runQuery("SELECT diametr t_info WHERE model_id = ".$this->id()." GROUP BY diametr");
    }

    public function getType(){
       return 't';
   }
   
   
}
