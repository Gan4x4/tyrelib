<?php

namespace gan4x4\Market\ReadOnly;
use gan4x4\Market\ReadOnly\Product;
use gan4x4\Market\ReadOnly\Car;
use gan4x4\Market\Utils;


class Wheel extends Product{
    protected static $typeDesc = array('Диск ',' Диски для внедорожников');
    
    protected function getInfoTableName(){
        return "d_info";
    }
    
    public function getTypeName(){
        return self::$typeDesc[0];
    }
    
    
    
    public function getName(){
         return $this->getDiametr().
                'x'.
                $this->getWidth().
                " ; ".
                $this->info->getField('bolts').
                " ET ".
                $this->info->getField('offset');
    }
    
    
    public function getModel(){
        assert('$this->info->hasField("model_id")',"Info table not contain field model_id");
        if ($this->model == null){
            $modelId = $this->info->getField("model_id");
            $this->model = new WheelModel($modelId,$this->getDb());
            assert('$this->model != null',"Model not created");
        }
        return $this->model;
    }
    
    
    public function getDiametr(){
        return Utils::remove_zeros($this->info->getField('diametr'));
    }
    
    public function getWidth(){
        return Utils::remove_zeros($this->info->getField('width'));
    }
    
    public function enoughAmount(){
        if ($this->getField('amount') ==1 && $this->getField('amount_d') > 9){
            return true;
        }
        return false;
    }
    
    public function getPcdName(){
        $pcdList = \System::get_db_data_by_id('wheel_pcd','sinonim','name');    
        $bolts = $this->info->getField('bolts');
        
        $pcds = explode(', ',$bolts);
        // some Mickey Thomson disk has multiple holes pack
        $names = [];
        foreach ($pcds  as $pcd){
        //var_dump($pcdList);
            foreach ($pcdList as $key=>$value){
                if (trim($pcd) == $key) {
                    $names[] = $value;
                }
            }
        }
        
        if (count($names) == 0){
            throw new \Exception("PCD '".$bolts."' not found!");    
        }
        elseif (count($names) == 1)
        {
            return trim($names[0]);
            
        }
        else{
            return implode('/', $names);
        }
        
    }
    
    public function getDia(){
        return $this->info->getField('stupicha');
    }
    
    public function getCars(){
        $allBolts = explode(', ',$this->info->getField('bolts'));
        $dia = $this->getDia();
        $diametr = $this->getDiametr();
        $query = "SELECT cars.id AS id FROM cars, car_tuning , wheel_pcd "
                . "WHERE cars.id = car_tuning.car_id AND "
                . "wheel_pcd.id = car_tuning.disk_pcd AND "
                . "wheel_pcd.name IN ('".implode("','",$allBolts)."') AND"
                . " (car_tuning.disk_dia = 0 OR car_tuning.disk_dia <= $dia) AND"
                . " car_tuning.disk_diametr <= $diametr GROUP BY id ";
        //die($query);
        $rawCars = $this->getDb()->runQuery($query);
       
        $cars = [];
        foreach ($rawCars as $rawCar){
            $cars[] = new Car($rawCar['id'],$this->getDb());
        }
        return $cars;
    }
    
    
    
}
