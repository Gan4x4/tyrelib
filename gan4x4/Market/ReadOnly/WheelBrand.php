<?php

namespace gan4x4\Market\ReadOnly;

class WheelBrand extends Brand{
    public $url = "/wheel.htm";
    
    public function __construct($id,$db) {
       parent::__construct($id,$db);
       $this->checkType('d');
    }
    
    public static function getAll($db){
        $query = "SELECT id FROM market_brand WHERE type LIKE '%d%' ORDER BY name";
        $tyreBrandIdList = $db->runQuery($query);
        $brandsList = array();
        foreach($tyreBrandIdList as $line){
            $brandsList[] = new WheelBrand($line['id'], $db);
        }
        return $brandsList;
    }
    
    
    public function getAllModels()
    {
        //var_dump($this->db);
        $query = "SELECT id FROM wheel_models WHERE av > -1 AND brand_id = ".$this->getId();
        $modelsIdList = $this->getDb()->runQuery($query);
        $modelsList = array();
        foreach($modelsIdList as $line){
            //var_dump($id);
            $id = $line['id'];
            $modelsList[$id] = new WheelModel($id, $this->getDb());
        }
        //var_dump($modelsIdList);
        return $modelsList;
    }
   
   public function isAvaliable(){
        $query = "SELECT * FROM wheel_models WHERE av = 1 AND brand_id = ".$this->getId() ;
        $result = $this->getDb()->runQuery($query);
        return count($result) > 0;
    }
}
