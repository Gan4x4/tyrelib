<?php

namespace gan4x4\Market\ReadOnly;

class WheelModel extends Model{
    public $url = "/wheel.htm";
    
    public function __construct($id,$db) {
        $clearId = intval($id);
        parent::__construct($clearId,'wheel_models',$db);
    }

    public function getDirForPicture() {
        return 'models/wheel';
   }
   public function getBrand() {
        return new WheelBrand($this->getField('brand_id'), $this->getDb());
   }
   
   public function getType(){
       return 'd';
   }
    
}
