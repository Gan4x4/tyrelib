<?php


namespace gan4x4\Market;
use gan4x4\Market\ReadOnly\Catalog;


define('TYRE','t');
define('WHEEL','d');
define('INVALID_ID_VALUE',-100);
define('INVALID_ACTION_VALUE',-100);

class Request{
    
    const ACT_EDIT = 2;
    const ACT_UPDATE = 3;
    const ACT_DELETE = 4;
    const ACT_CREATE = 6;
    
    
    
    private $data;
    //private static $modelsTables = array(TYRE=>'tyre_models',WHEEL=>'wheel_models', 's'=>'suspension_models');
    function __construct($input = false){
        if ($input == false){
            $this->data = $_REQUEST; 
        }
        else{
            $this->data = $input;
        }
    }

    function getRawData(){
        return $this->data;
    }
            
    function getAction(){
        if (isset($this->data['action'])) {
            $action = intval($this->data['action']);
        }
        else{
            $action = INVALID_ACTION_VALUE;
        }
        return $action;
    }
   
    function getAllowedTypes(){
        //return array_keys(self::$modelsTables);
        return Model::getAllowedTypes();
    }
    
    static function getModelTable($type){
        /*
        $type = $id{0};
        if (isset(self::$modelsTables[$type])){
            return self::$modelsTables[$type];
        } 
        throw new Exception("Table for type $type not set");
         * 
         */
        return Model::getTableByType($type);
    }
    
    
    function getTypeTable(){
        $type = $this->getType();
        return self::getModelTable($type);
    }


    function getInfoTable(){
        $type = $this->getType();
        return $type.'_info';

    }

    function isFieldFilled($name){
        return isset($this->data[$name]) && ! empty($this->data[$name]);
    }
    
    function isFieldsFilled($fieldNames){
        foreach ($fieldNames as $name){
            if (! $this->isFieldFilled($name)){
                return false;
            }
        }
        return true;
    }

    function getType(){
        $keys = array('_type','type');
        foreach ($keys as $key) {
            if (isset($this->data[$key]) && 
                strlen($this->data[$key]) >= 1 &&
                strlen($this->data[$key]) <= 2 ) {
                return $this->data[$key];
            }
        }
        
        $goodId = $this->getGoodId();
        $type = $this->getSection2GoodId($goodId);
        return $type;
        /*
        if ()
        if (strlen($this->data['id']) == 6){
            $val =  substr($this->data['id'],0,2);
            var_dump(Model::getAllowedTypes());
            var_dump($val);
            if (ctype_alpha($val) &&
                    $this->isSectionAllowed($this->data['id'])){
                //in_array($val,$this->getAllowedTypes())){
                return $val;
            }
        }
         * 
         */
        throw new \Exception("Type field not set");
    }


    function getId($field = 'id'){
        $idValue = INVALID_ID_VALUE;
        if (isset($this->data[$field]) &&
            intval($this->data[$field]) > 0  ) {
            $idValue = $this->data[$field];
        }
        return $idValue;
    }
    
    
    private function getSection2GoodId($goodId){
        $perfix = substr($goodId,0,2);
        if (Catalog::isSimple($perfix)){
            return $perfix;
        }
        
        $goodIdList = $this->getAllowedTypes();
        foreach($goodIdList as $section){
            $id = substr($goodId,0,strlen($section));
            if ($id == $section){
                return $id;
            }
        }
        return false;
    }
    
    private function isSectionAllowed($goodId){
        /*
        $goodIdList = $this->getAllowedTypes();
        foreach($goodIdList as $section){
            $id = substr($goodId,0,strlen($section));
            if ($id == $section){
                return true;
            }
        }
        return false;
         * 
         */
        $type = $this->getSection2GoodId($goodId);
        return   $type ? true: false; 
    }
    
    // Get string six-symbol id
    function getGoodId($field = 'id'){
        $gid  = $this->getGoodIdNoThrow($field);
        if ($gid == -1) {
            
            throw new \Exception("Good id not found or has bad format");
        }
        return $gid;
    }
    
    
    public static function isGoodIdValid($string){
        return (strlen($string) == 6 && is_numeric(substr($string,2)));
    }
    
    function getGoodIdNoThrow($field = 'id',$default = -1){
        if (isset($this->data[$field]) &&
            self::isGoodIdValid($this->data[$field]) &&
            $this->isSectionAllowed($this->data[$field])) {
            return $this->data[$field];
        }
        else{
            return $default;
        }
    }
    
    
    function getClass(){
        $field = 'class_name';
        if ($this->isFieldsFilled($field)){
            return $this->data[$field];
        }
        return false;
    }
    
    function setField($field,$value){
        assert($field !='');
        $this->data[$field] = $value;
    }
    
    function getStringField($field,$default = false){
        if (isset($this->data[$field])){
            return trim(filter_var($this->data[$field], FILTER_SANITIZE_STRING));
        }
        return $default;
    }
    
    function getIntField($field,$default = false){
        if (isset($this->data[$field])){
            return intval($this->data[$field]);
        }
        return $default;
    }
    
    // Legacy
    function getDate($n){
        $result = array();
        $keys = array(0=>'dt_y',1=>'dt_mn',2=>'dt_d');
        foreach ($keys as $datepart){
            $currKey = $datepart.$n;
            $value = $this->getIntField($currKey);
            if ($value === false){
                return false;
            }
            $result[] = $value; 
        }
        return \System::create_commas_list($result,'-');
        //$this->data['dt_y1'].'-'.$this->data['dt_mn1'].'-'.$this->data['dt_d1'];
    }
    
    
    function generateUrl($additional = array()){
        $base = $_SERVER['PHP_SELF'];
        $allParams = array_merge($this->data, $additional);
        $urlParamList = array();
        foreach($allParams as $key=>$value){
            $urlParamList[] =  $key.'='.$value;
        }
        return $base."?".trim(\System::create_commas_list($urlParamList,'&'));
    }

    function getDateField($name,$default = null )
    {
        $time = strtotime($this->data[$name]);
        if ($time === false){
            $time =  $default == null ? mktime() : $default ;
        }
        return  Date('Y-m-d',$time);
    }
    
    function getCheckField($name)
    {
        if (isset($this->data[$name])){
            return true;
        }
        else{
            return false;
        }
    }
    
}
