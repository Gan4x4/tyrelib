<?php
    namespace gan4x4\Market\Rest;
    use Exception;
    
    trait ApiTrait{
        
        public static function sendGet($url,$params)
        {
            $curl = curl_init();
            if( $curl ) {
                curl_setopt($curl, CURLOPT_URL, $url."?".http_build_query($params));
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
                $out = curl_exec($curl);
                curl_close($curl);
                return $out;
            }    
            throw new Exception("Curl not init");
        }
    
    
        public static function sendPost($url,$params){

            $ch = curl_init(); 
            curl_setopt($ch, CURLOPT_HEADER, 0); 
            curl_setopt($ch, CURLOPT_TIMEOUT, 10); 
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
            curl_setopt($ch, CURLOPT_POST, 1); 
            //$rs = 'Http_username='.urlencode(urlencode(self::$login)).'&Http_password='.urlencode(self::$password).'&Phone_list='.$this->getPhone().'&Message='.urlencode($this->getMessage()); 
            //print $rs;
            $rs = http_build_query($params);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $rs);
            curl_setopt($ch, CURLOPT_URL, $url); 
            $ret = trim(curl_exec($ch)); 
            //var_dump($ret);

            curl_close($ch); 

            
            //throw new Exception("Error while call ".$url);
            

            return $ret;
        }
    }