<?php

namespace gan4x4\Market\Rest;
use gan4x4\Market\Rest\ApiTrait;


/**
 * Description of RegByPhone
 *
 * @author anton
 */
class PhoneInfoApi {
    use ApiTrait;
    //private static $url = 'https://www.eduscan.net/help/phone_ajax.php';
    
    private static $url = 'https://gsm-inform.ru/api/info/';
    
    public function getInfo($phonePart){
        try {
            $raw =  self::sendPost(self::$url,['phone'=>$phonePart,'get-phone-info'=>'on']);
            $responce = json_decode($raw);
            //var_dump($responce);
            if ($responce->error == 'ok'){
                return $responce->region->name;
            }
            /*
        return $enc;
        $tail = strrpos($raw,'~');
        if ($tail !== false){
            return trim(substr($raw, $tail+1));
        }
        return $raw;
        */
       throw new Exception("Server return error code");
        } catch (Exception $exc) {
            return 'error';
        }
    }
    //put your code here
}
