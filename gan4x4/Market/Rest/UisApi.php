<?php

namespace gan4x4\Market\Rest;
use Exception;

class UisApi{
    
    private $sessionId;
    //private $baseUrl = 'http://api.comagic.ru/api/';
    
    
    public function __construct($login,$password)
    {
        $credential = array ('login' =>$login , 'password' => $password); 
        $session = self::sendPost('http://api.comagic.ru/api/login/',$credential);
        $result = json_decode($session);
        //var_dump($result);
        if (! $result->success  ){
            throw new Exception("Auth is unsuccessfull");
        }
        $this->sessionId = $result->data->session_key;
    }
    
    
    
    private static function sendPost($url,$params)
    {
        $curl = curl_init();
        if( $curl ) {
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($params));
            $out = curl_exec($curl);
            curl_close($curl);
            return $out;
        }    
        throw new Exception("Curl not init");
    }
    
    private static function sendGet($url,$params)
    {
        $curl = curl_init();
        if( $curl ) {
            curl_setopt($curl, CURLOPT_URL, $url."?".http_build_query($params));
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
            $out = curl_exec($curl);
            curl_close($curl);
            return $out;
        }    
        throw new Exception("Curl not init");
    }
    
    public function getCalls($from,$to)
    {
        $request = array('session_key' =>$this->sessionId,
            'direction' => 'in',
            'date_from' => $from,
            'date_till' => $to.' 23:59:59.000');
        $raw = self::sendGet('http://api.comagic.ru/api/v1/call/',$request);
        
        $result = json_decode($raw);
        if (! $result->success  ){
            throw new Exception("Get calls is unsuccessfull");
        }
        return $result->data;
    }
    
    public function sessionClose()
    {
        $request = array('session_key' =>$this->sessionId);
        self::sendGet('http://api.comagic.ru/api/logout/',$request);
        $this->sessionId = null;
    }
    
    
    function __destruct() {
       if ($this->sessionId) {
           $this->sessionClose();
       }
   }
    
}


