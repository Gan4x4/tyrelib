<?php

namespace gan4x4\Market\Rest;
use Exception;

define('CODE_NO_RESPONSE',-1);

class WebSms {

    private static $login = "Gan4x4";
    private static $password = "KOL99MOS";
    private static $url  = 'http://cab.websms.ru/http_in6.asp';  // Utf-8
    public static  $error_codes = array(0=>'ok',
        1=> 'error login password',
        2=> 'blocked user',
        3=> 'insufficient funds',
        4=> 'blocked ip',
        5=> 'http not enabled',
        6=> 'this server ip not enabled',
        7=> 'email sending not enabled',
        8=> 'this email not enabled',
        9=> 'blocked moderator ID',
        10=> 'error manual phone list', 
        11=> 'empty message text',
        12 => 'empty phone list',
        13 => 'stop service',
        14 => 'error format date',
        15 => 'double sent from web interface',
        16 => 'error dealer off',
        17 => 'error multiaccess',
        20 => 'Incorrect Group',
        21 => 'empty password' ,
        22 => 'empty login',
        23 => 'Invalid FromPhone' ,
        24 =>  'flood' 
        );

    private $phone = null;
    private $text = null;
    //private $error_code = 0;
    private $response = array();
    
    function __construct($tel,$message){
        $this->setPhone($tel);
        $this->setMessage($message);
    }
    
    function setPhone($p){
        $this->phone = $p;
    }
    
    function setMessage($t){
        $this->text = $t;
    }
    
    function getPhone(){
        return $this->phone;
    }
    
    function getMessage(){
        return $this->text;
    }
    
    function getErrorCode(){
        $code = CODE_NO_RESPONSE;
        if ($this->response != null ){
            $code =  $this->response['Error_code'];
        }
        return $code;
    }
    
    function getErrorMessage(){
        $ec = $this->getErrorCode();
        if (code == CODE_NO_RESPONSE) {
            $message = "Request not send yet";
        }
        else{
            $message = self::$error_codes[$ec];
        }
        return $message;
    }
    
    function readResponce($r){
        $this->response = array();
         $lines = explode("\n",$r);
         foreach($lines As $l){
             $pair = explode("=", $l);
             if (count($pair) == 2){
                 $this->response[trim($pair[0])] = trim($pair[1]);
             }
         }
    }
    
    
/*    
    function send(){
        $r = new HttpRequest(self::$url, HttpRequest::METH_GET); 
        $r->addQueryData(array('Http_username' => urlencode($this->login), 
            'Http_password' =>urlencode($this->password), 
            'Phone_list' => $this->getPhone(), 
            'Message' => urlencode($this->getMessage())
            )); 

        try { 
            $r->send(); 
            if ($r->getResponseCode() == 200) { 
                file_put_contents('myresults.log', $r->getResponseBody()); 
            } 
        } 
        catch (HttpException $ex) { 
            echo $ex; 
        } 
    }
    
  */  
    
    function validate(){
        
    }
    
    function send(){
        $this->validate();
        $u = self::$url;
        $ch = curl_init(); 
        curl_setopt($ch, CURLOPT_HEADER, 0); 
        curl_setopt($ch, CURLOPT_TIMEOUT, 10); 
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
        curl_setopt($ch, CURLOPT_POST, 1); 
        $rs = 'Http_username='.urlencode(urlencode(self::$login)).'&Http_password='.urlencode(self::$password).'&Phone_list='.$this->getPhone().'&Message='.urlencode($this->getMessage()); 
        //print $rs;
        curl_setopt($ch, CURLOPT_POSTFIELDS, $rs);
        curl_setopt($ch, CURLOPT_URL, $u); 
        $ret = trim(curl_exec($ch)); 
        //var_dump($ret);
        $this->readResponce($ret);
        curl_close($ch); 
        
        if ($this->getErrorCode() != 0) {
            throw new Exception("Error while send SMS to number ".$this->getPhone()." error_code = ".$this->getErrorCode()." message = ".$this->getErrorMessage());
        }
        
        //print $ret;
        //$this->response = explode("\n",$ret);
        //var_dump($this->response);
        
        
        //print "<hr>".$res;
        //
        //preg_match("/message_id\s*=\s*[0-9]+/i", $u, $arr_id ); 
        //$id = preg_replace("/message_id\s*=\s*/i", "", @strval($arr_id[0]) ); 
        //return $id;     
    }
    
    
    public static function checkPhone($phone){
        // remove non numeric
        $p = preg_replace("/[^0-9,.]/", "", $phone);
        if ($p{0} == '8') $p{0} = 7;
        if (strlen($p) != 11) return false;
        return $p;
    }
    
    
}
