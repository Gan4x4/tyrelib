<?php

namespace gan4x4\Market;

use \System;
use gan4x4\Market\Car;
use gan4x4\Market\Typ\CarBinding;
use Exception;

class ShockAbsorber extends Product{
    static $info_table = 'sa_info';
    protected $bindings = array();
    //static $pcdList = false;
   
    static protected $fieldsShowOrder = array(
        'price1'=>1,
        'price2'=>2,
        'amount'=>3,
        'reserv1'=>4,
        'name'=>11,
        'sa_info_vendor_id'=>12,
        'sa_info_top_mount'=>19,
        'sa_info_bottom_mount'=>20,
        'sa_info_stroke'=>22,
        'sa_info_length_max'=>24,
        'sa_info_length_min'=>25,
        'sa_info_lift_max'=>26,
        'sa_info_lift_min'=>27,
        'about'=>30,
        'sa_info_about'=>31
        
        );
    
    public function __construct($id){
        parent::__construct($id);
        $this->initBindingsByDb();
        //$this->hideSomeFields(array('d_info_model_id','d_info_brand_id','d_info_manufacturer','d_info_model','d_info_name','name'));
    }
    
    protected function initBindingsByDb(){
        $query = "SELECT * FROM parts_binding WHERE part_id = '".$this->id()."'";
        $result = System::run_query($query);
        if (count($result) > 0){
            foreach($result as $line){
                $this->bindings[$line['tune_type']][] = $line['car_id'];
            }
        }
    }
    
    //Owerride
    public static function createByRequest($input){
        $instance = parent::createByRequest($input);
        $perfix='bind';
        $instance->bindings  =array();
        foreach($input as $field=>$value)
        {
            $parts = explode('_', $field);
            if ($parts[0] == $perfix){
                print "FF $field <br>";
                $car = $parts[1]; 
                $tune = $parts[2]; 
                $instance->bindings[$tune][] = $car;
            }
        }
        //var_dump($instance->bindings);
        return $instance;
    }
    
    public function getBindings(){
        return $this->bindings;
    }
    
    protected function init_add_date(){
        
        $this->datalist['sa_info_top_mount']['htm_type'] = SELECT; 
        $this->datalist['sa_info_top_mount']['sqldata'] = self::getMountTypes(); 
        
        $this->datalist['sa_info_bottom_mount']['htm_type'] = SELECT; 
        $this->datalist['sa_info_bottom_mount']['sqldata'] = self::getMountTypes(); 
        
        //$this->datalist['sa_info_bottom_mount']['htm_type'] = SELECT; 
        //$this->datalist['sa_info_bottom_mount']['sqldata'] = self::getMountTypes(); 
        
        $this->hideSomeFields(array('sa_info_model_id','sa_info_brand_id'));
        return parent::init_add_date();
    }
    
    public static function getMountTypes(){
        $res = \System::run_query("SELECT * FROM sa_mount_type ORDER BY a,name");
        $allTypes = array();
        foreach($res as $line){
            $allTypes[$line['id']] = $line['name']." ".$line['a']." x ".$line['b']." x ".$line['c'];    
        }
        return $allTypes;
    } 
    
    
    public function generateNewPerfix(){
       return 'sa';
    }
    /*
    private function fillBrandAndModel(){
        $model = $this->getModel();
        $this->setValue('manufacturer',$model->getBrandName());
        $this->setValue('model',$model->name());
    }
    */
    public function fillAutoFields(){
        // Populate dumb legacy fields
        parent::fillAutoFields();
        $goods_name = $this->generateNameForGoods();
        $this->set_name($goods_name);
    }
    
    public function Validate($input = false){
        $this->fillAutoFields();
        if (! parent::Validate()){
            return false;
        }
        return $this->error == '';
    }
    
    protected function getCarsName()
    {
        $allCars = array();
        foreach ($this->bindings as $cars){
            $allCars = array_merge($allCars,$cars);
        }
        $clearedCars = array_unique($allCars);
        $names = array();
        $brands = array();
        foreach ($clearedCars as $carId){
            $car = new Car($carId);
            $brands[] = $car->getBrand()->Name();
            $names[] = $car->Name(); 
        }
        $clearedBrands = array_unique($brands);
        
        if (count($clearedBrands) > 1 ){
            return \gan4x4\Framework\Utils::createCommasList($clearedBrands);
        }
        elseif (count($clearedBrands) == 1 && count($names) > 1 ){
            return $clearedBrands[0];
        }
        return \gan4x4\Framework\Utils::createCommasList($names);
    }
    
    
    protected function generateNameForGoods(){
        return "амортизатор ".$this->getCarsName()." ".$this->getValue('vendor_id');
    }
    
    //Override
    protected function getInfoTableName()
    {
        return self::$info_table;
    }
    
    //Override
    public function getAddFormBody($input = '')
    {
        ob_start();
        print parent::getAddFormBody($input);
        CarBinding::getCarSelect($this->getBindings(),"Привязка к машинам");
        //print "CN:". $this->getCarsName();
        print "<br>";
        return ob_get_clean();
    }
    
    
    
    protected function saveBindings()
    {
        $delQuery = "DELETE FROM parts_binding WHERE part_id = '".$this->id()."'";
        if (System::run_query($delQuery) === false){
            return false;
        }
        foreach($this->bindings as $tuneId=>$carList){
            if (count($carList) == 0){
                continue;
            }
            foreach($carList as $carId){
                $insQuery = "INSERT INTO parts_binding SET part_id = '".$this->id()."' , car_id = $carId ,tune_type = $tuneId";
                print $insQuery;
                if (System::run_query($insQuery) === false){
                    return false;
                }
            }
        }
        return true;
    }
    
    
    public function Save(){
        if (parent::Save()){
            return $this->saveBindings();
        }
        return false;
    }
    
    
   
    
    private function getPattern($field,$parts = array()){
        throw new Exception("Not implemented yet");
        /*
        $bolts = $this->replaceDelimetrs($this->getValue('bolts'));
        $size = $this->getDiametr().'_'.TyreSize::round5Float($this->getWidth());
        $size = $this->replaceDelimetrs($size);
        //$et = 'ET%'.$this->getValue('offset');
        $minParts = array($bolts,$size);
        $finalParts = array_merge($minParts,$parts);
        return \System::prepareNamesToSqlLike($finalParts,$field,' AND ');
         * *
         */
    }
    
    public function getSearchPattern($field){
        //$et = 'ET%'.$this->getValue('offset');
        //return $this->getPattern($field,$parts = array($et));
        throw new Exception("Not implemented yet");
    }
    
    public function getDirtySearchPattern($field){
       //return $this->getPattern($field,$parts = array());
        throw new Exception("Not implemented yet");
    }
    
}
