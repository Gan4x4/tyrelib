<?php
namespace gan4x4\Market\Typ;
use gan4x4\Market\Car;

class CarBinding {

    public static function getCarSelect($current,$name='',$perfix = "bind")
    {
        print '
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Привязка товара к автомобилю</h4>
              </div>
                <div class="modal-body">
                    <div class="row">
                        ';
                             $brands = \System::run_query("SELECT id, name FROM car_brand ORDER BY name");
                             foreach(Car::$tuneList as $tune=>$tuneName){
                                 print "<div class='col-md-4'>";
                                 print "<h3>".$tuneName."</h4><br>";
                                 foreach ($brands as $brand){
                                    $collapseId = 'b_'.$brand['id'].'t_'.$tune;
                                    print '<a  data-toggle="collapse" href="#'.$collapseId.'" aria-expanded="false" aria-controls="collapseExample">'.$brand['name'].'</a>'."\n";
                                    $cars = \System::run_query("SELECT id, name FROM cars WHERE brand_id = ".$brand['id']." ORDER BY name");
                                    $show = 'collapse';
                                    $output = '';
                                    foreach($cars as $car){
                                        //var_dump($current[$tune]);
                                        if (isset($current[$tune]) && in_array($car['id'], $current[$tune])){
                                            $show = 'collapse.in';
                                            $checked = 'CHECKED';
                                        }
                                        else{
                                            $checked = '';
                                        }
                                        $checkBoxId = $perfix.'_'.$car['id'].'_'.$tune;
                                        $output .= "<input type='checkbox' name='$checkBoxId' $checked> ";
                                        $output .= $car['name'];
                                        $output .= "<br>\n";
                                     }
                                     print"<div class='$show' id='".$collapseId."'>";
                                     print $output;
                                     print "</div>";
                                     print "<br>";
                                 }

                                 print "</div>";
                             }
                        print '
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal" id="modalClose" >Close</button>
                </div>
            </div>
          </div>
        </div>
        
        <button type="button"  data-toggle="modal" data-target="#myModal">
            '.$name.'
        </button>
                ';
    }
    
    
    
}
