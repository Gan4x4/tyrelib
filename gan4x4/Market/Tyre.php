<?php

namespace gan4x4\Market;
use \Exception;


//require_once (__DIR__.'/TyreModel.php'); // For tyre type constant

class Tyre extends Product{
    
    // Moved to Tyre Size
    //const CORD_RADIAL = 'Радиальная';
    //const CORD_DIAGONAL = 'Диагональная';
    
    static protected $fieldsShowOrder = array(
        'price1'=>1,
        'price2'=>2,
        'amount'=>3,
        'reserv1'=>4,
        't_info_original'=>10,
        't_info_heigth'=>11,
        't_info_width'=>12,
        't_info_inch_size'=>20,
        't_info_cm_size'=>21);
    
        static $uniqueTuple = array('code','diametr','heigth','width');
    
     public function __construct($id){
        parent::__construct($id);
        $this->datalist['t_info_name']['title'] = $this->datalist['t_info_name']['title'].' (auto)';
        
    }
    
    public function dump(){
        //var_dump($this->datalist);
        print $this->getValue('original');
    }
    
    public static function createByLineFromCsvFile($line){
        $table_info = \Db_objects_list::get_table_info('t_info');
        $data = explode(";", $line);
        //var_dump($data);
        if (count($data) != count($table_info)){
            throw new Exception("Bad items count in line ".count($data)." != ".count($table_info));
        }
        $formattedInput = array();
        $tableKeys = array_keys($table_info);
        $i = 0;
        foreach($tableKeys as $key){
            $formattedInput['t_info_'.$key] = trim($data[$i]);
            $i++;
        }
        $formattedInput['_type'] = 't'; // to set table to t_info
        //var_dump($formattedInput);
        
        return new static($formattedInput);
        
    }
    
   public function getClearOriginal()
   {
       return $this->getSize()->getClearOriginal();
   }
    
    public function getSpeedCode(){
        return $this->getValue('code');
    }
    
    public function setMass($mass){
        if ($mass < 0 || ($mass > 0 && $mass < 9) || $mass > 199){
            throw new Exception("Invalid value for mass");
        }
        $this->setValue('mass',$mass);
    }
    
    public function setVendorId($vendorCode){
         $this->setValue('vendor_id',$vendorCode);
    }
    
   
    public function getHeigth(){
        $h = $this->getValue('heigth');
        return $h;
    }
    
    public function getWidth(){
        $w = $this->getValue('width');
        return $w;
    }
    
    public function getMetricSize(){
        return trim($this->getValue('cm_size'));
    }
    
    public function getInchSize(){
        return trim($this->getValue('inch_size'));
    }
    
    public function setMetricSize($size){
        $this->setValue('cm_size',$size);
    }
    
    public function setInchSize($size){
        $this->setValue('inch_size',$size);
    }
    
    // Override
    public function getModel(){
        /*
        if (! $this->model){
            $this->model = new TyreModel($this->getValue('model_id'));    
        }
        return $this->model;
         * 
         */
        return parent::getModel();
    }
    
    protected function getSize(){
        $original = $this->getValue('original');
        try {
            $ret = Size\TyreSize::parseSize($original);    
            return $ret;
        } catch (Exception $e) {
            //throw new Exception($e->getMessage()." in object with id: ".$this->id(), $e->getCode(), $e);
            //var_dump($this->datalist);
            throw new Exception("Size not parsed in object with id: ".$this->id()." original = ".$original);
        }
        
    }
    
    public function generateNewPerfix(){
        //$h = $this->getHeigth();
        
        $protector  = $this->getModel()->getTyreClass();
        //print "P:".$protector." ".PNEUMO;
        if ($protector == PNEUMO){
            return "tp";
        }elseif ($protector == WINTER){
            return "tw";
        }
        
        $h = $this->getValue('inch_h');
        
        //assert('$h > 0','invalid inch_f field value');
        if (! ($h > 0)){
            throw new Exception('invalid inch_f field value');
        }
        if ($h <=31){
            $p = 'ta';
        }
        elseif ($h < 35){
            $p = 'tb';
        }
        elseif ($h <= 36){
            $p = 'tc';
        }
        else{
            $p = 'td';
        }
        return $p;
    }
    
    protected function init_add_date(){
        parent::init_add_date();
        //$this->datalist['t_info_brand']['htm_type'] = HIDDEN;
        //$this->hideSomeFields(array('t_info_brand','t_info_model_id','t_info_brand_id','t_info_model','t_info_name','name'));
        $this->hideSomeFields(array('t_info_brand','t_info_model_id','t_info_brand_id','t_info_model','name'));
        return true;
    }
    
    private function validateInchSize(){
        $iSize = $this->getInchSize();
        if (! Size\InchTyreSize::checkSize($iSize)){
            $this->error = "Bad inch size : ".$iSize;
        }
    }
    
    private function validateMetricSize(){
        $mSize = $this->getMetricSize();
        if (! Size\MetricTyreSize::checkSize($mSize)){
            $this->error = "Bad Metric size : ".$mSize;
        }
    }
    
    
    private function extractDiskFromOriginal(){
        
        $size = $this->getSize();
        //$size = $this->getValue('original');        
        //$disk = TyreSize::extractDisk($size);
        //if (! $disk){
        //    throw new Exception("Can't extract disk size from original ".$size);
        //}
        return $size->getDisk();; 
    }
    
    
    private function validateOriginalSize(){
        
        try{
            $this->getSize();
        }
        catch (Exception $ex){
            $this->setError("Bad original size : ".$ex->getMessage());
            return false;
        }
        return true;
    }
    
    private function validateCordType(){
        
        if (Size\PneumoTyreSize::checkSize($this->getValue('original'))){
            // Arctictrans is strange
            return true;
        }
        
        $res = true;        
        $size = $this->getSize();
        $cordTypeExp = $size->getCord();
        $cordTypeAct = $this->getValue('cordtype');
        if ($cordTypeExp != $cordTypeAct){
            $this->setError("Тип корда($cordTypeAct) не соответствует зашифрованному в названии($cordTypeExp) !");
            $res =false;
        }        
       
        return $res;
     }
     
    public function validateRealSize()
    {
        $size = $this->getSize();
        
        if (get_class($size) == 'gan4x4\Market\Size\FullProfileInchTyreSize'){
            // Its a crazy size, ignore it
            return true;
            
        }
        $h = $size->getInch_H();
        $realH = $this->getHeigth();
        if (abs($h - $realH) > 2){
            $this->setError("Реальная высота($realH) отличаеться от заявленной($h) больше чем на 2 дюйма!");
            return false;
        }
        $w = $size->getInch_W();
        $realW =$this->getWidth();
        if (abs($h - $realH) > 2){
            $this->setError("Реальная ширина($realW) отличаеться от заявленной($w) больше чем на 2 дюйма!");
            return false;
        }
    }
     
    private function validateSizeLimits(){
        $h = $this->getHeigth();
        if ($h< 25 || $h > 100){
            $this->setError("Недопустимая высота($h) шины!");
            return false;
        }
        $w = $this->getWidth();
        if ( $w < 4 || $w > 40){
            $this->setError("Недопустимая ширина($w) шины !");
            return false;
        }
        return true;
    }
    
    public function validateBaseData(){
        /*
        $h = $this->getHeigth();
        if ($h< 25 || $h > 100){
            $this->setError("Недопустимая высота($h) шины!");
            return false;
        }
        $w = $this->getWidth();
        if ( $w < 4 || $w > 40){
            $this->setError("Недопустимая ширина($w) шины !");
            return false;
        }
         * 
         */
        
        $this->validateOriginalSize();
    }
    
    
     
     protected function generateNameForGoods(){
        //$h = $this-> getHeigth();
        //$w = remove_zeros($this->getWidth());
        //$pcd = $this->getPcdName();
        //$et = $this->getValue('offset');
        $model = $this->getValue('model');
        $brand = $this->getValue('brand');
        $size = $this->getValue('original');
        if (empty($size) || empty($brand) || empty($model)){
            return '';
        }
        return $brand." ".$model." ".$size;
    }
    
    
    private function dataToLine(){
        $d = $this->extactData('t_info');
        $line = array();
        foreach ($d as $key=>$data){
            if (isset($data['value'])){
                $line[$key] = $data['value'];
            }
            else{
                $line[$key] = $data['default'];
            }
        }
        return $line;
    }
    
    private function fillEmptyField($key,$value){
        $oldVal = trim($this->getValue($key));
        if  ($oldVal == '' || $oldVal == 0){
             $this->setValue($key,$value);
        }
        else{
            //print " OldVal: $key => $oldVal :: ".$value;
        }
    }
    
    private function comma2dot($fields){
        foreach($fields as $key){
            $text = $this->getValue($key);
            $textWithoutComma = strtr($text, ',', '.');
            $this->setValue($key, $textWithoutComma);
        }
    }
    
    
    private function fillBrandAndModel(){
        $model = $this->getModel();
        $this->setValue('brand',$model->getBrandName());
        $this->setValue('model',$model->name());
    }
    
    
    protected function fillAutoFields(){
        $this->fillBrandAndModel();
        $goods_name = $this->generateNameForGoods();
        $this->set_name($goods_name);
        if ($this->getValue('name') == ""){
            $this->setValue('name',$goods_name);
        }
        $size = $this->getSize();
        if ( $this->getMetricSize() == '' ){
            $this->setMetricSize($size->getMetricName());
        }
        //print "<b>!!!!!!!!!!!".$this->getInchSize()."!!!!!!!!!!</b>";
        if ($this->getInchSize() == '' ){
            $this->setInchSize($size->getInchName());
        }
        $this->fillEmptyField('cm_h',$size->getCm_H());
        $this->fillEmptyField('cm_w',$size->getCm_W());
        $this->fillEmptyField('inch_h',$size->getInch_H());
        $this->fillEmptyField('inch_w',$size->getInch_W());
        $this->fillEmptyField('diametr',$size->getDisk());
        $this->fillEmptyField('cordtype',$size->getCord());
        
        parent::fillAutoFields();
        // For very bad tyres with real size is unknown
        $this->fillEmptyField('heigth',$size->getInch_H()); 
        $this->fillEmptyField('width',$size->getInch_W()); 
        $model = $this->getModel();
        $this->fillEmptyField('protector',$model->getTyreClassName()); 
    }
    
    
    private function validateProtectorType(){
        $mainTyreClasses = $this->getModel()->getMainTyreClasses();
        $protector = $this->getValue('protector');
        //print "PR: ".$protector." vs ".var_dump($mainTyreClasses)."<hr>";
        if (! in_array($protector , $mainTyreClasses) || $protector == ''){
            //print ("--FFF--<hr>");
            var_dump($this->datalist['t_info_protector']);
            $this->setError("Недопустимый тип рисунка шины : ".$protector );
            return false;
        }
        return true;
    }
    
    private function validatePin(){
        $pinTypeList= \System::get_db_data_by_id('t_info','pin');
        $pin = $this->getValue('pin');
        
        if (! in_array($pin , $pinTypeList) || $pin == ''){
            $this->setError("Недопустимое значение поля ошиповка: ".$pin );
            return false;
        }
        return true;
    }
    
    private function validateModelName(){
        $goodName = trim($this->getModel()->name());
        $currentName = trim($this->getValue('model'));
        if ($goodName != $currentName ){
            $this->setError("Недопустимое название модели: ".$currentName." vs ".$goodName);
            return false;
        }
        return true;
    }
    
     private function validateBrandName(){
        $goodName = $this->getModel()->getBrand()->name();
        $currentName = $this->getValue('brand');
        if ($goodName != $currentName ){
            $this->setError("Недопустимое название бренда: ".$currentName." vs ".$goodName);
            return false;
        }
        return true;
    }
    
   
    
    
    public function isHasDuplicate(){
        $table = $this->getInfoTableName();
        $data = $this->extactData($table);
        //var_dump($data);
        //die();
        $fields = array('vendor_id','code','diametr','heigth','width','model_id','brand_id');
        $pairs = self::getKeyValuePairListForSql($fields,$data);
        $where = \System::create_commas_list($pairs," AND ");
        $query = "SELECT * FROM $table WHERE ".$where;
        //print $query."<br>";
        $queryResult = \System::run_query($query);
        if (count($queryResult) == 0){
            return false;
        }
        return $queryResult;
    }
    
   public function Validate($input = false){
        //die("Validate");
        //$this->fillAutoFields();
        
        $this->resetError();
        $this->comma2dot(array('inch_size','inch_h','inch_w','heigth','width','tread','skid','maxinf','mass'));
        
        $this->validateBaseData();
        //die("EM".$this->getValidateErrorMessage());
        if ($this->isError()){
            return false;
        }
        
        $this->fillAutoFields();
        
        $this->validateSizeLimits();
        $this->validateRealSize(); // Used to generatePerfix
        $this->validateInchSize();
        $this->validateMetricSize();
        $this->validateCordType();
        //$this->validateMajorFields();
            
        $this->validateProtectorType();
        $this->validatePin();
        $this->validateModelName();
        $this->validateBrandName();
        
        
        if (! $this->isError()){
            parent::Validate();
        }
        
            // Validate auto filled 
            //print "Validate auto filled";
        
       return $this->error == '';
    }
   
    
    
    
    public function exportToInfoCsvLine(){
        $this->setDefaultValues();
        $result = $this->getNewId().";";
        $tableKeys = array_keys(\Db_objects_list::get_table_info('t_info'));
        foreach($tableKeys as $key){
//            $value = $this->getValue($key);
            $result .= $this->getValue($key).";";
        }
        return $result;
    }
    
    
    
    public function getSearchPattern($field){
        
        $size = $this->getSize();
        
        //print "<hr>";
        //$common = parent::getCommonSqlPatternPart();
        $sizePattern = $size->getSqlSearchPattern();
        return \System::prepareNamesToSqlLike($sizePattern,$field);
    }
    
}
