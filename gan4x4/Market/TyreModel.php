<?php
namespace gan4x4\Market;

define('AT', 1);
define('MT', 2);
define('EXTREME', 3);
define('WINTER', 4);
define('PNEUMO', 5);


class TyreModel extends Model  {
    //put your code here
    
    public function __construct($input) {
        //$table = 't_info';
        parent::__construct($input, 'tyre_models');
    }


    protected function getDirForPicture(){
        return parent::getDirForPicture().DIRECTORY_SEPARATOR.'tyre';
    }

    protected function getViewLink(){
        //die("XXX");
        return " <a href='http://extremetyre.ru/tyre.htm?model_id=".$this->id()." '  target=new> <img src='/design/view.gif' ></a>";
    }
    
    
    private function getEtDb(){
        //var_dump($this->db_list);
        foreach ($this->db_list as $db){
            //print $db->getName()."<hr>";
            if ($db->getName() == 'extremetyre'){
                return $db;
            }
        }
        throw new \Exception("Db with table tyre_class not found");
    }
    
    private function getFromTyreClass($field){
        $db = $this->getEtDb();
        $allTypes = array();
        $res = $db->run_query("SELECT id, $field FROM tyre_class");
        foreach($res as $line){
            $allTypes[$line['id']] = $line[$field];    
        }
        return $allTypes;
    }
    public  function getMainTyreClasses(){
        /*
        $allTypes = System::get_db_data_by_id('tyre_class','name');
        $mainTypes = array();
        foreach ($allTypes as $key=>$value){
            if (intval($key) < 10){
                $mainTypes[$key] = $value;
            }
        }
        assert(count($mainTypes) > 3, "Classel list too small");
        return $mainTypes;
         * 
         */
        
        //return System::get_db_data_by_id('tyre_models','type_name','type');
        $allTypes = $this->getFromTyreClass('protector_type');
        $majorTypes = array();
        foreach ($allTypes as $key=>$value){
            if (intval($key) < 9 ){
                $majorTypes[$key] = $value;
            }
        }
        assert(count($majorTypes) < 7, "Sub Classes list too small");
        return $majorTypes;
    }
    
    
    
    
    
    public function getSubTyreClasses(){
        //$allTypes = \System::get_db_data_by_id('tyre_class','name');
        $allTypes = $this->getFromTyreClass('name');
        $minorTypes = array();
        foreach ($allTypes as $key=>$value){
            if (intval($key) > 9 ){
                $minorTypes[$key] = $value;
            }
        }
        assert(count($minorTypes) > 3, "Sub Classes list too small");
        return $minorTypes;
        
    }
    
    protected function init_add_date(){
        
        $this->datalist['type']['htm_type'] = SELECT; 
        $this->datalist['type']['sqldata'] = $this->getMainTyreClasses(); 
        
        $this->datalist['sub_type']['htm_type'] = SELECT; 
        $this->datalist['sub_type']['sqldata'] = $this->getSubTyreClasses(); 
        return true;
    }
    
    public function getTyreClass(){
        return intval($this->get_field_value('type'));
    }
    
    public function getTyreClassName(){
        $id = $this->getTyreClass();
        $classes = $this->getMainTyreClasses();
        assert('isset($classes[$id])');
        return $classes[$id];
    }
    
    public function getTyreSubClass(){
        return intval($this->get_field_value('sub_type'));
    }
    
    public function getOffRoadIndex(){
        return intval($this->get_field_value('or_index'));
    }
    
    
    private function validateClass(){
        $class = $this->getTyreClass();
        $subClass = $this->getTyreSubClass();
        if ($class == PNEUMO && $subClass == 0){
            return true;
        }
        if ( $class != round($subClass/10)){
            $this->error = " Недопустимый подтип для выранного типа шины";
        }
    }
    
    private function validateOffRoadIndex(){
        $class = $this->getTyreClass();
        $or = $this->getOffRoadIndex();

        if ($class == AT && $or > 35){
            $this->error = " У AT шин индекс проходимости не должен быть выше 35";
        }
        
        if ($class == MT && $or < 25){
            $this->error = " У MT шин индекс проходимости не должен быть шиже 25";
        }
        
        if ($class == MT && $or > 70){
            $this->error = " У MT шин индекс проходимости не должен быть выше 70";
        }
        
        if ($class == EXTREME && $or  < 55){
            $this->error = " У Экстримальных шин индекс проходимости не должен быть ниже 55";
        }
        
    }
    
    
     public function fillAutoFields(){
        $class = $this->getTyreClass();
        if ($class == WINTER){
            $this->set_field_value('or_index',0);
        }
        
        if ($class == PNEUMO){
            $this->set_field_value('or_index',99);
            $this->set_field_value('sub_type',0);
        }
        
    }
    
    public function Validate($input = false){
        $this->fillAutoFields();
        if (parent::Validate($input)){
            $this->validateClass();
            $this->validateOffRoadIndex();
        }
        
        return $this->error == '';
    }
    
    
    
    // Override
    public function clearCache($cacheFilesToDel = false){
        $fn = Utils::get_cache_fn('tyre.htm','',"model_id=".$this->id());
        $fn1 = Utils::get_cache_fn('tyre.htm','','brand_id='.$this->getBrand()->id().'&model_id='.$this->id());
        $cacheFilesToDel = array($fn,$fn1);
        parent::clearCache($cacheFilesToDel);
    }
}
