<?php

namespace gan4x4\Market;

class Utils {
    //public $paths;
    
    public static function get_cache_fn($file_name,$add = '',$qstring = false)
    {
        $etCachePath = "/var/www/sites/extremetyre/public_html/cache/";
        if ($qstring == false){
            $qstring = $_SERVER['QUERY_STRING'];
        }
        if ($add !='') {
            $add .= '_';
        }
        $pi=pathinfo($file_name);
        $bad_symbol=array('?','&','=','/');
        $good_symbol=array('_','_','_','_');
        $p_add=str_replace($bad_symbol,$good_symbol,$qstring);
        $result = $etCachePath.$add.$pi['filename'].$p_add.'.html';
        return $result;
    }
    
    public static function remove_zeros($std){
        if (strpos($std, '.') === false) {
            return $std;
        }
        while ( strlen($std)> 0 &&  in_array(substr($std,-1),array('0','.'))){
            $deleted = substr($std,-1);
            $std =  substr($std, 0, strlen($std)-1);
            if ($deleted == '.') {
                break;
            }
        }
        return $std;
    }
    
    // if return value with key equals $perfix
    // if that key don't exist return value where key equal to ferst character in perfix
    public static function findByPerfix($perfix,$array){
        $fullType = $perfix;
        $baseType = $perfix{0};
        if (isset($array[$fullType])){
             $value = $array[$fullType];
        }
        elseif(isset($array[$baseType])){
             $value = $array[$baseType];
        }
        else{
            $value = false;
        }
        return $value;
    }
    
    
    
    
    private static function is_red_day($day)
    {	
        $red_days=array('01-01','02-01','03-01','04-01','05-01','06-01','07-01','23-02','08-03','01-05','02-05','09-05','12-06','04-11');
        $day_s=date('d-m',$day);
        if (in_array($day_s,$red_days)) { return TRUE;};
        $day_ow=date('w',$day);
        if (($day_ow==0) || ($day_ow ==6)) {return TRUE;};
        return FALSE;
    }

    public function getAnswerTime($current = false)
    {
        $ONE_DAY = 86400;
        if (! $current ){
            $day  = mktime(0, 0, 0, date("m")  , date("d"), date("Y")); // today
            $query_time = date('G');
        }else{
            $day = $current;
            $query_time = date('G',$current);
        }
        $answer_day = $day;
        $answer_time = 0;
        $check = 0;

            while ((self::is_red_day($answer_day)) || ($answer_time == 0))
            {
                if (self::is_red_day($answer_day) || $query_time > 16){
                    $answer_time = 14;
                    $answer_day += $ONE_DAY;
                    $query_time = 11; // reset query time
                }
                elseif ($query_time < 12) {
                    $answer_time = 14;
                }
                elseif ($query_time <= 16) {
                    $answer_time = 18;
                }

                $check++;
                assert('$check < 100');
            }

            //$i=$i-1;
            $i = round(($answer_day-$day) / $ONE_DAY); 
            if ($i==0) {$postfix=' сегодня ';}
            if ($i==1) {$postfix=' завтра ';}
            if (($i>1) && ($i<5)) {$postfix='через '.$i.' дня';};
            if ($i>4) { 
                $postfix=' через '.$i.' дней';
            }

            $res_str=$postfix." до ".$answer_time.":00";
            return 	$res_str;

    }

    public static function getInterval($start,$finish)
        {
            //$start = strtotime($this->get_field_value('start_date'));
            //$finish = strtotime($this->get_field_value('end_date'));
            $months = array( 1 => 'января', 2 => 'февраля',
                3 => 'марта', 4 => 'апреля', 5 => 'мая',
                6 => 'июня', 7 => 'июля', 8 => 'августа', 9 => 'сентября', 10 => 'октября', 11 => 'ноября',  12 => 'декабря');
            
            //$day = 
            if (Date('Y',$start) == Date('Y',$finish)){ 
                $y = Date('Y',$start);
                if (Date('n',$start) == Date('n',$finish)){
                    $m = Date('n',$start);
                    if (Date('d',$start) == Date('d',$finish)){
                        // One day
                        $d = Date('d',$start);
                        $text = $d."-го ";                          
                    }else{
                        $text = "c ".Date('d',$start)." по ".Date('d',$finish);
                    }       
                    // One month
                    $text .= " ".$months[$m]; 
                }
                else{
                    $text = "c ".Date('d',$start)." ".$months[Date('n',$start)]." по ".Date('d',$finish)." ".$months[Date('n',$finish)];
                }
                if ($y != Date('Y')){
                    // Not current
                    $text .= " ".$y." г.";
                }
            }  else {
                
                $text = "c ".Date('d',$start)." ".$months[Date('n',$start)]." ".Date('Y',$start)." по ".Date('d',$finish)." ".$months[Date('n',$finish)]." ".Date('Y',$finish);
            }
            
            
            return $text;
        }
            
    

    
}
