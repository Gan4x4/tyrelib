<?php

namespace gan4x4\Market;
use gan4x4\Market\Size\TyreSize;

class Wheel extends Product{
    static $info_table = 'd_info';
    static $pcdList = false;
    static protected $fieldsShowOrder = array(
        'price1'=>1,
        'price2'=>2,
        'amount'=>3,
        'reserv1'=>4,
        'd_info_diametr'=>11,
        'd_info_width'=>12,
        'd_info_offset'=>20,
        'd_info_bolts'=>21,
        'd_info_stupicha'=>22);
    
    static $uniqueTuple = array('diametr','width','offset','stupicha');
    
    public function __construct($id){
        parent::__construct($id);
        //$this->disableSomeFields(array('image','cat'));
        //$this->hideSomeFields(array('d_info_model_id','d_info_brand_id','d_info_manufacturer','d_info_model','d_info_name','name'));
        $this->hideSomeFields(array('d_info_model_id','d_info_brand_id','d_info_manufacturer','d_info_model','d_info_name'));
       
        //print "A";
        
        //fillAutoFields();
        
        //print $this->getPcdDescription();
    }
    
     public static function createByLineFromCsvFile($line){
        $table_info = \Db_objects_list::get_table_info('d_info');
        $data = explode(";", $line);
        //var_dump($data);
        if (count($data) != count($table_info)){
            throw new Exception("Bad items count in line ".count($data)." != ".count($table_info));
        }
        $formattedInput = array();
        $tableKeys = array_keys($table_info);
        $i = 0;
        foreach($tableKeys as $key){
            $formattedInput['d_info_'.$key] = trim($data[$i]);
            $i++;
        }
        $formattedInput['_type'] = 'd'; // to set table to d_info
        //var_dump($formattedInput);
        
        return new static($formattedInput);
        
    }
    
    public function exportToInfoCsvLine(){
        $this->setDefaultValues();
        $result = $this->getNewId().";";
        $tableKeys = array_keys(\Db_objects_list::get_table_info('d_info'));
        foreach($tableKeys as $key){
            $result .= $this->getValue($key).";";
        }
        return $result;
    }
    
    
    protected function init_add_date(){
        parent::init_add_date();
        $this->datalist['d_info_bolts']['htm_type'] = SELECT; 
        //$pcdList = \System::get_db_data_by_id('wheel_pcd','name','name');   
        $pcdList =\System::get_db_data_by_id('d_info','bolts','bolts');
        $this->datalist['d_info_bolts']['sqldata'] = $pcdList; 
        
         // Set default material for new object only
        $currentMaterial = $this->getValue('maidein');
        if (empty($currentMaterial)){
            $defaultMaterial = $this->getModel()->getMaterial();
            
            $this->setValue('maidein',$defaultMaterial);
        }
        
        $currentColor = $this->getValue('color');
        if (empty($currentColor)){
            $defaultColor = $this->getModel()->getColor();
            //var_dump($defaultColor);
            $this->setValue('color',$defaultColor);
        }
        
        $beadlock = $this->getValue('beadlock');
        if (empty($beadlock)){
            $defaultBeadlock = $this->getModel()->getBeadlock();
            $this->setValue('beadlock',$defaultBeadlock);
        }
        
        
        return true;
    }
    
    private function fillBrandAndModel(){
        $model = $this->getModel();
        $this->setValue('manufacturer',$model->getBrandName());
        $this->setValue('model',$model->name());
    }
    
    public function generateNewPerfix(){
        $pcdName = $this->getPcdName();
        $perfix = 'da'; // Прочие
        $pcdNameToPerfix = array(
            'dt'=>array('TLC-105','TOY'),
            'du' => array('УАЗ'),  
            'dj' => array('JEEP','WRL JK'),  
            'dl' => array('LR D II','LR','LR D III'),  
        );
        
        foreach ($pcdNameToPerfix as $pfx=>$names){
            if (in_array($pcdName, $names)){
                $perfix = $pfx;
                break;
            }
        }
        return $perfix;
    }
    
    private function changePcdDescription(){
        $pcd = $this->getValue('bolts');
        $descList = $this->getPcdDescription($pcd);
        $currDesc = $this->getValue('avto');
        if (! in_array($currDesc, $descList)){
            $this->setValue('avto',$descList[0]);
            print "Description changed from ".$currDesc." to ".$descList[0]."<br>\n";
            
        }
    }
    
    public function fillAutoFields(){
        // Populate dumb legacy fields
        parent::fillAutoFields();
        
        $this->fillBrandAndModel();
        if ($this->name() == '' ){
            $goods_name = $this->generateNameForGoods();
            $this->set_name($goods_name);
        }
        $this->datalist['d_info_name']['value'] = $this->name();
        $site = $this->getValue('site');
        if (substr($site,0,4) == 'http'){
            $href = "<a href='$site'>".$this->getModel()->name()."</a>";
            $this->setValue('site',$href);
        }
        
        $this->changePcdDescription();
    }
    
    public function Validate($input = false){
        $this->fillAutoFields();
        if (! parent::Validate()){
            return false;
        }
        
        $diam = $this->getDiametr();
        if (! ctype_digit($diam)){
            $this->error = " Диаметр диска должен быть целым числом : ".$diam; 
        }
            
        if ($diam < 15 || $diam > 22){
            $this->error = " Недопустимый диаметр диска : ".$diam; 
        }
        
        $width = $this->getWidth();
        if (! is_numeric($width)){
            $this->error = " Ширина диска должна быть числом. Возможно не целым : ".$width; 
        }
        
        if ($width < 6 || $width > 23){
            $this->error = " Недопустимая ширина диска : ".$width; 
        }
        //var_dump($_REQUEST);
        //print $this->generateNameForGoods();
        //die();
        return $this->error == '';
    }
    
    protected function generateNameForGoods(){
        $d = $this->getDiametr();
        $w = Utils::remove_zeros($this->getWidth());
        $pcd = $this->getPcdName();
        $et = $this->getValue('offset');
        $model = $this->getValue('model');
        return $d."x".$w." ".$pcd." ET ".$et." ".$model;
    }
    
    //Override
    protected function getInfoTableName(){
        return self::$info_table;
    }
    
    public function getDiametr(){
        return $this->getValue('diametr');
    }
    
    public function getWidth(){
        return $this->getValue('width');
    }
    public function getEt(){
        return $this->getValue('offset');
    }
    
    /*
    public static function getPcdList(){
        if (! self::$pcdList){
            self::$pcdList = System::get_db_data_by_id('wheel_pcd','name');    
        }
        return self::$pcdList;
    }
    */
    
    private function getPcdName(){
        $pcdList = \System::get_db_data_by_id('wheel_pcd','sinonim','name');    
        
        $bolts = $this->getValue('bolts');
        //var_dump($bolts);
        $pcds = explode(', ',$bolts);
        // some Mickey Thomson disk has multiple holes pack
        $names = [];
        foreach ($pcds  as $pcd){
        //var_dump($pcdList);
            foreach ($pcdList as $key=>$value){
                if (trim($pcd) == $key) {
                    $names[] = $value;
                }
            }
        }
        
        if (count($names) == 0){
            throw new \Exception("PCD '".$bolts."' not found!");    
        }
        elseif (count($names) == 1)
        {
            return trim($names[0]);
            
        }
        else{
            return implode('/', $names);
        }
        
    }
    
    private function getPcdDescription($bolts){
        $result = array();
        //var_dump($bolts);
        $allBolts = explode(", ",$bolts);
        $pcdList = \System::run_query("SELECT DISTINCT avto FROM `d_info` WHERE bolts IN ('".implode("','",$allBolts)."') ");
        foreach  ($pcdList as $line ){
            $result[] = $line['avto'];
        }
        //var_dump($result);
        return $result;

    }
    
    
    public function Save(){
        return parent::Save();
        
        
    }
    
    
    private function replaceDelimetrs($name){
        $replacePairs = array();
         $toUnderline = array('x','х','.',',');
        foreach ($toUnderline as $from){
            $replacePairs[$from] = '_';
        }
        return strtr($name,$replacePairs);
    }
    /*
    // http://habrahabr.ru/sandbox/42250/
    public static function permutation($arr) 
    {
        if(is_array($arr)&&count($arr)>1) {
            foreach($arr as $k=>$v) {
                $answer[][]=$v;
            }
            do {
                foreach($arr as $k=>$v) {
                    foreach($answer as $key=>$val) {
                        if(!in_array($v,$val)) {
                            $tmpArr[]=array_merge(array($v),$val);
                        }
                    }
                }
                $answer=$tmpArr;
                unset($tmpArr);
            }while(count($answer[0])!=count($arr));
            return $answer;
        }else{
            $answer=$arr;
        }
        return $answer;
    }
    */
    
    private function getPattern($field,$parts = array()){
        $bolts = $this->replaceDelimetrs($this->getValue('bolts'));
        $size = $this->getDiametr().'_'.TyreSize::round5Float($this->getWidth());
        $size = $this->replaceDelimetrs($size);
        //$et = 'ET%'.$this->getValue('offset');
        $minParts = array($bolts,$size);
        $finalParts = array_merge($minParts,$parts);
        
        $normalSize = \System::prepareNamesToSqlLike($finalParts,$field,' AND ');
        $size2 = TyreSize::round5Float($this->getWidth()).'_'.$this->getDiametr();
        $size2 = $this->replaceDelimetrs($size2);
        
        $minParts = array($bolts,$size2);
        $finalParts = array_merge($minParts,$parts);
        $invertedSize =  \System::prepareNamesToSqlLike($finalParts,$field,' AND ');
        return " ($normalSize) OR ($invertedSize) ";
    }
    
    public function getSearchPattern($field){
        $et = 'ET%'.$this->getValue('offset');
        return $this->getPattern($field,$parts = array($et));
    }
    
    public function getDirtySearchPattern($field){
       return $this->getPattern($field,$parts = array());
    }
    
     
    
    // Override    
    public function getVendorId(){
        $rawId = $output =  parent::getVendorId();
        $colors = ['черный'=>['B','BL'],'белый'=>['W','WH'],'хром'=>['C','CH']];
        
        if (in_array($this->getModel()->id(),array(53,54,76)) &&
            $this->getValue('maidein') == 'Сталь' &&
            $this->getValue('stupicha') > 0){
            // generate id's for ORW disks
            $size = $this->getDiametr().floor($this->getWidth());
            if (strlen($size) < 4){
                $size = $size.'0';
            }
            $rawPcd = $this->getValue('bolts');
            $pcdParts = explode('x',$rawPcd);
            $pcdCode = $pcdParts[0].substr($pcdParts[1],1,2);
            
            
            $dia = floor($this->getValue('stupicha')); 
            if ($dia == 111){
                $dia = '110';
            }
            if ($dia > 99 || strlen($dia) > 2){
                $dia = substr($dia,1);
            }
            $rawColor = mb_strtolower(trim($this->getValue('color')));
            $et = $this->getEt();
            if ($et > 0) {
                $et = '+'.$et; 
            }
            //print "Core";
            if (isset($colors[$rawColor])){
                // generate additional ids
                if (! empty($rawId)){
                    $variants = explode(',',$rawId);
                }else{
                    $variants = [];
                }
                
                
                $base = $size.'-'.$pcdCode.$dia;

                foreach ($colors[$rawColor] as $col){
                    $variants[] = $base.$col.$et;
                    $variants[] = $base.$col.$et.'-4S'; // for 4x4sport disk
                    $variants[] = $base.$col.$et.'A17'; // for Autoventuri disk
                    
                    $variants[] = $base.$et.$col; // Vezdehod
                    
                    if ($pcdCode == '514'){
                        // for strange Vezdehod jeep disk
                        $variants[] = $size.'-543'.$dia.$et.$col;
                    }
                }
                $output = implode(',',  array_unique($variants));
            }
            else{
                //print "CNF";
            }
        }
        elseif($this->getModel()->getBrand()->id() == 5){
            // Mickey Thompson
            
            if (strpos(strval($rawId), '9000000') === 0){
                $var = explode(',',$rawId);
                $var[] = substr($var[0],-4);
                $output = implode(',',  $var);
            }
        }
       
        return $output;
    }
  
}
