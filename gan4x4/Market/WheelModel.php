<?php

namespace gan4x4\Market;

class WheelModel extends Model  {

    function __construct($input,$table = 'wheel_models',$table_name = ''){
        parent::__construct($input, $table, $table_name);
        
        if ($this->id()){
            $currentMaterial = $this->get_field_value('material');
            if (empty($currentMaterial)){
                $material =  $this->getDefaultMaterial();
                $this->set_field_value('material',$material);
            }

            $currentColor = $this->get_field_value('color');
            if (empty($currentColor)){
                $color =  $this->getDefaultColor();
                $this->set_field_value('color',$color);
            }
        }
        
        //var_dump($this->datalist);
    }
    
    protected function getDefaultMaterial(){
        $id = $this->id();
        if (empty($id)){
            return false;
        }
        $result =  \System::run_query("SELECT maidein FROM d_info WHERE model_id = ".$this->id()." GROUP BY maidein"); 
        if (count($result) > 1 ){
            throw new \Exception("Model ".$this->name()." has two disk with distinct material");
        }
        if (count($result) == 0){
            throw new \Exception("Model ".$this->name()." has't material");
        }
        return $result[0]['maidein'];
    }
    
    protected function getDefaultColor(){
        $id = $this->id();
        if (empty($id)){
            return false;
        }
        $result =  \System::run_query("SELECT color FROM d_info WHERE model_id = ".$this->id()." GROUP BY color"); 
        //var_dump($result);
        return $result[0]['color'];
    }
    
    
    public function getColor(){
        return $this->get_field_value('color');
    }
    
    public function getMaterial(){
        return $this->get_field_value('material');
    }
    
    public function getBeadlock(){
        return $this->get_field_value('beadlock');
    }
    
    protected function getDirForPicture(){
        return parent::getDirForPicture().DIRECTORY_SEPARATOR.'wheel';
    }
    
    protected function getViewLink(){
        //die("XXX");
        return " <a href='http://www.extremetyre.ru/wheel.htm?model_id=".$this->id()." '  target=new> <img src='/design/view.gif' ></a>";
    }
    
    // Override
    public function clearCache($cacheFilesToDel =  false){
        $fn = Utils::get_cache_fn('wheel.htm','',"model_id=".$this->id());
        $fn1 = Utils::get_cache_fn('wheel.htm','','type=0&brand_id='.$this->getBrand()->id().'&model_id='.$this->id());
        $cacheFilesToDel = array($fn,$fn1);
        parent::clearCache($cacheFilesToDel);
    }
    
    /*
    protected function init_add_date(){
        parent::init_add_date();
        $this->datalist['material']['htm_type'] = SELECT; 
        //$d = \System::run_query("SELECT DISTINCT maidein FROM d_info "); 
        $d = \System::get_db_data_by_field('d_info','maidein');
        $r = array();
        foreach($d as $key=>$value){
            $r[$value] = $value; 
        }
        //var_dump($d);
        $this->datalist['material']['sqldata'] = $r; 
        //$pcdList = \System::get_db_data_by_id('wheel_pcd','name','name');   
        //$this->datalist['d_info_bolts']['sqldata'] = $pcdList; 
        return true;
    }
    */
}
