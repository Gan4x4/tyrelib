<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of class_complex_db_object
 *
 * @author Administrator
 */

require_once(dirname(__FILE__).'/class_db_object.php');

class complex_db_object extends Db_object{
    //put your code here
    public $linked_fields = false;
    protected $db_list = null;
    //public $ext_datalist =array();
            
    public function __construct($id,$table,$table_name = '',$more_fields = false)
	{		
            parent::__construct($id,$table,$table_name);
            if ($this->datalist === false) 
            {
                //$this = null;
                throw new Exception("Can't find data for table ".$table_name);            
            };
            
            if (is_array($more_fields))
            {
                
                $ext_datalist  = $ext_data =array();
                foreach ($more_fields as $value)
                {
                    $ext_data['table_name'] = $value[0];
                    $ext_data['connect_key'] = $value[1];
                    if (isset($value[2]) && is_array($value[2]))
                    {
                        $ext_data['fields_to_get'] = $value[2];
                    }
                    else
                    {
                        $ext_data['fields_to_get']= false;    
                    };

                    $connected_id = $this->get_field_value($ext_data['connect_key']);

                    $tmp_datalist = Db_objects_list::get_table_info($ext_data['table_name']);
                    if ($tmp_datalist === false) throw new Exception("Can't find data for table ".$table_name);

                    if (($connected_id !== false) && (! is_array($id)))
                    {
                        // Fill data from db
                        $fields = System::$cache->get_data($ext_data['table_name'],$connected_id);
                        foreach ($fields as $key=>$value)
                        {
                            $tmp_datalist[$key]['value'] = $value;
                        };
                    }
                    elseif (! is_array($id))
                    {
                        System::Log_Error('Bad connect key :'.$ext_data['connect_key']);
                    };

                    // Change keys
                    
                    //
                    //$this->datalist[$ext_data['connect_key']]['htm_type'] = IGNORE;
                    foreach ($tmp_datalist as $key=>$value)
                    {
                        $new_key = $ext_data['table_name'].'_'.$key;
                        $ext_datalist[$new_key] = $value;   
                    };

                    if ( is_array($id))
                    {
                        // Fill data by input array
                        foreach ($id as $key=>$value)
                        {
                            if (isset($ext_datalist[$key])  && (! isset($ext_datalist[$key]['value']))) 
                            {
                                //if (Db_objects_list::isEnum($ext_datalist[$key]['type'])){
                                //    $ext_datalist[$key]['value'] = getEnumValues( $table, $field )$value;
                                //}
                                //else{
                                    $ext_datalist[$key]['value'] = $value;
                                //}
                            };
                        };
                    };

                    $this->datalist=array_merge($this->datalist,$ext_datalist);
                    //var_dump($this->datalist);
                    //$this->datalist[$ext_data['connect_key']]['htm_type'] = IGNORE;

                    $this->linked_fields[$ext_data['table_name']]['connect_key'] =  $ext_data['connect_key'];
                    $this->linked_fields[$ext_data['table_name']]['fields_to_get'] = $ext_data['fields_to_get'];
                };
            };
            //if (is_array($value_list)) $this->create_by_list($value_list,$client);
    }
    
    //protected function &getDbList(){
    //    return $this->db_list;
    //}
    
    public function cloneDbList($complex_db_object){
        //assert()
        $this->db_list = $complex_db_object->db_list;
    }
    
    public function setDbList($sites)
    {
        // TO DO refactor, use system
        $db_list = array();
        $query_list_win_1251=array('SET collation_connection = cp1251_general_ci',' SET collation_database = cp1251_general_ci',' SET collation_server = cp1251_general_ci ', ' SET character_set_client = utf8 ' , ' SET character_set_connection = cp1251 ' , '  SET character_set_database = cp1251 ' , ' SET character_set_results = utf8 ' , ' SET character_set_server = cp1251' , " SET character_set_server = 'cp1251' ");
        //$query_list_utf8 = $g_config['my_sql_init_queryes'];
        $query_list_utf8 = System::get_config_var('my_sql_init_queryes');
        foreach ($sites as $site)
        {
            if ($site['enabled'] == false) continue;
            $ql = $query_list_utf8;
            if ($site['utf8'] == false ) $ql =  $query_list_win_1251;
            $d = new DB_pdo($site['db_name'], $site['db_login'], $site['db_password'],$site['ip'],$ql); 
            if ($d != null) {
                $db_list[] = $d;
            }
        };        
        if (count($db_list) > 0 ) {
            $this->db_list = $db_list;
        }
    }
        
    public function getConnectedKeyValue($table_name){
        $key = $this->linked_fields[$table_name]['connect_key'];
        return $this->get_field_value($key);        
    }
       
                
    
   
    public function saveOneTable($table_name,$data,$newId = false)    {
        
        // Save object to db
        $ret=false;
        if (! empty($table_name))
        {
            $v=$k=$pair=array();
            //var_dump($data);
            foreach($data as $key=>$value) 
            {
                if (($key != 'id' ) && (isset($value['value'])))
                {
                    if (Db_object::ekran($value['type'])) {
                        //$value['value'] =" '".$value['value']."' ";
                        $value['value'] = System::escape_string($value['value']);
                    }
                    
                    
                    //if (Db_object::ekran($value['type'])) {$value['value'] =System::escape_string($value['value']);}
                    $v[]=$value['value'];
                    $k[]=$key;
                    $pair[] = $key." = ".$value['value'];
                }
                //else{
                //print "Skip $key ".$value['value']."<br>";
                //}
            }
            
            
            
            if (! isset($data['id']['value']) ||  $data['id']['value'] == NULL) 
            {
            // Create new object
                //$id=$this->getNewId();
                assert($newId != false);
                $id = $newId;
                $v[]=System::escape_string($id);
                $k[]='id';
                
                $query="INSERT INTO ".$table_name."( ".System::create_commas_list($k)." ) VALUES ( ".System::create_commas_list($v)." )";
                //die($query);
                if ($this->runMultiDbQuery($query) !== False) {
                    
                    //$this->datalist['id']['value'] = 
                    $ret = $id;
                }
                else {
                    
                    $ret = false;
                };
            }
            else
            {
                
                //print " GGG : <hr>";
                //var_dump($data['id']);
                $query="UPDATE ".$table_name." SET ".System::create_commas_list($pair)." WHERE id = ".System::escape_string($data['id']['value']);
                //print $query;
                if ( $this->runMultiDbQuery($query) !==False ) {
                    $ret=$data['id']['value'];
                }
            };
            
        }
        if ($ret !== False) System::$cache->flush($table_name);
        //print "<hr>$query<hr>";
        return $ret;
    }
   
    
   public function runMultiDbQuery($query)
   {
       $res = false;
       if ( is_array($this->db_list))
       {
           //print "<h3>Save to many base </h3>";
           foreach ($this->db_list as $db)
           {
               try {
                    if (System::run_query($query,$db) !== false) {
                        $res = true;
                    }    
               } catch (Exception $exc) {
                   // Some sites may not have apopriate tables
                   error_log("Not saved to db: ".var_export($this->db_list,true));
               }
               //var_dump($db);
               //var_dump($query);
               // Clear cache on Et 
           }
           //die();
       }
       else
       {
           //print "<h3>Save to one base </h3>";
           $res = System::run_query($query);
       }
       return $res;
   }
    
    public function extactData($table_name)
    {
        $ret = array();
        foreach($this->datalist as $key=>$value)
        {
            
            if ($value['table'] != $table_name ) continue;
            // For this object fields
            $new_key = $key;

            if ($table_name != $this->table_name)
            {
                //For another objects
                if ((strpos($key,$table_name) === false))
                {
                    System::Log_Error('Table name : '.$table_name.' not found in datalist');
                };
                $new_key = substr($key,strlen($table_name)+1);
            }
            $ret[$new_key] = $value;
        }
        
        //print  "<hr> ===================";
        //var_dump($ret);
        //print  "<hr> ===================";
        if ((! isset($ret['id']['value']) || (empty($ret['id']['value']))) && ($table_name != $this->table_name))
        {
        // Add id for external objects
            assert($table_name != $this->table_name);
            //Print "NOW HERE";
            $ret['id']['value'] = $this->getConnectedKeyValue($table_name);
            $ret['id']['table'] = $table_name;
            $ret['id']['type'] = 'int';
            $ret['id']['title']= 'key';
        };
        return $ret;
    }
    
   


   
   public function getTables()
   {
       $tables_to_save = array($this->table_name);
       if ($this->linked_fields)
       {
            $external_tables =  array_keys($this->linked_fields) ;
            $tables_to_save = array_merge($tables_to_save,$external_tables);
       };
       return $tables_to_save;
   }
            
            
   public function Save()    {
        
       // Save object to db
       // save main table
       
       
       $tables_to_save = $this->getTables();
       $newId = false;
       if (! $this->id()){
           $newId = $this->getNewId();
       } 
      
       foreach ($tables_to_save as $table){
           $data = $this->extactData($table);
           if ($this->saveOneTable($table,$data,$newId) == False) {
               return False;  
           }
       }
       if ($newId){
           $this->datalist['id']['value'] = $newId;
       }
       return True;
       
       
    }
    
    private function getDeletedLineId($table){
        if ($table == $this->table_name){
            $id = $this->id();
        }
        else{
            $id = $this->getConnectedKeyValue($table);
        }
        return $id;
    }
    
    public function Delete()    {
        if ( $this->id() != ''){
            $tables_to_delete = $this->getTables();
            foreach ($tables_to_delete as $table){
                $query = "DELETE FROM `".$table."` WHERE id = '".$this->getDeletedLineId($table)."'";
                //print $query;
                if ( ! $this->runMultiDbQuery($query)){
                    print $query;
                    throw new Exception("Delete failed : ".$query);
                }
            }
        }
        else{
            return false;
        }
        unset($this);
        return true;
    }
    

    
}

?>
