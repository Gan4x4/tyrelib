<?php
require_once (dirname(__FILE__).'/class_system.php');

interface DB_int
{
     public function run_query($query);
     public function run_is_query($query);
     public function escape_string($string);
     public function getLastError();
     public function getName();
}

abstract class  DB_base implements DB_int {
    protected $sql = null;
    protected $db_name = false;
    
    public function DbRunSomeQueryes($ql)
    {   
        if (! is_array($ql)){
            if (trim($ql) == ''){
                return false;
            }
            $ql = array($ql);
        }
        if (count($ql) == 0) {
            return false;
        }
        foreach ($ql as $query){
            if ($this->run_query($query) == false) {
                return false ;
            }
        }
        return true;
    }
    
    public function getName()
    {
        return $this->db_name;
    }
    
    
}


class DB_mysql extends DB_base implements DB_int
{
    private $is_name = 'information_schema';
    
    public function __construct($name,$user,$pass,$host='localhost',$add_query = '',$is = 'information_schema')
    {
        
        $this->sql = mysql_connect($host, $user, $pass);
        if ($this->sql)
        {
            if (! mysql_select_db($name))
            {
                System::Log_error( "Could not select db ". mysql_error() );
                return false;
            };
            $this->DbRunSomeQueryes($add_query);
        }
        else{
            System::Log_error( "Could not connect to mysql ". mysql_error() );
        };
        // Information schema
        $this->is_name = $is;
        $this->db_name = $name;
    }

    public static function fetchFromRes($res)
    {
        if ($res == False) return False;
        
        $data=array();
        if (is_resource($res))
        {
            $i=0;
            while ($line = mysql_fetch_array($res, MYSQL_ASSOC))
            {
                $data[$i]=$line;
                $i++;
            }
        }
        return $data;
    }
    
    
    public function run_query($query)
    {
        $res=mysql_query($query,$this->sql);
        if (! $res)
        {
            System::Log_error( " Query not completed : ".$query." | " . mysql_error() );    
            return false;
        };
        
        return self::fetchFromRes($res);
    }
    
    public function run_is_query($query)
    {
        if (! mysql_select_db($this->is_name,$this->sql))
        {
            System::Log_error( "Not connect connect to  ". $db." ".mysql_error() );
            return false;
        };
        $res=mysql_query($query,$this->sql);
        if (! $res)
        {
            System::Log_error( " Query not completed : ".$query." | " . mysql_error() ); 
            $res = false;
        };
        mysql_select_db($this->db_name,$this->sql);
        return self::fetchFromRes($res);
    }
    
    public function escape_string($string)
    {
        return "'".mysql_real_escape_string($string,$this->sql)."'";
    }
    
    public function getLastError()
    {
        return mysql_error();
    }
    
}



class DB_pdo implements DB_int {
    private $db_name;
    protected $is_sql = null;
    
    public function __construct($name,$user,$pass,$host='localhost',$add_query = '',$driver = 'mysql',$is = 'information_schema')
    {
        //try {
            //$this->sql = new PDO($driver.':host='.self::getPortFromHost($host).';dbname='.$name.';charset=utf8'.$port, $user, $pass);
            $this->sql = new PDO($driver.':host='.self::getPortFromHost($host).';dbname='.$name.';charset=utf8', $user, $pass);
            $this->is_sql = new PDO($driver.':host='.self::getPortFromHost($host).';dbname='.$is.';charset=utf8', $user, $pass);
            $this->db_name = $name;
        //} 
        //catch (PDOException $e) {
        //    System::Log_Error($e->getMessage());
        //    
        //}
    }
    public function getName()
    {
        return $this->db_name;
    }
    public static function getPortFromHost($host_name)
    {
        $res = explode(':',$host_name);
        $al = count($res)-1; 
        $pval = intval($res[$al]);
        if ($pval > 10 && $pval < 100000)
        {
            $i =strrpos($host_name,':');
            return substr($host_name,0,$i).";port=".$res[$al];
        }
        return $host_name;
    }
    
    public function run_query($query)
    {
        //var_dump("<hr>".$query);
        $res=$this->sql->query($query,PDO::FETCH_ASSOC);
        if ($res === False){
            $errorInfo = $this->sql->errorInfo();
            System::Log_error( " Query not completed : ".$query." | " . var_export($errorInfo,true) );    
            throw new Exception(" Query not completed : ".$query." | " . var_export($errorInfo,true) );
            //return false;
        }
        return $res->fetchAll();
    }
    
    public function run_is_query($query)
    {
        $res = $this->is_sql->query($query);
        if ($res === False)
        {
            System::Log_error( " Query not completed : ".$query." | " . errorInfo() ); 
            return false;
        }
        return $res->fetchAll();
    }

    public function escape_string($string)
    {
       return  $this->sql->quote($string);
    }
    
    public function getLastError()
    {
        $ei = $this->sql->errorInfo();
        return $ei[2];
    }
    
    public function import($sql)
    {
        $this->sql->setAttribute(PDO::ATTR_EMULATE_PREPARES, 0);
        if ( $this->sql->exec($sql) === False)
        {
            return False;
        };
        return True;
    }

}

?>
