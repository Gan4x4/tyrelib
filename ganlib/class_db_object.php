<?php

include_once('class_html_form.php');


define('FILTER',5);
define('ACT_EDIT',2);
define('ACT_UPDATE',3);
define('ACT_DELETE',4);
define('ACT_CREATE',6);

class Db_object{
    protected $datalist=array();
    protected $table_name = '';
    
    public function __construct($id,$table,$table_name = '')
    {
        try{
            $this->table_name = $table_name;
            if (! is_array($table)) {
                $this->datalist = Db_objects_list::get_table_info($table);
                $this->table_name = $table;
            }
            else $this->datalist = $table;
            if ((! is_array($id)) && (($id > 0) || is_string($id)))
            {
		if (is_string($id)) {
                    $id = System::escape_string($id);
                };
                $query = "SELECT * FROM `".$table."` WHERE id = ".$id;
                $res = System::run_query($query);
                //print $query."<br>";

                if (count($res) != 1) throw new Exception("Line with id $id not found in ".$table );
                $line = $res[0];
                //$line = mysql_fetch_array($res, MYSQL_ASSOC);
                $this->table_name = $table;
            }
            else {
                $line=$id;
            }
            
            if (is_array($line))
            {
                self::readDataFromArrayToDatalist($line,$this->datalist);
                //print "<br> !!!!! ".$this->datalist['construction']['value']."!!!!<br>";
                //var_dump($line);
                /*
                foreach($this->datalist as $key => $value)
                {
                    if (isset($line[$key])) {
                        $this->datalist[$key]['value']=$line[$key];
                    }
                    else{
                       //print ("<hr>setr def : ".$key."<hr>");
                       $this->datalist[$key]['value']=$this->datalist[$key]['default'];
                    }
                    
                }
                */
            }

        } catch (Exception $e)
        {
            //System::Log_error($e->getMessage().' Can not create new db object ');
            //$stackTrace = debug_backtrace();
            //System::Log_error(var_export($stackTrace,true));
            return false;
        }

    }
    
    
    protected static function readDataFromArrayToDatalist($input,&$datalist){
        assert(is_array($input),"This method must be called only for array");
        foreach($datalist as $key => $value){
            if (isset($input[$key]) && (! empty($input[$key]))) {
                if (Db_objects_list::isEnum($datalist[$key]['type'])){
                    assert(isset($datalist[$key]['sqldata'][$input[$key]]),"Invalid value for enum : ".$input[$key]);
                }
                    //var_dump($input);
                    //print "<hr>Enum ".$key."   --> ".$input[$key];
                    //var_dump($datalist[$key]['sqldata']);
                    //print "<br><b>".$datalist[$key]['sqldata'][$input[$key]]."</b><br>";
                //$datalist[$key]['value']=$datalist[$key]['sqldata'][$input[$key]];
                    //print "<br>".$datalist[$key]['value']."<br>";
                //}else{
                    $datalist[$key]['value']=$input[$key];
                //}
            }
            else{
               //print "Set default $key <br>\n";
               $datalist[$key]['value']=$datalist[$key]['default'];
            }
        }
    }


    public function id(){
        
        if (! isset($this->datalist['id']['value'])) return False;
        $v = $this->datalist['id']['value'];
        if (self::ekran($this->datalist['id']['type']))
        {
            $v =  strval($v);
            if (($v == '') || ($v =='0')) return False;
        }
        else {
            $v = intval($v);
            if ($v == 0)  return False;
        };
        //if ((isset($this->datalist['id']['value'])) &&  ($this->datalist['id']['value'] > 0))  return $this->datalist['id']['value'];
        //if ((isset($this->datalist['id']['value'])) &&  (strval($this->datalist['id']['value']) !=  '') &&  (strval($this->datalist['id']['value']) !=  '0') )  return $this->datalist['id']['value'];
        
        return $v;
         
    }
    
    public function name(){
        
        if ((isset($this->datalist['name']['value'])) &&  (! empty($this->datalist['name']['value'])))  return $this->datalist['name']['value'];
        return false;
         
    }
    
    public function set_name($name){
        
        $this->datalist['name']['value'] = $name;
    }
    
    public function set_field_value($field,$val)
    {
        if (isset($this->datalist[$field])) {
            $this->datalist[$field]['value']=$val;
            return true;
        }
        else {
            return false;
        }
        
    }
    
    public function getNewId()
    {
        //if ($table == False) 
        $table = $this->table_name;
        $res = System::run_query("SELECT id FROM ".$table." ORDER BY id ASC LIMIT 1");
        if ($res === False) return False;
        if (count($res) == 0) return 1;
        $new_id = intval($res[0]['id']) + 1;
        while (count(System::run_query("SELECT id FROM ".$table." WHERE id = '".$new_id."'")) > 0 )
        {
            $new_id +=1;
        };
        return $new_id;
    }
    
    
    protected static function getKeyValuePairListForSql($keys,$data){
        assert(is_array($keys));
        $v=$k=$pair=array();
        foreach($keys as $key){
            if (isset($data[$key])){
                $value = $data[$key];
            }
            else{
                //var_dump($this->datalist);
                throw new Exception("Bad key found:  ".$key);
            }
            if (isset($value['value'])){
                if (Db_object::ekran($value['type'])) {
                    $value['value'] =System::escape_string($value['value']);
                }
                else{
                    if ($value['value'] ==''){
                        continue;
                    }
                }
                $v[]=$value['value'];
                $k[]=self::ekranSqlField($key);
                $pair[] = $key." = ".$value['value'];
            }
        } 
        return $pair;
    }
    
    private static function ekranSqlField($k)
    {
        return "`".$k."`";
    }
    
    public function Save()    {
        // Save object to db
        $ret=false;
        //!!!! removed 6/12/15
        //if (($this->name()) && (! empty($this->table_name))) 
        
        if (! empty($this->table_name))
        {
            // TO DO: replace this code with function getKeyValuePairListForSql
            $v=$k=$pair=array();
            foreach($this->datalist as $key=>$value) 
            {
                if (($key != 'id' ) && (isset($value['value'])))
                {
                    if (Db_object::ekran($value['type'])) {
                        //$value['value'] =" '".$value['value']."' ";
                        $value['value'] =System::escape_string($value['value']);
                    }
                    else
                    {
                        if ($value['value'] ==''){
                        //$value['value'] = $value['default'];
                            continue;
                        };
                    };
                    
                    if ($key == 'change_date'){
                        $value['value'] = "'".date ("Y-m-d")."'";
                    }
                    
                    $v[]=$value['value'];
                    $k[]=self::ekranSqlField($key);
                    $pair[] = self::ekranSqlField($key)." = ".$value['value'];
                }
            }    
            
           
            
            if (! $this->id()) 
            {
            // Create new object
                
                //var_dump($this->datalist['id']);
                //die("Create");
                $id = $this->getNewId();
                
                $v[]=$id;
                $k[]=self::ekranSqlField('id');
                $query="INSERT INTO ".$this->table_name."( ".System::create_commas_list($k)." ) VALUES ( ".System::create_commas_list($v)." )";
                //error_log($query);
                //print $query;
                if (System::run_query($query) !== false){
                    $this->datalist['id']['value']= $id;
                    $ret = $id;
                }
                
                //=mysql_insert_id();
                
            }
            else
            {
                $query="UPDATE ".$this->table_name." SET ".System::create_commas_list($pair)." WHERE id = ".System::escape_string($this->id());
                //print $query."<hr>";
                if ( System::run_query($query)!==false) {
                    $ret=$this->datalist['id']['value'];
                }
            };
            
        }
        if ($ret !== False) System::$cache->flush($this->table_name);
        //print $query;
        return $ret;
    }
    
    public function Delete()    {
        if ( $this->id() > 0){
            $res = System::run_query("DELETE FROM `".$this->table_name."` WHERE id = ".$this->id());
            if ($res !== false ){
                //unset($this);
                return true;
            }    
            return false;
        }
        return true; // Object has not id and live only in memory
    }
    
    
    protected static function ekran($str)
    {
        if (Db_objects_list::isEnum($str)) {
            return true;
        }
        $st_types=array('varchar','text','date','decimal','timestamp');
        foreach ($st_types as $value)
        {
            if (! (strpos(strtolower($str),$value) === false)) return true;
        }
        
        return false;
        
    }
    
    public function Show($styles = '')
    {
       foreach ($this->datalist as $value)
       {
           if (  (! (isset($value['title']))) || ($value['title'] == '')) continue;
           print $value['title']." ";
           if (isset($value['value']) ) {print $value['value'];}
           else {print "unset";};
           print "<br>\n";
           
       }
        
    }
    
    public function  ShowInfo($interest = null)
    {
        foreach($this->datalist as $key=>$data)
        {
            if ($interest != null && ( ! in_array($key,$interest))) continue;
            $name = (trim($data['title']) == '') ? $key : $data['title'];
            print "<b>".$name."</b> ".$data['value']."<br>\n";
        };
    }
    
        
    public function Show_head($styles = '',$body = 100)
    {
        
       print $this->datalist['name']['value']."<br>"; 
        
    }
    
    
    private function get_control_type($field)
    {
        $num_type = array('int','bigint');
        
        if (! isset($field['htm_type']))
        {
            if ($field['title']=='id' && $this->id() == false){
                return IGNORE;
            }
            if (($field['title']=='id')){
                return HIDDEN;
            }
            if (empty($field['title']) )
            {
              if  ( $this->id()) {
                  return IGNORE;
              }
              else{
                  return HIDDEN; // If object new, all fields must be saved
              }
            }
            // Remove bracket
            $type = $field['type'];
            $bracket = strpos($type,'(');
            if ($bracket !== false) {
                $type = strtolower(trim(substr($field['type'],0,$bracket)));
            }
            if ($type=='date'){
                return DATE;
            }
            if (in_array($type,$num_type) ){
                return INT;
            }
            $number_type = array('decimal','float','double','real');
            if (in_array($type,$number_type) ){
                return NUMBER;
            }
            if ( ! (strpos($type,'text') === false)) {
                return TEXTAREA;
            }
            if ($type == 'enum'){
                return SELECT;
            }
            return TEXT;
        }
        else {
            //print "H: ".var_export($field,true)."<br>";
            return  $field['htm_type'];
        }
    
    }
    
    protected function init_add_date()
    {
        // For init add data 
        return true;
    }
    
    protected function getUnit($field){
        return '';
    }
    
    protected function createControl($key,$input){
        $value = $this->datalist[$key];
        $type = $this->get_control_type($value);
        if ($type == IGNORE) {
            return false;
        }
        $inp='';
        $val='';
        if (isset($value['value'])) {
            $val = $value['value'];
        }elseif (isset($value['default'])){
            $val = $value['default'];
        }
        
        if ( ($input !=false) && (isset($input[$key]))){
            $inp=$input[$key];
        }
        if ($type == SELECT ){
            if ($input == false) {
                $inp=$val;
            }
            $val = $value['sqldata'];
        }
        //$output = '';
        if( $type != HIDDEN) {
            print "<tr><td>".$value['title']."</td><td> ";
        }
            html_form::create_control($type,$key,$value['title'],$val,$inp,'',50);
        if( $type != HIDDEN) {
            print $this->getUnit($key);
            print "</td></tr>\n";
        }
        
    }
    
    
    public function getAddFormBody($input = '')
    {
        $this->init_add_date();
        ob_start();
        print "<table>";
        html_form::create_control(HIDDEN,'class_name','',get_class($this));
        foreach ($this->datalist as $field=>$fieldData){
        //foreach ($this->datalist as $key=>$value){
            
            $this->createControl($field,$input);
            
            
            /*
            $type = $this->get_control_type($value);
            if ($type == IGNORE) {
                continue;
            }
            $inp='';
            $val='';
            if (isset($value['value'])) {
                $val = $value['value'];
            }elseif (isset($value['default'])){
                $val = $value['default'];
                //print $key." => ".$val."<hr>";
            }

            if ( ($input !=false) && (isset($input[$key]))) $inp=$input[$key];
            if ($type == SELECT) 
            {
                if ($input == false) $inp=$val;
                //if ($value['sqldata'] == ''){
                //    $value['sqldata'] =  self::getEnumValues( $value['table'], $key );
                //}
                $val = $value['sqldata'];
            };
            
            if( $type != HIDDEN) {
                print "<tr><td>".$value['title']."</td><td> ";
            }
            html_form::create_control($type,$key,$value['title'],$val,$inp,'',50);
            if( $type != HIDDEN) {
                print "</td></tr>\n";
            }
             */
            
        }
        print "</table>";
        print html_form::create_control(HIDDEN,'action','',ACT_UPDATE,'');
        return ob_get_clean();

    }
    
    public function show_add_form($input = false,$back_url = ''){
    
        $form = new html_form($this->datalist['name']['title'].$this->id(),'POST',$back_url);
        $form->start();

        print $this->getAddFormBody($input);

        $form->create_control(SUBMIT,'submit',"Применить",'');
        print " ";
        $form->create_control(DELETE,'delete',"Удалить",'');
        $form->finish(false); 
    }   
      
    public function get_field_value($name){
        if (isset($this->datalist[$name]['value'])) return $this->datalist[$name]['value'];
        return false;
    }
    
    
    public function getFieldTitle($name){
        if (isset($this->datalist[$name]['title'])) return $this->datalist[$name]['title'];
        return false;
    }

    static function sortByTitle($a,$b){
        $al = isset($a['title']) ? mb_strtolower($a['title']) : '';
        $bl = isset($b['title']) ? mb_strtolower($b['title']) : '';
        if ($al == $bl) {
            return 0;
        }
        return ($al > $bl) ? +1 : -1;
    }
    
    protected function sortFields($sortMethod = "sortByTitle"){
        // Sort fields by it's title
        uasort($this->datalist, array($this, $sortMethod));
    }
    
    public function getFields(){
        return array_keys($this->datalist);
    }
    
}

class Db_objects_list
{
    protected $object_list = array ();
    protected $table_info = array(); 
    protected $table_name = '';
    protected $name = false;
    protected $items_count=0;
    protected $items_per_line=3;
    protected $obj_type = 'db_object';
    protected $filter = null;
    protected $show_function = 'Show_head';
    protected $sort_order = null;
    //private $limit = 'Show_head';
    
    
    public function __construct($table,$where = '',$Dtype = 'Db_object',$filter = false)
    {
        $this->table_info=Db_objects_list::get_table_info($table);
        $this->table_name = $table;
        $this->obj_type = $Dtype;
        //die($filter);
        $this->setFilter($filter);
        $where = $this->addWhereFromFilter($where);
           
        $res=System::run_query("SELECT * FROM `".$table.'` '.$where);
        //print "SELECT * FROM `".$table.'` '.$where;
        //System::Log_message("SELECT * FROM `".$table.'` '.$where);

        if ((! $res) || (count($res) == 0))  { 
            //System::Log_error(' Data list is empty . '); 
            
            } 
        else
        {   
            $this->items_count=0;
            //while ($line = mysql_fetch_array($res, MYSQL_ASSOC))
            foreach($res as $line)
            {
                //print $this->items_count." : ".$line['name']."<br>";
                $this->object_list[$this->items_count]= new $this->obj_type($line,$this->table_info,$table);
                $this->items_count++;
            }   
            
        }
    }
    
    protected function addWhereFromFilter($where){
        $where = $this->addConditionFromFilter($where);
        if (! empty ($where)){
            $where = ' WHERE '.$where;
        }
        return $where;
        
    }
    
    private function addConditionFromFilter($where){
        if (! $this->filter){
            return $where;
        }
        $filterWhere = $this->filter->get_where();
        if (empty($filterWhere)){
            return $where;
        }
        $out = $where;
        if (! empty($where)){
            $out .= ' AND ';
        }
        $out .= $filterWhere;
        return $out;
    }
    
    protected function setFilter($filter)
    {
        if ($filter === true){
            // get default filter for class
            $this->filter = call_user_func($this->obj_type.'::filter');
        }elseif(is_object($filter) && get_class($filter) == 'db_filter'){
            // filter is object
            $this->filter = $filter;
        }
    }
    
    public function SetSortOrder($str)
    {
        if (isset($this->table_info[$str]) )
        {
            $this->sort_order = $str;
            return true;
        }
        return false;
    }
    
    protected function Compare($object1,$object2)
    {
        //if ($this->table_info[$str] == 'DATE') 
        //{
        $res = strtotime($object2->get_field_value($this->sort_order)) - strtotime($object1->get_field_value($this->sort_order)) ;
        //print $res."<br>";
        if ($res > 0) return 1;
        if ($res < 0) return -1;
        return 0;
        
        //}
            //return
    }
    
    
    public function Sort()
    {
        
        assert($this->sort_order != null);
        usort($this->object_list,array("Db_objects_list","Compare"));
        
    }
    
    
    public function filter()
    {
        $this->filter->show();
    }
    
    
    
    public function set_show_method($method)
    {
        $this->show_function = $method;
    }
    
    public static function getTableName($table)
    {
        $db = System::get_config_var('my_sql_db_name');
        $query = "SELECT * FROM `TABLES` WHERE `TABLE_NAME` = '".$table."' AND `TABLE_SCHEMA` = '".$db."'";
        $res=System::run_is_query($query);
        //if (mysql_num_rows($res) != 1) return false;
        if (count($res) != 1) return false;
        //$line = mysql_fetch_array($res, MYSQL_ASSOC);
        return $res['TABLE_COMMENT'];
    }
    
    public function name($refresh = false )
    {
        if ($this->name === false || $refresh)
        {
            $this->name =  $this->getTableName($this->table_name);
        };
        return $this->name;
    }
    
    static  function getEnumValues( $table, $field )
    {
        $result = \System::run_query( "SHOW COLUMNS FROM $table WHERE Field = '$field'" );
        $textToParse = $result[0]['Type'];
        //die();
        preg_match("/^enum\(\'(.*)\'\)$/i", $textToParse, $matches);
        //var_dump($matches);
        $enum = explode("','", $matches[1]);
        $output = array();
        foreach($enum as $val){
            $output[$val] = $val; // to select control 
        }
        return $output;
    }
    
    public static function isEnum($fieldType){
        if (strpos(strtolower($fieldType), 'enum') === false){
            return false;
        }
        return true;
    }
    
    public static  function get_table_info($table)
    {
        //global $config;
        //$config['my_sql_db_name']
        $db = System::get_config_var('my_sql_db_name');
        $table_info=array();
        $query = "SELECT * FROM `COLUMNS` WHERE `TABLE_NAME` = '".$table."' AND `TABLE_SCHEMA` = '".$db."'";
        
        $res=System::run_is_query($query);
        
        if (count($res) == 0) return false;
        #while ($line = mysql_fetch_array($res, MYSQL_ASSOC))
        foreach($res as $line)
        {
            $table_info[$line['COLUMN_NAME']]['title']=$line['COLUMN_COMMENT'];
            $table_info[$line['COLUMN_NAME']]['type']=strtolower($line['COLUMN_TYPE']);
            $table_info[$line['COLUMN_NAME']]['default']=$line['COLUMN_DEFAULT'];
            // for complex object
            $table_info[$line['COLUMN_NAME']]['table']=$table;
            // for enums
            if (self::isEnum($table_info[$line['COLUMN_NAME']]['type'])){
                $table_info[$line['COLUMN_NAME']]['sqldata'] = self::getEnumValues( $table, $line['COLUMN_NAME'] );
            }
        };
        
        
        
        return $table_info;
    }
    
    
    
    
    public function add(&$obj)
    {
        $this->object_list[$this->items_count]= $obj;
        $this->items_count++;
    }
    
    public function set_items_per_line($n)
    {
        
        if ($n<1) {$this->items_per_line = 1;}
        else { $this->items_per_line = $n;};
        
    }
    
    
    public function showAsTreeNode($start = 0, $max = 0)
    {
        
        print  '<li id="'.get_class($this).'" >';
        print '<a href="#">'.$this->name().'</a>';
        print '<ul>';
        $finish = ($max == 0) ?  count($this->object_list) : $max;
        for ($i=$start;$i<$finish;$i++)
            {
                $tmp=$this->object_list[$i];
                
                
                
                print  "<li id=\"".$tmp->id()."\" php_class=\"".get_class($tmp)."\"  php_id=\"".$tmp->id()."\"  php_def_action=\"edit_simple\">";
                $name = $tmp->name();
                if (empty($name)) $name = get_class($tmp).$tmp->id();
                print '<a href="#">'.$name.'</a>';
                //call_user_func(array($tmp,$this->show_function));
                print  "</li>";
            }
        print '</ul>';
        print "</li>";
    }
    
   
    public function Show($start = 0, $max = 10,$page_control = false)
    {
        
        if ($this->items_count < $max) $max=$this->items_count; 
        
        if ($page_control == true)
        {
            $post_method=FALSE;
            if (! empty($_POST)) { $post_method=TRUE;};
            $rs=count($this->object_list);
            $curr_page=System::Safe_GET_int('_page');
            if ((($curr_page=='') || ($curr_page==false)) && $post_method )  {$curr_page=$_POST['_page'];};
            if (($curr_page=='') || ($curr_page<1) || $curr_page==false )  { $curr_page=1;};
            $finish=$curr_page*$max;
            if ($finish>$rs) { $finish=$rs; };
            $start=$finish-$max;	
	
	 // Generating a form with query data

 		if ($post_method)
 		{
                    $inp_arr=$_POST;
                    $method='POST';
 		}
                else
 		{ 	
                    $inp_arr=$_GET;
                    $method='GET';
 		};
 		$form = new html_form('inputdata','GET',$_SERVER['REQUEST_URI']);
                $form->start();
                //print "\n <form method=get name='inputdata' id='inputdata' action='".$_SERVER['REQUEST_URI']."'>\n";
                
                foreach($inp_arr as $key=>$value)
                {
                    if ($key != '_page') { print "<input type=hidden name=\"".$key."\" value=\"".$value."\">\n"; };
                };
                print "<input type=hidden name=\"_page\" value=\"".$curr_page."\">\n";
                $form->finish(false);
                // Filter must be here
                //$tmp->Show_filter();
                //print "</form>\n";
                

        }
        else
        {
            // No page control
            $finish=$max;
            $start=0;
        };
        $iol=0;    
        if ($start<$finish) 
        {
            print "<table width=\"100%\"><tr>";
            for ($i=$start;$i<$finish;$i++)
            {
                print "<td>";
                $tmp=$this->object_list[$i];
                call_user_func(array($tmp,$this->show_function));
                //$tmp->Show_head();
                print "</td>";
                $iol++;
                if ($iol >= $this->items_per_line) 
                {
                    print "</tr>\n<tr>";
                    $iol=0;
                };
            };
            print "</tr></table>"; 
        }
        
        if (($page_control) && ($rs>$max))
	{
            $pages=ceil($rs/$max);
            print "<br>";
            print "<div> стр ";

	    $tmp=$curr_page-1;
            
            if ($curr_page>1) { 
                //print "<a href=\"javascript:void(0)\"   OnClick=\"javascript:document.getElementById('inputdata')._page.value=".$tmp."; document.getElementById('inputdata').submit(); \">перед.</a> \n";
                $form->page_link($tmp,'перед.');
                
                };
            for ($i=1;$i<=$pages;$i++)
            {
                //print "<a href=\"javascript:void(0)\" OnClick=\"javascript:document.getElementById('inputdata')._page.value=".$i."; inputdata.submit(); \"> ";
                
                if ($curr_page == $i) { $pn= "<b>".$i."</b>";}
                else { $pn = $i; };
                $form->page_link($i,$pn);
            };

            $tmp=$curr_page+1;
            if ($curr_page<$pages) { 
                //print "<a href=href=\"javascript:void(0)\" OnClick=\"javascript:document.getElementById('inputdata')._page.value=".$tmp."; inputdata.submit(); \">след.</a> \n";
                $form->page_link($tmp,'след.');
                
            };
            print "<div>";
            
        };    
        
    }
    
    
    public function Search_by($needle,$par = 'name')
    {
        foreach ($this->object_list as $value)
        {
            if ($value->get_field_value($par) == $needle) return $value;
        };
        return false;
    }
    
    public function GetByNum($i)
    {
        if (isset($this->object_list[$i]))
        {
            return $this->object_list[$i];
        };
        return false;
    }
    
    public function getObjects()
    {
        return @$this->object_list;
    }
    
    
    public function Show_title($start=0, $max = 10,$style='',$body=100)
    {
        if ($this->items_count < $max) $max=$this->items_count; 
        
        for ($i=$start;$i<$max;$i++)
        {
            $tmp=$this->object_list[$i];
            //print "<hr>";
            $tmp->Show_title($style,$body);
            print "<br>";
            
        };
    }
    
    public function Show_right($start=0, $max = 10)
    {
        if ($this->items_count < $max) $max=$this->items_count; 
        
        for ($i=$start;$i<$max;$i++)
        {
            $tmp=$this->object_list[$i];
            print "<div style=\"clear: both\">"; 
            $tmp->Show_right();
            print "</div>";
            
        };
    }
    
    public function Count()
    {
        return $this->items_count;
    }
   
}


   
    class db_filter{
        private $controls = array();
        private $var_change = '_x_';
        public $name = "filter";
        
    function __construct($data = '')
        {
            if (is_array($data) ) $this->controls=$data;
            
        }
        
        public function add_control($name,$sname,$type,$data,$sql_data = '', $ignore = 0,$sql = '')
        {
            if ((empty ($name)) && (! isset ($this->controls[$name]))) return false;
            $this->controls[$name]['data'] = $data;
            if (is_array($data))
            {
                $this->controls[$name]['data'] = $data;
                $this->controls[$name]['type'] = SELECT;
                $this->controls[$name]['sqldata'] = $data;
            };
            if (! empty ($type)) $this->controls[$name]['type'] = $type;
            if (! empty ($sql_data)) $this->controls[$name]['sqldata'] = $sql_data;
            $this->controls[$name]['ignore'] = $ignore;
            $this->controls[$name]['sql'] = $sql;
            $this->controls[$name]['name'] = $sname;
        }
        
        public function show($input = '')
        {
            
            if ($input == '') 
            {
                if (! empty($_POST)) $input = $_POST;
                if (! empty($_GET)) $input = $_GET;
            };
            
            $saved=array();
            $form = new html_form($this->name);
            $form->start();
            
            foreach ($this->controls as $key=>$value)
            {
                $inp=System::Safe_input_int($key);
                if ($inp == false)  $inp = '';
                print $value['name'];
                html_form::create_control($value['type'],$key,$value['name'],$value['data'],$inp,'',25);
                $saved[]=$key;
            }
            
            // Save additional input data
            if (! empty($input))
            {
                foreach($input as $key=>$value)
                {
                    if (! in_array($key,$saved)) { html_form::create_control(HIDDEN,$key,'',$value); };
                };
            };
            //$form->create_control(SUBMIT,'submit',"",'');
            $form->finish();
            
            
        }
        
        
        public function get_where($input = '')
        {
            $sql=false;
            if ($input == '') 
            {
                if (! empty($_GET)) $input = $_GET;
                if (! empty($_POST)) $input = $_POST;
            }
            
            foreach ($this->controls as $key=>$value)
            {
                
                $inp=System::Safe_GET_int($key);
                
                if ($value['type'] == SELECT){
                    if ((! $inp) || ($inp == $value['ignore'])){
                        continue;
                    }
                    $sqli=str_replace($this->var_change,$value['sqldata'][$inp],$value['sql']);
                    if (! $sql ) { $sql = $sqli; }
                    else { $sql .= ' AND '.$sqli; };
                }
                
                if ($value['type'] == TEXT){
                    $inp=System::Safe_GET_str($key);
                    if ((empty($inp)) || ($inp == strval($value['ignore']))) {
                        continue;
                    }
                    $sqli=str_replace($this->var_change,$inp,$value['sql']);
                    if (! $sql ) {
                        $sql = $sqli;
                    }
                    else { 
                        $sql .= ' AND '.$sqli; 
                    }
                }
                
                if ($value['type'] == CHECKBOX && $inp){
                    //$inp=System::Safe_GET_str($key);
                    //if (! $inp) continue;
                    
                    $sqli .= $key." =  1";
                    if (! $sql ) { $sql = $sqli; }
                    else { $sql .= ' AND '.$sqli; };
                    
                }
            }
            return $sql;
        }
        
        public function get_control_data($name)
        {
            $inp=System::Safe_GET_int($name);
            if ($this->controls[$name]['type'] = SELECT)  return $this->controls[$name]['sqldata'][$inp];
        }
    }
    
    
    class View_Db_objects_list extends Db_objects_list {
        private $all_count = 0;
        private $start_page = 0;
        private $items_per_page = 10;
        
        public function __construct($table,$Dtype, $where = '',$filter = true,$start = 0,$item_pp = 9, $items_pl = 3)
        {
            $this->table_info=Db_objects_list::get_table_info($table);
            $this->table_name = $table;
            $this->obj_type = $Dtype;
            
            if (isset($_GET['_page'])) {$this->start_page = System::Safe_get_int ('_page');}
            elseif (isset($start)) { $this->start_page=$start; };
            if (isset($item_pp)) $this->items_per_page=$item_pp;
            if (isset($items_pl)) $this->set_items_per_line($items_pl);

            $this->setFilter($filter);
            $where = $this->addWhereFromFilter($where);
            /*
            if ($filter)
            {
                $this->filter = call_user_func($Dtype.'::filter');
                $f_where=$this->filter->get_where();
                if ($where !='') { 
                    if ($f_where) $where.= ' AND '.$f_where;}    
                else { $where .= $f_where; };
            };  
            
            
         

            if (! empty ($where)) $where = ' WHERE '.$where;
             * */
          
            $query = "SELECT * FROM `".$table.'` '.$where;
            $res=System::run_query($query);
            //print $query;
            //$this->all_count=mysql_num_rows($res);
            $this->all_count=count($res);
            //System::Log_message("SELECT * FROM `".$table.'` '.$where);

            if ((! $res) || ($this->all_count == 0))  { System::Log_error(' Data list is empty . '); } 
            else
            {   
                $this->items_count=0;
                $l1=$this->start_page*$this->items_per_page;
                $l2=$this->items_per_page;
                $res=System::run_query("SELECT * FROM `".$table.'` '.$where." LIMIT ".$l1.' , '.$l2);
                //System::Log_message("SELECT * FROM `".$table.'` '.$where." LIMIT ".$l1.' , '.$l2);
                //while ($line = mysql_fetch_array($res, MYSQL_ASSOC))
                foreach($res as $line)
                {
                    $this->object_list[$this->items_count]= new $this->obj_type($line,$this->table_info,$table);
                    $this->items_count++;
                };   

            };
        }
        
        
        private function page_control()
        {
            $post_method=FALSE;
            if (! empty($_POST)) { $post_method=TRUE;};
           
            $curr_page=$this->start_page;
            
            	
	
	 // Generating a form with query data

            if ($post_method)
            {
                $inp_arr=$_POST;
                $method='POST';
            }
            else
            { 	
                $inp_arr=$_GET;
                $method='GET';
            };
            $form = new html_form('inputdata',$method,$_SERVER['REQUEST_URI']);
            $form->start();

            foreach($inp_arr as $key=>$value)
            {
                if ($key != '_page') { print "<input type=hidden name=\"".$key."\" value=\"".$value."\">\n"; };
            };
            print "<input type=hidden name=\"_page\" value=\"".$curr_page."\">\n";
            
        
            $pages=ceil($this->all_count/$this->items_per_page);
            print "<br>";
            print "<div> стр ";
            //$curr_page=$this->start_page;
	    $tmp=$curr_page-1;
            
            //if ($curr_page>1)  $form->page_link($tmp,'перед.');
            if ($curr_page>0)  $form->page_link($tmp,'перед.');
            //for ($i=1;$i<=$pages;$i++)
            for ($i=0;$i<$pages;$i++)
            {
                if ($curr_page == $i) { $pn= "<b>".strval($i+1)."</b>";}
                else { $pn = strval($i+1); };
                $form->page_link($i,$pn);
            };

            $tmp=$curr_page+1;
            if ($curr_page<$pages) { 
                $form->page_link($tmp,'след.');
            };
            print "</div>";    
            $form->finish(false);
        }


        public function Show($start = 0, $max = 10,$page_control = false)
        {

            /*
            if ($this->items_count < $max) 
            $max=$this->items_count; 
            $finish=$curr_page*$max;
            if ($finish>$rs) { $finish=$this->all_count; };
            $start=$finish-$max;
            */
        
            $iol=0;    
            /*
            print "<table width=\"100%\"><tr>";
            foreach ($this->object_list as $value)
            {
                print "<td>";
                call_user_func(array($value,$this->show_function));
                print "</td>";
                $iol++;
                if ($iol >= $this->items_per_line) 
                {
                    print "</tr>\n<tr>";
                    $iol=0;
                };
            };
            print "</tr></table>"; 
            */
            
            $minW = round(100/$this->items_per_line)-2;
            $maxW = round(1.7*(100/$this->items_per_line));
            if ($maxW > 100) {
                $maxW = 100;
            }
            foreach ($this->object_list as $value)
            {
                print "<div style='display:inline-block; margin:0 auto;  width:auto; min-width: $minW%; max-width:$maxW%; text-align: center'>";
                call_user_func(array($value,$this->show_function));
                print "</div>\n";
                $iol++;
                if ($iol >= $this->items_per_line) 
                {
                   // print "<br>\n";
                    $iol=0;
                }
            }
            
            
            if ($this->all_count > $this->items_count) $this->page_control();
        }
    
        
    }


?>
