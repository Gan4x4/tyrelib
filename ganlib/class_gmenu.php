<?php
include_once($paths['script'].'class_decorate.php');
class Gmenu
{
    private $data = array();
    private $style = 0;
    private $items_count = 0;
    private $color = 'lgreen' ;
    private $h_color = 'yellow';
    private $disabled_color = 'ggreen';
    private $text_color = 'green' ;
    private $text_h_color = 'yellow';
    private $text_disabled_color = 'gray';
    private $selected = -1;
    private $text_align = 'left';
    private $link_n = 0;
    private $name_n = 1;
    private $title_n = 2;
    private $id_n = 3;
    
    
    

    public function __construct($stl = 0,$menu_data = '') {
    	$this->data = array();
    	$this->style = 0;
    	$this->items_count = 0;
    	if (is_array($menu_data))
    	{
        	foreach	($menu_data as $value)
		{
			$this->data[$this->items_count] = $value;
			$this->items_count++;
		};
    	};
        
        if (is_file($menu_data)) $this-> load_from_file($menu_data);
        
   }
   
   public function load_from_file($fn)
   {
       
        $fp = fopen($fn, "r");
        
        if (!$fp) 
        {
             System::Log_error($e->getMessage() . ' Can not load menu file ');
             return false;  
        };    
        
       
        $menu=array();
        while ($data = fgets($fp, 1000)) 
        {
            
            $data = explode(';', $data);
            if ((!empty($data[$this->link_n])) && (!empty($data[$this->name_n])))
            {
                $enb=false;
                if (empty($data[$this->title_n])) System::Log_message(" У пункта меню  ".$data[$this->name_n]." нет title ");
                if (( is_dir($_SERVER['DOCUMENT_ROOT']."/".$data[$this->link_n]) ) || (is_file($_SERVER['DOCUMENT_ROOT']."/".$data[$this->link_n]))) $enb=true;
                $this->add_item($data[$this->name_n],$data[$this->link_n],$data[$this->title_n],$enb);  
            };
        };
        fclose($fp);
   }
   

    public function set_bg_colors($hcolor = '',$color = '',$d_color = '')    {

	   if ( $hcolor  != '' ) $this->h_color = $hcolor;
  	   if ( $color  != '' ) $this->color = $color;
  	   if ( $d_color  != '' ) $this->$disabled_color = $d_color;
    }

    public function set_text_colors($hcolor = '',$color = '',$d_color = '')    {

	   if ( $hcolor  != '' ) $this->h_color = $hcolor;
  	   if ( $color  != '' ) $this->color = $color;
  	   if ( $d_color  != '' ) $this->$disabled_color = $d_color;
    }


    public function set_align($a)    {
		$this->text_align = $a;
    }

    public function set_selected($a)    {
		$this->selected = $a;
    }



    public function add_item($name,$link,$title,$enabled = true) {
        
        $i = $this->items_count;
	$this->data[$i]['name'] =  $name;
	$this->data[$i]['link'] =  $link;
	$this->data[$i]['title'] =  $title;
	$this->data[$i]['enabled'] =  $enabled;
	$this->items_count++;
    }


    public function show($selected = -1) {

        if ($this->items_count == 0)
         {
                 return false;
         }

	print "<table width=\"100%\" ><tr>";
        for($i=0;$i<$this->items_count;$i++)
	{
        	$key=$i;
                $value=$this->data[$i];
		if ($key==$selected)
                {
                        $col = $this->h_color;
                        $text_col = $this->text_h_color;
                }
                else
                {
                        $col = $this->color;
                        $text_col = $this->text_color;
                };
                if (! $value['enabled'] )
                {
                         $col = $this->disabled_color;
                         $text_col = $this->text_disabled_color;
                };
                print "<td align=\"".$this->text_align."\" style=\" color: $text_col\">";
                //Decorate::color_block_start($col,30,'','',false);
                if (($value['link'] !='') && ($value['enabled']))
                {
                    print "<a href=\"".$value['link']."\"  title=\"".$value['title']."\" class=\"menutext\">".$value['name']."</a>";
                }
                else
                {
                    print $value['name'];
                };
                print "<br><br>";
                //Decorate::color_block_end();
                print "</td>";
                };
		print "</table>";
    }


    
    private function get_selected()
    {
        if ( strlen($_SERVER['PHP_SELF'])<2 ) return 0;
        for($i=0;$i<$this->items_count;$i++)
        {
            //$value=$this->data[$i];
            
            $ln=strlen($this->data[$i]['link']);
            $srv=substr($_SERVER['PHP_SELF'],0,$ln);
            $lnk=$this->data[$i]['link'];
            if ( $srv == $lnk) return $i;
            
        }
        return -1;
    }
    
    public function show_line($selected  = -1) {

    if ($selected <0) $selected = $this->get_selected();
    if ($this->items_count == 0)
     {
             return false;
     }

    for($i=0;$i<$this->items_count;$i++)
    {
            $key=$i;
            $value=$this->data[$i];
            if ($key==$selected)
            {
                    $text_col = $this->text_h_color;
                    $style ='menutextH';
            }
            else
            {
                    $text_col = $this->text_color;
                    $style ='menutext';
            };
            if (! $value['enabled'] )
            {

                     $text_col = $this->text_disabled_color;
                     $style ='menutextD';
            };
             if (($value['link'] !='') && ($value['enabled']))
            {
                    print "<a href=\"".$value['link']."\"  title=\"".$value['title']."\" class=\"".$style."\" >".$value['name']."</a>";
            }
            else
            {
                print "<span class=\"".$style."\" >";    
                print $value['name'];
                print "</span>";
            };
            if ($i<($this->items_count-1)) print " :: ";

        };

    }
    
    public function getItems(){
        return $this->data;
    }

   }

?>