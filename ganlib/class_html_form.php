<?php
define('IGNORE',-1);
define('TEXT',1);
define('SELECT',2);
define('TEXTAREA',3);
define('SUBMIT',4);
define('HIDDEN',5);
define('DATE',6);
define('PIC_SELECT',7);
define('INT',8);
define('NUMBER',14);
define('DELETE',9);
define('BUTTON',10);
define('RADIO',11);
define('SEARCH',12);
define('TIMER',13);
define('CAR_SELECT',14);
define('RESET',15);
define('CHECKBOX',16);


class html_object
{
    
};


class html_form{
    
    private $name = "form1";
    private $method = "GET";
    private $action = "";
    private $id = "";
    private $controls_list=array();
    private $def_text_size = 70;
    static $pic_no_image ='';
    static private $new_window_name = 'Bar_add_win';
    public function __construct($name = 'form1',$method='GET',$action='')
    {
        global $names;
        self::$pic_no_image=system::get_config_var('names','pic_no_image'); //$this->pic_no_image=$names['pic_no_image'];
        if ($action == '') $this->action = basename($_SERVER['PHP_SELF']);
        if (strtolower($method) == 'post') $this->method = 'POST';
        if (!empty ($name)) $this->name=$name;
        $this->id=$name;
        
        
    }
    
    
    public static function create_control($type,$f_name,$name,$data,$index = 0,$style = '',$text_size = 75)
    {
            global $paths;
        
        switch ($type) {
            case SELECT:
                print "<select name=\"".$f_name."\">";
                //var_dump($index);
                print  html_form::select_box_body($data,$index);
                print "</select>";

                break;

            case TEXTAREA:
                
                
                print "<textarea name=\"".htmlspecialchars($f_name)."\" id=\"".htmlspecialchars($f_name)."\" cols =80 rows = 15>";
                print stripslashes(htmlspecialchars($data));
                print "</textarea>";

                break;

            case SUBMIT:
                
                //print "<input type=submit name=\"".$f_name."\" value=\"".$name."\">";
                print "<input  type=submit value=\"".$name."\">";
                break;
            
            case RESET:
                    print "<input  type='reset' value='$name' >";
                break;

            
            case BUTTON:
                
                //print "<input type=submit name=\"".$f_name."\" value=\"".$name."\">";
                print "<input  type=button name=\"".$f_name."\" value=\"".$name."\">";
                

                break;
            
            case CHECKBOX:
                //print "<input type=submit name=\"".$f_name."\" value=\"".$name."\">";
                $checked = $index ? 'CHECKED' : ''; 
                print "<input type='checkbox' name=\"".$f_name."\" value=\"".(bool)($data)."\" $checked>";

                break;
            
             case HIDDEN:
                
                //print "<input type=submit name=\"".$f_name."\" value=\"".$name."\">";
                print "<input type=hidden name=\"".$f_name."\" value=\"".stripslashes(htmlspecialchars($data))."\">";
                

                break;
            
            
            
            case DATE:
                
                //print "<input type=submit name=\"".$f_name."\" value=\"".$name."\">";
                //print "<input type=hidden name=\"".$f_name."\" value=\"".$data."\">";
                if (! isset($data)) $data=date('Y-m-d');
                //else 
                require_once($paths['calendar'].'tc_calendar.php');  
                $myCalendar = new tc_calendar($f_name, true,false);
                $myCalendar->setIcon($paths['www_calendar']."images/iconCalendar.gif");
                //$myCalendar->setIcon("/external/calendar/images/iconCalendar.gif");
                //$myCalendar->setDate(date('d'), date('m'), date('Y'));
                $myCalendar->setDateYMD($data);
                $myCalendar->setPath($paths['www_calendar']);
                $myCalendar->zindex = 150; //default 1
                $myCalendar->setYearInterval(1901, 2099);
                $myCalendar->dateAllow('1901-03-01', '2099-01-01');
                //$myCalendar->setDatePair($f_name, $name, $date);
                //$myCalendar->autoSubmit(true, "calendar");

                //$myCalendar->setSpecificDate(array("2011-04-14", "2010-12-25"), 0, 'month');
                //$myCalendar->setSpecificDate(array("2011-04-01"), 0, 'year');
                $myCalendar->setAlignment('right', 'bottom'); //optional
                $myCalendar->writeScript();

                

                break;
                
                case PIC_SELECT:
                   
                   html_form::small_window_js();
                    print "<input type=HIDDEN name=\"".$f_name."\" id = \"".$f_name."\" value=\"".$data."\" >";
                    if ($data > 0 )
                    {
                        $pic = new db_picture($data);
                    };
                    if ((isset($pic)) && ($pic != null))
                    {
                        $img_src=$pic->www_thumb_file(SMALL);

                    }
                    else {
                        $img_src=self::$pic_no_image;
                    }
                    //print "<a href=\"/admin/image_select.php\" target=new >__<img src=\"...\" alt = \"pic\" id = \"pictc\" > </a>";
                    print "<a href=\"javascript:void(0);\"  onClick='open_window(\"/admin/image_select.php?pic_id=".$f_name."\",750,600)' ><img src=\"".$img_src."\" alt = \"Иллюстрация\" title = \" Кликните что бы добавить изображение.\" id = \"".$f_name."_pictc\" > </a>";
                

                break;

            
                case INT:
                    if (empty($data)) {$data = 0;}
                    else { $data = (int)$data; };
                    print "<input type=text name=\"".$f_name."\" value=\"".$data."\" size = 5>";

                break;
                
                case NUMBER:
                    if (empty($data)) {$data = 0;}
                    else { $data = (float)$data; };
                    print "<input type=text name=\"".$f_name."\" value=\"".$data."\" size = 5>";

                break;

                
                case DELETE:
//print "<input type=button name=\"".$f_name."\" value=\"".$name."\" onClick='if (confirm(\"Удалить ".$data." ?\")) { ".$this->name.".action.value=".ACT_DELETE."; submit();}; ' >";
                    print "<input type=button name=\"".$f_name."\" value=\"".$name."\" onClick='if (confirm(\"Удалить ".$data." ?\")) { action.value=".ACT_DELETE."; submit();}; ' >";
                break;


            case CAR_SELECT:
                self::getCarSelect($name,$data);
                
                break;
                
                
            
            
            default:
                //print $name;
                print "<input type=text name=\"".$f_name."\" value=\"".stripslashes(htmlspecialchars($data))."\" size = ".$text_size.">";
                break;
        }
        
        print "\n";
    }

    //public function calendar($date)
    
    
    
    
    public static function select_box_body($array,$sel_ind)
    {
        $x='';$k=0;
        $a_keys=array_keys($array);
        //print $sel_ind;
        //var_dump($array);
        for ($i=0;$i<count($array);$i++){
            $sel='';
            if ( strlen($a_keys[$i]) == 0)  {
                System::Log_error(' Bad key');
            }
            else{  			
                $k=$a_keys[$i];
            }
            if (($k==$sel_ind) && ( strlen((string)$sel_ind) > 0 )){
                $sel='SELECTED';
            }
            if (isset($array[$k])){
                $x=$x."<option value=\"$k\"  $sel>".$array[$k]."</option>\n";
            }
            $k=$i+1;
        }
        return $x;
    }
    
    public function start()
    {
        print "\n<form name =\"".$this->name."\" method=\"".$this->method."\" action =\"".$this->action."\" enctype=\"multipart/form-data\" >\n";
        
    }
    
    public function finish($submit=true)
    {
        if ($submit) $this->create_control(SUBMIT,'submit',"Применить",'');

        print "\n</form>\n";
    }
    
    public function page_link($page,$title,$onClick = ''){
        
//        print "<a href=\"javascript:void(0)\"   OnClick=\"javascript:document.getElementById('inputdata')._page.value=".$page."; document.getElementById('inputdata').submit(); \">".$title.".</a> \n";
		if ($onClick == '') $onClick=$this->name."._page.value=".$page."; ".$this->name.".submit()";
        print "<a href=\"javascript:void(0)\"   OnClick=\"".$onClick." \">".$title."</a> \n";
        
    }
    
    static private function small_window_js()
    {
         
         print '<script language="JavaScript">
                <!--
                
                function open_window(link,w,h) {
                
                
		var win = "left=300,top=180, width="+w+",height="+h+",menubar=no,location=no,resizable=no,scrollbars=no";
		newWin = window.open(link,\''.html_form::$new_window_name.'\',win);
                newWin.focus();
                    
		return newWin;
                 };
        -->
        </script>';
    }
    
    static public function save_request($inp = '')
    {
        if ($inp=='')
        {
            if ((isset($_POST)) &&(! empty($_POST)))  { $inp_arr = $_POST;}
            else {$inp_arr = $_GET;};
            
        }
        else {
            $inp_arr = &$inp;
			  };
        
        foreach($inp_arr as $key=>$value)
            {
                if ($key != '_page') { 
                    //print "<input type=hidden name=\"".$key."\" value=\"".$value."\">\n"; 
                    html_form::create_control(HIDDEN,$key,'',$value);
                    };
            };
    }
      
     
}


class html_control{
    
    public $type = TEXT;
    protected $form = null;
    protected $style = '';
    protected $java = '';
    protected $show_name='';
    protected $add='';
    protected $name='';
    protected $value ='';
    
    static $types=array(TEXT=>'text',RADIO=>'radio');
    
    public function __construct($type,$name,$show_name,$value ='',$form = null){
        $this->type=$type;
        $this->name=$name;
        $this->show_name=$show_name;
        $this->value = $value;
        
    }

    function Show($data ='',$add = '')
    {
       // if ($data=='') $data=$this->value;
        //print "<input type=".self::$types[$this->type]." name=\"".$this->name."\" value=\"".htmlspecialchars($data)."\" ".$add.">";
    }
    
    public function GetHtml()
    {
        ob_start();
        $this->Show();
        return ob_get_clean();
    }
    
    
    public function set_style($str)
    {
        $this->style = $str;
    }
    
    public function set_java($str)
    {
        $this->java = $str;
    }
    
    public function get_id()
    {
        if (! isset($this->id)) return $this->name;
        return $this->id;
        
    }

}





class TagHtmlControl extends html_control
{
    public $tag='';
    public $id='';
    
    public function __construct($name,$id = '')
    {
        parent::__construct(99,$name,$name);
        $this->tag = $name;
        $this->id = $id;
        
    }
    
    function Start($data = '')
    {
        
        print "<".$this->tag." id=\"".$this->id."\" $data >";
    }
    
    function finish()
    {
        print "</".$this->tag.">\n";
     
    }
    
}





class HtmlHref extends html_control{
    
    public $title;
    public function __construct($link,$show_name='',$title = '')
    {
        parent::__construct(0,'',$show_name,$link);
        $this->title = $title;
          
    }
    
    public function Show($data = '', $add = '')
    {
        $s = $this->show_name;
        if ($this->show_name=='') $s = $this->value;
        print  "<a href=\"".$this->value."\" title = \" ".$this->title." \">".$s."</a>"; 
    }
    
    
}


class  HtmlTimer extends html_control{

    static $number = 0;
    static $common_name = 'jsTimer';
    
    public $id = 0;
    public $interval = 1; // sec
    public $start_time;
    
    public function __construct($start = '',$interval = '',$id = '')
    {
        if ($id == '') $this->id = self::$number;
        if ($interval != '') $this->interval = $interval;
        if ($start != '') $this->start_time = $start;
        
        parent::__construct(TIMER,'timer'.$this->id,'');
        self::$number++;
        $this->start_time = $start;
    }
    
    public function SetInterval($int = -1)
    {
        if ($int > -1) $this->interval = $int;
         
        $pause = $this->interval*1000;
        print "\n<SCRIPT LANGUAGE=\"JavaScript\">\n";
        $d = Date('U',$this->start_time)*1000;
        print "$().ready(function() {show_time(".$d.",'".$this->GetId()."')});\n";
        print "var timerMulti = window.setInterval(\"show_time(".$d.",'".$this->GetId()."');\", ".$pause.");\n";
        
        //print "alert(timerMulti);";
        print "</SCRIPT>\n";
    }
    
    public function GetId()
    {
        return self::$common_name.$this->id;
    }


    public function Show($data = '', $add = '')
    {
        $text = '';
        $css  ='';
        //$text =Date('r',$this->start_time).' -- '. Date('G:i:s',$this->start_time)." -- ".Date('G',mktime()-$this->start_time);
        //$text =Date('G:i:s',$this->start_time - mktime());
        
        //$text = self::GetTimeDifference($this->start_time, mktime()); 
       /*
        if (($this->start_time - mktime()) < 0 ) 
        {
            $css = "style='color:red>'";
            $text .= ' просрочен';
        };
        
        */
        print "<span $css id=\"".$this->GetId()."\">".$text."</span>\n";
    }

    static function GetTimeDifference($cur_time, $time) 
    {

        $difference = abs($cur_time - $time);
        $diff_days = floor($difference / 86400); 
        $diff_hours = floor($difference / 3600);
        $diff_minutes = floor($difference / 60);
        $diff_seconds = floor($difference );

        $return = array();

        $return['days'] = $diff_days;

        // Если разница > суток - 1 час
        if ($diff_hours > 23) {
            $return['hours'] = $diff_hours - (24 * $diff_days);
        } else {
            $return['hours'] = $diff_hours;
        }

        // Если разница > часа - 1 минута
        if ($diff_minutes > 59) {
            $return['mins'] = $diff_minutes - (60 * $diff_hours);
        } else {
            $return['mins'] = $diff_minutes;
        }
        
        $return['sec'] = $diff_seconds-$diff_minutes*60;

        
        if  ($return['mins']< 10) $return['mins'] = '0'.$return['mins'];
        if  ($return['sec']< 10) $return['sec'] = '0'.$return['sec'];
        
        return $return['hours'].':'.$return['mins'].':'.$return['sec'];
        //return $return;
    }
    
}


class  html_input extends html_control{

    public $type = TEXT;
    protected $form = null;
    protected $style = '';
    protected $show_name='';
    protected $name = '';
    protected $value ='';
    protected $events = array();
    static $types=array(TEXT=>'text',RADIO=>'radio',HIDDEN => 'hidden');

    public function __construct($type,$name,$show_name,$value ='',$form = null){
        $this->type=$type;
        $this->name=$name;
        $this->show_name=$show_name;
        if ($value != '') $this->value = $value; 
    }

    function Show($data = null,$add = '')
    {
        if (is_null($data)) $data=$this->value;
        $ev_add='';
        foreach ($this->events as $event=>$value) $ev_add .= $event."='".$value."'";   
        print "<input type=".self::$types[$this->type]." name=\"".$this->name."\" value=\"".htmlspecialchars($data)."\" ".$add." ".$ev_add.">";
    }
    
    function set_event($event,$value)
    {
        $this->events[$event] = $value;
    }

}




class search extends html_control{
    
    
    
    
    
}

class radio extends html_input{
    public $divider="";
    public $selected = -1;
    
    public function __construct($name,$show_name,$value ='',$form = null){
        if (! is_array($show_name)) return null;
        if ($value =='') $value=array_keys ($show_name);
        parent::__construct(RADIO,$name,$show_name,$value,$form);
    }
    
    public function set_default($i)
    {
        
        assert(isset($this->value[$i]));
        $this->selected=$i;
    }
    public function set_selected($i = null)
    {
        if (isset($this->value[$i])) $this->selected=$i;
        //assert(isset($this->value[$i]));
        //$this->selected=$i;
        /*
        //assert($i>-1);
        //if ($i=='') && ($this->default)
        if (is_null($i)) {
            //$this->set_default($i);
            return false;
        } else {
            if (! is_null($this->form) ) $i=$this->form->get_input($name);
            else $i=$_GET[$this->name];
        }
        if (isset($this->value[$i])) $this->selected=$i;
       */
    }

    
    function Show($data = '', $add = '')
    {

        for($i=0;$i<count($this->value);$i++)
        {
            $add='';
            if ($i == $this->selected) $add = 'checked';
            parent::Show($this->value[$i],$add);
            print " ".$this->show_name[$i];
        };
    }
    
}


class html_select extends html_control{
    
    private $data = array();
    
    public function __construct($name,$show_name='',$value = '',$form = null){
        if (! is_array($value)) return false;
        $this->data = $value;
        parent::__construct(SELECT,$name,$show_name,$value,$form);
    }
    
    
    
    function Show($data = 0, $add = '')
    {
        $index = $data;
        print "<select name=\"".$this->name."\" id=\"".$this->get_id()."\" ".$this->style." ".$this->java.">";
        print  html_form::select_box_body($this->data,$index);
        print "</select>";
    }
    
}

class html_date extends html_control{
    private $myCalendar = null;
    
    public function __construct($name,$show_name,$value = '',$form = null){
        global $paths;
        if ($value =='') $value=date('Y-m-d');
        parent::__construct(DATE,$name,$show_name,$value,$form);
        require_once($paths['calendar'].'tc_calendar.php');
        $this->myCalendar = new tc_calendar($this->name, true,false);
        $this->myCalendar->setIcon($paths['www_calendar']."images/iconCalendar.gif");
        $this->myCalendar->setDateYMD($value);
        $this->myCalendar->setPath($paths['www_calendar']);
        $this->myCalendar->zindex = 150; //default 1
        $this->myCalendar->setYearInterval(1901, 2099);
        $this->myCalendar->dateAllow('1901-03-01', '2099-01-01');
        //$myCalendar->setAlignment('right', 'bottom'); //optional
        
    }

    function set_Alignment($a = 'right',$b='bottom')
    {
        $this->myCalendar->setAlignment($a, $b);         
    }
    function Show($data = '', $add = ''){
        $this->myCalendar->writeScript();
    }
    
    
}

class html_date_pair extends html_control{
    
    
    
    
}


class html_video extends html_control{
    private $path ='';
    
    public function __construct($name,$show_name,$value = '',$form = null){
        global $paths;
        if ($value =='') $value=date('Y-m-d');
        parent::__construct(DATE,$name,$show_name,$value,$form);
    }
    
    
    public function ShowFile($file)
    {
        
        print '<script type="text/javascript" src="/external/mediaplayer/swfobject.js"></script>';
        print '<div><p id="player1">
                <a href="http://www.macromedia.com/go/getflashplayer">Get the Flash Player</a> to see this player.</p>
                <script type="text/javascript">
                  var s1 = new SWFObject("/external/mediaplayer/player.swf","single","320","260","7");
                  s1.addParam("allowfullscreen","true");
                  s1.addVariable("autostart","false");
                  s1.addVariable("file","'.$file.'");
                  s1.write("player1");
                </script></div>';
        
        
    }
    
}

class html_page_control{
    public $input_arr; 
    public $curr_page = 1;
    public static $page_id = '_page';
    static $form_name =  'inputdata';
    public $items_per_page = 15;
    public $items_count =0;
    
    public function __construct($items_count,$input = null){
       
        assert ($items_count) > 0;
        $this->items_count = $items_count;
        $post_method = FALSE;
        if (!empty($_POST)) {
            $post_method = TRUE;
        };
        // Generating a form with query data

        if ($post_method) {
            $this->inp_arr = $_POST;
            $method = 'POST';
        } else {
            $this->inp_arr = $_GET;
            $method = 'GET';
        };
        if (! is_null($input)) $this->inp_arr = $input;
        if (isset($this->inp_arr[self::$page_id])) $this->curr_page=$this->inp_arr[self::$page_id];
            
        
    }
    public function mysql_get_limit()
    {
        //return " LIMIT ".$this->current_page.' , '. $this->items_per_page;
    }
    
    public function get_start_item_num()
    {
        assert($this->curr_page>0);
        return ($this->curr_page-1)*$this->items_per_page;
    }
    
    public function get_end_item_num()
    {
        $ret = ($this->curr_page)*$this->items_per_page;
        if ($this->items_count < $ret) $ret  = $this->items_count;
        return $ret;
    }
    
    public function set_items_on_page($i)
    {
         assert($items_per_page) > 0;
         $this->items_per_page = $i;
         
    }
    
    public function pages_count()
    {
        return ceil($this->items_count  / $this->items_per_page);   
    }


public function Show() {
        $pages = $this->pages_count();
        if ($pages < 2 ) return false;
        $form = new html_form($this->form_name, $method, $_SERVER['REQUEST_URI']);
        $form->start();

        foreach ($this->inp_arr as $key => $value) if ($key != self::$page_id)  print "<input type=hidden name=\"" . $key . "\" value=\"" . $value . "\">\n";
            
        print "<input type=hidden name=\"_page\" value=\"" . $this->curr_page . "\">\n";
        
        print "<br>";
        print "<div> стр ";
        $tmp = $this->curr_page - 1;

        if ($this->curr_page > 1) $form->page_link($tmp, 'перед.');
        
        for ($i = 1; $i <= $pages; $i++) {
            if ($this->curr_page == $i) {
                $pn = "<b>" . $i . "</b>";
            } else {
                $pn = $i;
            };
            $form->page_link($i, $pn);
        };

        $tmp = $this->curr_page + 1;
        if ($this->curr_page < $pages) {
            $form->page_link($tmp, 'след.');
        };
        print "<div>";
        $form->finish(false);
    }
}


?>
