<?php

class class_linked_db_object extends Db_object
{
    protected $linked_objects = false;
    protected $linked_ids = array();
    
// more tables is array: table, where, class
    public function __construct($id,$table,$table_name = '',$more_tables = false)
    {	
        parent::__construct($id,$table,$table_name);
        if ($this->datalist === false) 
        {
            throw new Exception("Can't find data for table ".$table_name);            
        };
        // Construct linked object
        if (is_array($more_tables))
        {
            foreach ($more_tables as $value)
            {
                if (isset($value[2])) 
                {
                    $this->addLinkedObject($value[0],$value[1],$value[2]);
                    
                }
                else $this->addLinkedObject($value[0],$value[1]);
            };
        };
        
        
    }
    
    
    public function addLinkedObject($table,$this_object_id,$class = 'Db_object')
    {
        if ($this->linked_objects == false) $this->linked_objects = array();
        $entity = new Db_objects_list($table,"`".$this_object_id."` = ".$this->id(),$class);
        $this->linked_objects[$table] = $entity;
        $this->linked_ids[$table] = $this_object_id;
        
        /*
        $query = "SELECT * FROM `".$table."` WHERE `".$this_object_id."` = ".$this->id();
        $res = System::run_query($query);
        $linked_objects[$table]['class']=$class;
        $linked_objects[$table]['connected_id']=$this_object_id;
        while ($line = mysql_fetch_array($res, MYSQL_ASSOC)){
            $linked_objects[$table]['data']['name']=$line['name'];
            $linked_objects[$table]['id']=$line['id'];
        }
        */
        
    }
    
    
    public function ShowInfo($interest = null)
    {
        parent::ShowInfo($interest);
        if (is_array($this->linked_objects))
        {
            foreach ($this->linked_objects as $key=>$value)
            {
              print "<hr>".$key;      
              $value->Show();
              //$value->getEditLinks();
            };
        };
    }
    
    
    

    
    
    
}

?>
