<?php

require_once(dirname(__FILE__).'/class_db_object.php');

class ClassLog extends  Db_object {
    public function __construct($input = null, $table = '',$table_name = '') {
        global $config;
        if ($table == '') $table=$config['my_sql_logs_table'];
        
        parent::__construct($input, $table, $table_name );
    }
    
    function name()
    {
        return $this->get_field_value('obj_type').' '.$this->get_field_value('time');
    }
    
    public function GetLog($id,$class,$limit = 1)
    {
        global $config;
        $query = "SELECT * FROM ".$this->table." WHERE object_id = ".$id." AND type = '".$class."' ORDER BY time DESC LIMIT ".$limit;
        $result = System::run_query($query);
        //print $query;
        $res = mysql_fetch_array($result, MYSQL_ASSOC);
        
        return $res;
    }
    
    
}

?>
