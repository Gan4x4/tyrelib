<?php

class gan_mail
{
    static $enc='utf-8';

	public static function clear_text($text)
	{
		$bad = array('\r\n\r\n','\r\n','\"');
		$good = array("\n<br>","\n<br>",'"');
		return str_replace($bad,$good,$text);
	}
        
        public static function  get_email_with_siteinfo($title,$email)
        {
            //$title= '"'.$sites[$site]['title'].'"';
            $title=self::add_encodinc($title);
            return $title.'<'.$email.'>';
            
        }
        
        public static function add_encodinc($str,$enc = '')
        {
            if ($enc == '') $enc = self::$enc;
            return "=?".$enc."?b?" .base64_encode($str) . "?=";
        }
        
        public static function get_clear_email($str)
        {
             preg_match("/<(.*)>/",$str,$out);
             return $out[1];
        }
        
        public static function send_mail($mail_to, $thema, $html, $path, $from = '',$name = '' )
 	{
               $thema=self::add_encodinc($thema);
		if (! (self::is_email($mail_to)) )
		{
			print "Указан ошибочный адрес получателя: ".$mail_to;
			return false;
		};

                $files_array=array();
	  	if (! is_array($path))
	  	{
                    if ($path !='') $files_array[1] = array('path' => $path, 'name' => $name);
                    else $files_array=array();
	  	}
	  	else
	  	{
	  		$files_array = $path;
	  	};
	    //$fn=pathinfo($_FILES['mail_file']['name']);
	    //$fn=pathinfo($path);
        //if ($name == '')  $name = $fn['filename'].".".$fn['extension']; // в этой переменной надо сформировать имя файла (без всякого пути)
	    $EOL = "\n"; // ограничитель строк, некоторые почтовые сервера требуют \n - подобрать опытным путём

	    $boundary     = "--".md5(uniqid(time()));  // любая строка, которой не будет ниже в потоке данных.
	    $headers    = "MIME-Version: 1.0;$EOL";
	    $headers   .= "Content-Type: multipart/mixed; boundary=\"$boundary\"$EOL";
            
	    $headers   .= "From: ".$from.$EOL;
            $headers   .= "Reply-To: ".$from.$EOL;
            $headers   .= "Return-Path: ".self::get_clear_email($from).$EOL;

	    $multipart  = "--$boundary$EOL";
	    $multipart .= "Content-Type: text/html; charset=UTF-8 $EOL";
	    $multipart .= "Content-Transfer-Encoding: base64$EOL";
	    $multipart .= $EOL; // раздел между заголовками и телом html-части
	    $multipart .= chunk_split(base64_encode($html));
	    $multipart .=  "$EOL--$boundary$EOL";

	    //var_dump($files_array);
            $files_count = count($files_array);
            $i=0;
            foreach ($files_array as $value)
	    {
                    $i++;
		    $path = $value['path'];
		    if (! isset($value['name']))
		    {
		    	$fn=pathinfo($value['path']);
        		if ($name == '')  $name = $fn['filename'].".".$fn['extension'];
		    }
		    else
		    {
		    	$name = $value['name'];
		    };
		    $include=false;
		    if ($path)
		  	{
		    	$fp = fopen($path,"rb");
		    	if ($fp)
			    {
			    	$file = fread($fp, filesize($path));
				    fclose($fp);
				    $include=true;
			    };
	    	};

        	if ($include)
        	{
                    //print "<br> File ".$name;
			    $multipart .= "Content-Type: application/octet-stream; name=\"$name\"$EOL";
			    $multipart .= "Content-Transfer-Encoding: base64$EOL";
			    $multipart .= "Content-Disposition: attachment; filename=\"$name\"$EOL";
			    $multipart .= $EOL; // раздел между заголовками и телом прикрепленного файла
			    $multipart .= chunk_split(base64_encode($file));
                    if ($i< $files_count)
                   {
                       $multipart .= "$EOL--$boundary$EOL";
                       //print "Not the end";
                   };
		};
            };
            $multipart .= "$EOL--$boundary--$EOL";
            //good_mail($to,$subject,$text,$from='',$enc='utf-8')
            if(!mail($mail_to, $thema, $multipart, $headers))
            //if (good_mail($mail_to,$thema,$multipart,$from='',$enc='utf-8'))
            {
                return false;           //если не письмо не отправлено
            }
            else { // если письмо отправлено
                   //mail('gan4x4@yandex.ru', $thema, $multipart, $headers);
                   return true;
            };
	}

	public static function preview($mail_subject,$mail_message)
	{
		print self::clear_text($mail_subject);
		print "<hr>".self::clear_text($mail_message);
	}

	function get_mail($str)
	{
		// Extract email from string
		$arr=preg_split("/[\s,;]+/",$str);
		//var_dump($arr);
		foreach ($arr as $value)
		{
                    //print "<br> hh".$value;
                    if (self::is_email($value)) return $value;
		};
		return "";
	}
        
        
        
    public static function is_email($mail)
    {
      if($mail !== "") 
      {
        if(preg_match("/^[-A-Za-z0-9_]+[-A-Za-z0-9_.]*[@]{1}[-A-Za-z0-9_]+[-A-Za-z0-9_.]*[.]{1}[A-Za-z]{2,5}$/", $mail)) 
        {
            return true;
        }
        else 
        {
            return false;
        };
      } 
      else 
      {
        return false;
      };
    }

};

?>