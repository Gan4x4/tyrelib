<?php
/*
if (file_exists($_SERVER['DOCUMENT_ROOT'].'/engine/config.php')){
    require_once($_SERVER['DOCUMENT_ROOT'].'/engine/config.php');
} 
else{
    require_once(realpath($_SERVER['DOCUMENT_ROOT'].'/../engine').'/config.php');
};
*/
require_once(dirname(__FILE__).'/class_log.php');
require_once (dirname(__FILE__).'/class_cache.php');
require_once (dirname(__FILE__).'/class_db.php');

class System
{
    //static private $db_link = false;
    //static private $db_is_link = false;
    private static $conf_data = array();
    static public $cache = null;
    static protected $db = null;

    public function __construct($conf_data = '')
    {
	global $config;
	if ($conf_data == '') self::$conf_data = $config;
        else self::$conf_data = $conf_data;
	if (self::isDebug()){
            ini_set('display_errors',1); //PHP_INI_ALL
            error_reporting(E_ALL & ~E_NOTICE );
	}
	self::DB_connect(self::$conf_data);  
        if (! is_object(self::$cache)) self::$cache = new DB_cache(); 
        //if (isset(self::$conf_data['my_sql_init_queryes']))
    }	
    
    private static function isDebug()
    {
        return defined('DEBUG') && DEBUG != false;
    }


    public static function DbRunSomeQueryes($ql)
    {   /*
        if (! is_array($ql))
        {
            if (trim($ql) == '') return false;
            $ql = array($ql);
        };
        if (count($ql) == 0) return false;
        foreach ($ql as $query)
        {
            if (self::run_query($query) == false) return false ;
        };
        return true;
     
     */
        return self::$db->DbRunSomeQueryes($ql);
    }

    
static public function get_config_var($arr,$name = ''){
        //global $gan_config;
        $gv=@self::$conf_data;
        if (is_array($gv))
        {
            if ( ($name == '') && (isset($gv[$arr]))) return $gv[$arr];
            if (($name != '') && (isset($gv[$arr][$name]))) return $gv[$arr][$name];

        };
        return false;
    }


    public static function Log_Error($text,$object_type = 0){	    
        $dt=date('Y-m-d G:i:s');
        $text= $dt." ".$text." by ".$object_type;
        if (self::isDebug()) { 
            print $text."<br>"; 
        }
        else {
            $log_file = self::get_config_var('paths','system_error_log');
            if (empty($log_file)){
                // use file defined in php.ini
                error_log($text."\n");
            }
            else{
                // Use user file
                error_log($text."\n", 3, $log_file);
            }
        }
    }

    public static function Log_message($text,$object_type=0)
    {
   	    $text= date('Y-m-d G:i:s')." ".$text." by ".$object_type;
	    if (self::isDebug()) { 
                print $text."<br>"; 
            }
	    else {
                error_log($text."\n", 3, self::get_config_var('paths','system_message_log'));
            }
    }
    
    protected function DB_connect($conf_data){
        $user = self::get_config_var('my_sql_db_user');
        if (empty($user)) {
            $user = $conf_data['my_sql_db_name'];
        }
        
        self::$db = self::createDb($conf_data['my_sql_db_name'], $user, $conf_data['my_sql_db_pwd'],$conf_data['my_sql_server'],$conf_data['my_sql_init_queryes']);
        /*
        if (extension_loaded('pdo_mysql')){
            self::$db = new DB_pdo($conf_data['my_sql_db_name'], $user, $conf_data['my_sql_db_pwd'],$conf_data['my_sql_server'],$conf_data['my_sql_init_queryes']);
        }
        else{
            self::$db = new DB_mysql($conf_data['my_sql_db_name'], $user, $conf_data['my_sql_db_pwd'],$conf_data['my_sql_server'],$conf_data['my_sql_init_queryes']);
        }
         * 
         */
     }

     
    public static function createDb($name,$user,$password,$host,$init_querys){
        if (extension_loaded('pdo_mysql')){
            return new DB_pdo($name, $user, $password,$host,$init_querys);
        }
        else{
            return new DB_mysql($name, $user, $password,$host,$init_querys);
        }
    }
     
    public static function createDbForSite($site)
    {
        $query_list_win_1251=array('SET collation_connection = cp1251_general_ci',' SET collation_database = cp1251_general_ci',' SET collation_server = cp1251_general_ci ', ' SET character_set_client = utf8 ' , ' SET character_set_connection = cp1251 ' , '  SET character_set_database = cp1251 ' , ' SET character_set_results = utf8 ' , ' SET character_set_server = cp1251' , " SET character_set_server = 'cp1251' ");
        $query_list_utf8 = System::get_config_var('my_sql_init_queryes');
        if ($site['enabled'] == false){
            return false;
        }
        $ql = $query_list_utf8;
        if ($site['utf8'] == false ){
            $ql =  $query_list_win_1251;
        }
        return self:: createDb($site['db_name'], $site['db_login'], $site['db_password'],$site['ip'],$ql);
        //$d = new DB_pdo(); 
//            if ($d != null) {
    }
     
    public static function siteList2DbList($sites){
        if (! is_array($sites) || count($sites) == 0){
            throw new Exception("Empty sites list");
        }
        $dbList = array();
        foreach ($sites as $site){
            $dbList[] = \System::createDbForSite($site);
        }
        return $dbList;
    }
     
     
    public static function getDb(){
        return self::$db;
    }


protected function get_system_var($name)
    {	  
        
        
        $congig=self::get_config_var('config');
        //if (! $this->db_link ) throw new Exception(' get_system_var: Connection to server not found');

        //$result=System::run_query(" SELECT * FROM `".$this->conf_data['my_sql_system_table']."` WHERE name = '".$name."'");
        $result = self::run_query($query);
        
        if ( count($result) == 0) {
            throw new Exception('Var not found ');
        }
        else {	
            //$res = mysql_fetch_array($result, MYSQL_ASSOC);
            return $result['value'];		};
        return false;
    }

    public static function setDb($db)
    {
        self::$db=&$db;
    }
    

public static function run_query($query,$db = null)
    {
    /*
        $res=mysql_query($query,self::$db_link);
        if (! $res)
        {
            System::Log_error( " Query not completed : ".$query." | " . mysql_error() );    
            return false;
        };
        return $res;
     
     */
        
        if ($db == null ) {
            $db = self::$db;
        }
        $res = $db->run_query($query);
        if ($res === false){
            $error = "SQL Error : ".$query." ; ".$db->getLastError();
            self::Log_Error($error);
            throw new Exception ($error);
        };
        return $res;
    }
    
    public static function run_is_query($query)
    {
        /*
        $db = self::get_config_var('my_sql_db_is');
        //print "IS QUERY : ".$query." -- ".$db."<br>";
        //$db_is_link = mysql_select_db($db);
        if (! mysql_select_db($db,self::$db_link))
        {
            System::Log_error( "Not connect connect to  ". $db." ".mysql_error() );
            return false;
        };
        
        
        //$query_list = self::get_config_var('my_sql_init_queryes');
        
        //$query_list=array_merge(self::get_config_var('my_sql_is_collation_list'),self::get_config_var('my_sql_collation_list'));
        //$query_list=array('SET collation_connection = utf8_general_ci',' SET collation_database = utf8_general_ci',' SET collation_server = utf8_general_ci ', ' SET character_set_client = utf8 ' , ' SET character_set_connection = utf8 ' , '  SET character_set_database = cp1251 ' , ' SET character_set_results = utf8 ' , ' SET character_set_server = utf8');
        
        foreach ($query_list as $value)
        {
            $result = mysql_query($valuev) or die("Query failed".mysql_error().$value);
        };
         
        
        $res=mysql_query($query,self::$db_link);
        if (! $res)
        {
            System::Log_error( " Query not completed : ".$query." | " . mysql_error() ); 
            return false;
        };
        mysql_select_db(self::get_config_var('my_sql_db_name'),self::$db_link);
        return $res;
         
         */
        return self::$db->run_is_query($query);
    }

    public static function importSql($sql)
    {
        return self::$db->import($sql);
    }
    

    public static function escape_string($string)
    {
        return self::$db->escape_string($string);
    }
    protected function set_system_var($name,$value)
    {
	    //$config=self::get_config_var('config');
  	    if (! $this->db_link ) throw new Exception(' set_system_var:  Connection to server not found ');

  	    if (System::get_system_var($name) === false) throw new Exception(' set_system_var:  var with name "'.$name.'" dos not exist ');

		if (! System::run_query("UPDATE  `".self::get_config_var('my_sql_system_table')."` SET value = '".$value."' WHERE name = '".$name."'"))
		{		
                    return false;
		};
		return true;
    }
    
    public function upload_site_block($file_name,$name)
    {
  // ����� ����������
        /*
        global $site_blocks,$names,$paths,$config;
        $congig=self::get_config_var('config');
        ob_start();
        if (file_exists($file_name))
        {
            include($file_name);
        } 
        else 
        {
            System::Log_message(" No block to show :  ".$name);
        };
        $site_blocks[$name] = ob_get_clean();
*/

    }
    
    static function Safe_get_int($pn)
    {
        if ((isset($_GET[$pn])) && (is_numeric($_GET[$pn]))) { return (int) $_GET[$pn] ; }
        else {
            //System::Log_message(" Bad get  ".$pn); 
            return false;
        };
        
    }
    
    static function Safe_get_str($pn)
    {
        //  Ned to be more safe
        if ((isset($_GET[$pn]))) { return $_GET[$pn] ; }
        else {
            //System::Log_message(" Bad get  ".$pn); 
            return false;
        };
        
    }
    
    static function getInputArray($pn){
        $array = false;
        if (isset($_GET[$pn]))  $array = @$_GET;
        elseif (isset($_POST[$pn])) $array = @$_POST;
        //if ($array == false){
        //    throw new Exception("Parametr $pn not found in input!");
        //}
        return $array;
    }
    
    static function Safe_input_str($pn,$array = null)
    {
        //  Ned to be more safe
        if ($array == null) $array= self::getInputArray($pn);
        
        if ((isset($array[$pn]))) { return $array[$pn] ; }
        else {
            //System::Log_message(" Bad get  ".$pn); 
            return false;
        };
        
    }
    
    
    static function Safe_input_int($pn,$array = null)
    {
        if ($array == null)
        {
            //if (isset($_GET[$pn]))  $array = @$_GET;
            //elseif (isset($_POST[$pn])) $array = @$_POST;
            $array= self::getInputArray($pn);
        };
        if ((isset($array[$pn])) && (is_numeric($array[$pn]))) {
            return intval($array[$pn]); 
            };
        //if ((isset($_POST[$pn])) && (is_numeric($_POST[$pn]))) { return (int) $_POST[$pn] ; };
        //System::Log_message(" Bad get  ".$pn); 
        return false;
    }
    
     static function get_some_text($text,$n)
        {
            $tmptext=rtrim($text);
            
            $tmptext=substr($tmptext,0,$n);
            $pn=strrpos($tmptext,'.');
            if (is_bool($pn) && !$pn)
            {
               // not found...
                $tmp2=$tmptext.' ...';
            }
            else
            {
                $tmp2=substr($tmptext,0,$pn+1);
            };

            if ( strlen($tmp2) < 10) {$tmp2=$tmptext.' ...';};
            return $tmp2;
        }
    
        
        
    public static function get_db_data_by_field($table,$field)
    {
        $data=array();
        $res=System::run_query("SELECT ".$field." FROM `".$table."` GROUP BY ".$field);
        if (! $res) 
        {
            System::log_error(" Do not collect data from field ".$field." from table ".$table);
            return false;
        }
        $i=0;
        //while ($line = mysql_fetch_array($res, MYSQL_ASSOC)){
        foreach($res as $line)
        {
            $data[$i]=$line[$field];
            $i++;
        }
        return $data;
    }
    
    public static function get_db_data_by_id($table,$field,$id = 'id')
    {
        $data=array();
        $res=System::run_query("SELECT ".$id.", ".$field." FROM `".$table."` ORDER by ".$field);
        //print "SELECT ".$id.", ".$field." FROM `".$table."` ORDER by ".$field;
        if (! $res) 
        {
            System::log_error(" Do not collect data from field ".$field." from table ".$table);
            return false;
        }
        //while ($line = mysql_fetch_array($res, MYSQL_ASSOC))
        foreach($res as $line){
            $data[$line[$id]]=$line[$field];
			//$data[$line[$id]]=$line[$field];
        }
        return $data;
    }
    
    public static function create_commas_list($arr,$add = ', ')
    {
        $ca=' ';
        $res='';
        if ((! is_array($arr) ) || (count($arr) ==0)) return false;
        foreach ($arr as $value)
        {
            $res.=$ca.$value;
            $ca=$add;
            
        }
        return $res;
        
    }
    
    /*
    // Generate part of sql query
    // @param $list array- List of word
    // @param $name string - field where we search
     * 
     */
    public static function prepareNamesToSqlLike($list,$name,$operator = " OR "){
        $escaped = array();
        if (empty($list)){
            return '';
        }
        if (! is_array($list)){
            $list = array($list);
        }
        $unique =array_unique($list);
        foreach ($unique as $sin){
            $preparedSin = mb_strtolower(trim($sin));
            $escaped[] = " (LOWER($name) LIKE '%".$preparedSin."%') ";
        }
        return self::create_commas_list($escaped,$operator);
    }
    
    
    static public function get_dir_list($dir)
    {
        $res=array();
        //if ($restricted == false) $restricted = array();
        if (! ($dirs=scandir($dir))) return $res;
        foreach ($dirs as $value)
        {
            $d=$dir.'/'.$value;
            if ((is_dir($d)) && ($value != '.') && ($value != '..') && (strpos(strtolower($value),'thumb') === false)) 
            {
                $res[]=$d;
                $r1=System::get_dir_list($d);
                $res=array_merge($res, $r1);
            }
        };
        return $res;
        
    }
    
    
    public static function GetFileForBrowser($fn,$show_name)    
        {
            $fsize=filesize($fn);
            if (file_exists($fn) && (! is_dir($fn) ))
            {
                Header("HTTP/1.1 200 OK");
                Header("Connection: close");
                Header("Content-Type: application/octet-stream; charset=utf-8");
                Header("Accept-Ranges: bytes");
                Header("Content-Disposition: Attachment; filename=\"".$show_name."\"; charset=utf-8");
                Header("Content-Length: ".$fsize);
                readfile($fn);
            }
            else System::Log_Error ("File not exists :".$fn);
        }
        
    public static function changeExt($fn,$ext = '')
    {
        $dot = strrpos($fn, '.');
        if ($dot === false) return false;
        $ret = substr($fn,0,$dot);
        if ($ext !='') $ext = '.'.$ext;
        return $ret.$ext;
    }
    
    
    static function GetTmpFileName($ext,$dir,$uid = 0)
    {
        
        $pos = strrpos($ext, '.');
        if ($pos !== false) $ext=substr($ext,$pos+1); 
        do 
        {
            $fn=tempnam($dir,$uid);
            $pi=pathinfo($fn);
            $fn=$dir."/".$pi['filename'].'.'.$ext;
            $uid=rand(1,32000);
        } while (file_exists($fn));
        return $fn;
    }
    
    
    public static function getPermalink(){
        return "http://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI']; 
    }
    
};
?>
