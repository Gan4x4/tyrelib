<?php
    function expandDirectories($base_dir){
        //http://stackoverflow.com/questions/14304935/php-listing-all-directories-and-sub-directories-recursively-in-drop-down-menu
        $directories = array();
        if (! empty($base_dir)) {
            foreach(scandir($base_dir) as $file) {
                if($file == '.' || $file == '..') continue;
                $dir = $base_dir.DIRECTORY_SEPARATOR.$file;
                if(is_dir($dir)) {
                    $directories []= $dir;
                    $directories = array_merge($directories, expandDirectories($dir));
                }
            }
        }
      return $directories;
    }
    
    
    function autoload($className){
        
        
        $libs = realpath(__DIR__."/..").DIRECTORY_SEPARATOR;
        $ext = '.php';
        $phpbbClasses = array('phpbb_default_captcha','phpbb_recaptcha','bbcode','utf_normalizer','utf_normalizer');
        if (in_array($className, $phpbbClasses)){
            // phpbb must have self autoload
            return false;
        }
        
        
        
        // Namespaces
        if (strpos($className, "\\") !== false ){
            //print $className."<br>";
            $file =  $libs.str_replace('\\', DIRECTORY_SEPARATOR, $className).$ext;
            if (file_exists($file)) {
                require_once $file;
                return;
            }
            else {
                //print "Class not found: ".$file.'<br>';
                error_log("Class not found: ".$className);
            }
            return;
        }
        
        
         $legacyClasses =array(
            'Db_object'=> 'class_db_object',
            'Db_object_list'=> 'class_db_object',
            'System'=>'class_system',
            'complex_db_object'=>'class_complex_db_object'
        );
    
        if (isset($legacyClasses[$className])) {
            $className = $legacyClasses[$className];
        }
        
        
        $ganlib = $libs."ganlib";
        //$adminClasses = realpath($_SERVER['DOCUMENT_ROOT']."/../admin/engine/classes");
        //$adminClasses = realpath($_SERVER['DOCUMENT_ROOT']."/../admin/engine/classes");
        $dirs = array($ganlib);
        
        $success = false;
        //var_dump($dirs);
        foreach ($dirs as $baseDir){
            $subdirs = expandDirectories($baseDir);
            $subdirs[] = $baseDir;
            foreach ($subdirs as $dir){
                $filename = $dir.DIRECTORY_SEPARATOR.$className.$ext;
                //print "autoload - called: ".$filename.'<br>';
                if (is_readable($filename )) {
                    require_once $filename;
                    $success = true;
                    break;
                }
            }
        }
        if (! $success) {
            print ("Class not found: ".$className);
        }
    }
    
    spl_autoload_register('autoload');
    
