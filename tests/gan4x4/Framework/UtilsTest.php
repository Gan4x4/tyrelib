<?php

namespace gan4x4\Framework;

class UtilsTest extends \PHPUnit_Framework_TestCase {

    public function testCreateCompactStrings() {
        $rawText = ["Toyota Land Cruiser 80", 
            "Toyota Land Cruiser 70",
            "Toyota Hilux V",
            "Toyota Hilux IV",
            "Toyota 4Runner",
            "Mitsubishi Pajero I",
            "Mitsubishi Pajero II",
            "Mitsubishi Pajero Sport"];
        $actual = Utils::createCompactString($rawText);
        $expected = 'Toyota Land Cruiser 80/70, Hilux V/IV, 4Runner; Mitsubishi Pajero I/II/Sport';
        $this->assertEquals($expected,$actual);
        
    }

}
