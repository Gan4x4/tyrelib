<?php
    use gan4x4\Market\Busket;
    class BusketTest extends PHPUnit_Framework_TestCase {

        static $mockKey = 'key';
        static $mockSession  = [];
        
    protected function setUp() {
        
        //$sys = new System($config);
        
    }

    protected function tearDown() {
          
    }

    public function testBase() {
        //$sut = new MarketOrderSource(['utm_www'=>56]);
        //assert(true);
        $busket = new Busket(self::$mockKey,self::$mockSession);
        $busket->addGood('ta9010',5);
        $amount = $busket->getAmount('ta9010'); 
        $this->assertEquals(5,$amount);
        $this->assertEquals(0,count(self::$mockSession));
        try {
            $busket->save();    
            $this->fail("Cookie exception maut be thrown");
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
        var_dump(self::$mockSession);
        $this->assertEquals('ta90105',self::$mockSession[self::$mockKey]);
    }
    
    
    
    public function testGetGoodsIdsAndAmountArray(){
        $busket = new Busket(self::$mockKey,self::$mockSession);
        $busket->addGood('ta9010',5);
        $busket->addGood('ta9011',4);
        $array = $busket->getGoodsIdsAndAmountArray();
        $this->assertEquals(5,$array['ta9010']);
        $this->assertEquals(4,$array['ta9011']);
    }
    
}
