<?php
    class TyreTest extends PHPUnit_Framework_TestCase {
    private $img;
        
    protected function setUp() {
        //$sys = new System($config);
        $this->img =  new Image(__DIR__.'/black_rectangle.jpg');
    }

    protected function tearDown() {
          
    }

    public function testScanRowUp() {
        
        $y = $this->img->scanRowUp();
        $this->assertEquals(4,$y);
    }
    
    public function testScanRowDown() {
        $y = $this->img->scanRowDown();
        $this->assertEquals(23,$y);
    }
    
    public function testScanColLeft() {
        $x = $this->img->scanColLeft();
        $this->assertEquals(4,$x);
    }
    
    public function testScanColRight() {
        $x = $this->img->scanColRight();
        $this->assertEquals(17,$x);
    }
    
    public function testAutoCrop() {
        $this->img->autoCrop(__DIR__."/test.jpg");
    }
    
    
    public function testIsCornersWhite() {
        $this->assertTrue($this->img->isCornersWhite());
    }
    
    public function testIsCornersWhiteNegative() {
        $badCornerImg = new Image(__DIR__.'/bad_corner.jpg');
        $this->assertFalse($badCornerImg->isCornersWhite());
    }
    
}