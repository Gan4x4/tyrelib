<?php
    class MareketBrandTest extends PHPUnit_Framework_TestCase {

    protected function setUp() {
        //$sys = new System($config);
    }

    protected function tearDown() {
          
    }

    public function testpopulateTypeByRequestByWheelAndTyre() {
        $request = array('type_d'=>'on','type_t'=>'on');
        $brand = new MarketBrand($request);
        $actual = $brand->get_field_value('type');
        $exprcted = "t, d";
        $this->assertEquals($actual,$exprcted);
    }

    public function testpopulateTypeByRequestTyreOnly() {
        $request = array('type_t'=>'on');
        $brand = new MarketBrand($request);
        $actual = $brand->get_field_value('type');
        $exprcted = "t";
        $this->assertEquals($actual,$exprcted);
    }
    
}