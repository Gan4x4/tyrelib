<?php



namespace gan4x4\Market\Size;

use gan4x4\Market\Size;
use gan4x4\Market\Tyre;

require_once(__DIR__."/TyreSizeTest.php");


   
    class AutorosTyreSizeTest extends TyreSizeTest {
        
        
        
    protected function setUp() {
        //$sys = new System($config);
    }

    protected function tearDown() {
          
    }
    
    public function testFactoryAvtoros() {
        $size = TyreSize::parseSize('600-55х21 LT');
        $this->assertEquals(__NAMESPACE__.'\\AvtorosTyreSize',get_class($size));
    }
    
    public function testIsAvtorosSizeTypicalPositive() {
        $this->assertTrue(AvtorosTyreSize::checkSize("450-45х18"));
    }
    
    public function testAvtorosSizeGetMetricName(){
        $size = new AvtorosTyreSize("700-55х21 LT");
        $this->assertEquals('700/55-21',$size->getMetricName());
    }
    
    public function testAvtorosSizeGetInchName(){
        $size = new AvtorosTyreSize("600-55х21 LT");
        $this->assertEquals("46x23.5-21",$size->getInchName());
    }

    
    /*    
    public function testInchSizeConstructor() {
        $this->checkSizesList($this->inch, 'InchTyreSize');
    }

    public function testInchSizeGetRadialCord(){
        $size = new InchTyreSize("31x10.5R15");
        $this->assertEquals(Tyre::CORD_RADIAL,$size->getCord());
    }
    
    public function testInchSizeGetDiagonalCord(){
        $size = new InchTyreSize("36x12.5-16");
        $this->assertEquals(Tyre::CORD_DIAGONAL,$size->getCord());
    }
    
    
    public function testInchSizeGetMetricName(){
        $size = new InchTyreSize("36x12.5-15");
        $this->assertEquals('320/85-15',$size->getMetricName());
    }
    
    public function testInchSizeGetInchName(){
        $size = new InchTyreSize("36x12.5-15");
        $this->assertEquals("36x12.5-15",$size->getInchName());
    }
    
    public function testInchSizeGetInchNameComma(){
        $size = new InchTyreSize("32X11,50R15LT");
        $this->assertEquals("32x11.5R15",$size->getInchName());
    }
    
    

    
  */  
    
}