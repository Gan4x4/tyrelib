<?php


namespace gan4x4\Market\Size;
use gan4x4\Market\Size;
use gan4x4\Market\Tyre;


define("ORIGINAL",0);
define("METRIC",1);
define("INCH_H",2);
define("INCH_W",3);
define("DISK",4);
define("METRIC_W",5);
define("METRIC_H",6);
//define("CORD",6);

    
    class TyreSizeTest extends \PHPUnit_Framework_TestCase {
        protected $inch = array(
            array("31x10.5R15","265/75R15",31,10.5,15,265,75),
            array("33x12.5 R16.5","320/65 R16.5",33,12.5,16.5,320,65)
            );
        
        protected $metric = array(
            array("215/75R15","27.5x8.5 R15",27.5,8.5,15,215,75),
            array("305/70 R16","33x12 R16",33,12,16,305,70)
            );
        
        protected $pneumo = array(
            array("1100х400-20","-",43.5,15.5,20,400,75),
            array("1150х620-22,5","-",45.5,24.5,22,620,50)
            );

        protected $fullsize = array(
            array("195R16","195/80R16",28.5,7.5,16,195,80),
            );
        
        protected $inchfullsize = array(
            array("7.50-16","",28,7.5,16,190,80),
        );
        
        protected $american = array(
            array("Q78-16","",35.5,11,16,280,90),
            array("Q78-15","",35.5,11,15,280,95)
            );

        protected $reverse = array(
            array("15/39.5-16.5","",39.5,15,16.5,380,75),
            array("19.5/44 -16.5LT","",44,19.5,16.5,495,70)
            );
        
        
    protected function setUp() {
        //$sys = new System($config);
    }

    protected function tearDown() {
          
    }

    public function testIsInchSizeTypicalPositive() {
        $this->assertTrue(InchTyreSize::checkSize("31x10.5R15"));
    }
    
    public function testIsInchSizeWithWhiteSpacePositive() {
        $this->assertTrue(InchTyreSize::checkSize("  35x10.5 R22 "));
    }
    
    public function testIsInchSizeDiagonalPositive() {
        $this->assertTrue(InchTyreSize::checkSize("36x10-16"));
    }
    
    public function testIsInchSizeHummerWheelPositive() {
        $this->assertTrue(InchTyreSize::checkSize("37x12.5-16.5"));
    }
    
    public function testIsInchSizeCommaPositive() {
        $this->assertTrue(InchTyreSize::checkSize("LT31x10,5 R17"));
    }
    
     
    public function testIsInchSizeBigXPositive() {
        $this->assertTrue(InchTyreSize::checkSize("BFG 30X9.50R15"));
    }
    
    public function testIsInchSizeCrazyDelimPositive() {
        $this->assertTrue(InchTyreSize::checkSize(" 30/9.50R15"));
    }
    
    public function testIsInchSizeStarkPositive() {
        // With Russian x
        $this->assertTrue(InchTyreSize::checkSize("39.5х16.5-16"));
    }
    
    
        
    public function testIsInchSizeThreeDigitHeigthNegative() {
        $this->assertFalse(InchTyreSize::checkSize("300x9.50R15"));
    }
    
    public function testIsInchWithMetricNegative() {
        $this->assertFalse(InchTyreSize::checkSize("285/75R16"));
    }
    
    public function testIsInchEmptyStringNegative() {
        $this->assertFalse(InchTyreSize::checkSize(""));
    }

   


    public function testInchSizeConstructor() {
        $this->checkSizesList($this->inch, 'InchTyreSize');
    }

    public function testInchSizeGetRadialCord(){
        $size = new InchTyreSize("31x10.5R15");
        $this->assertEquals(TyreSize::CORD_RADIAL,$size->getCord());
    }
    
    public function testInchSizeGetDiagonalCord(){
        $size = new InchTyreSize("36x12.5-16");
        $this->assertEquals(TyreSize::CORD_DIAGONAL,$size->getCord());
    }
    
    
    public function testInchSizeGetMetricName(){
        $size = new InchTyreSize("36x12.5-15");
        $this->assertEquals('320/85-15',$size->getMetricName());
    }
    
    public function testInchSizeGetInchName(){
        $size = new InchTyreSize("36x12.5-15");
        $this->assertEquals("36x12.5-15",$size->getInchName());
    }
    
    public function testInchSizeGetInchNameComma(){
        $size = new InchTyreSize("32X11,50R15LT");
        $this->assertEquals("32x11.5R15",$size->getInchName());
    }
    
    

    public function testFactoryInch() {
        $size = TyreSize::parseSize('35/12.50R15LT');
        $this->assertEquals(__NAMESPACE__.'\\InchTyreSize',get_class($size));
    }
    
    public function testConstructorNegative() {
        try{
            $size = TyreSize::parseSize('blablabla');
            $this->assertTrue(false);
        } catch (\Exception $ex) {
            $this->assertTrue(true);
        }
    }
    
    
    public function testCreatingInchSize(){
        
    }
    
    
}