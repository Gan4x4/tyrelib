<?php
    namespace gan4x4\Market\Size;
    use gan4x4\Market\Size;
    use gan4x4\Market\Tyre;
    //use gan4x4\Market\TyreSize;
    /*
    use gan4x4\Market\Size\InchTyreSize;
    use gan4x4\Market\Size\MetricTyreSize;
    use gan4x4\Market\Size\PneumoTyreSize;
    use gan4x4\Market\Size\ReverseInchTyreSize;
*/

    define("ORIGINAL",0);
    define("METRIC",1);
    define("INCH_H",2);
    define("INCH_W",3);
    define("DISK",4);
    define("METRIC_W",5);
    define("METRIC_H",6);
//define("CORD",6);

    
    class TyreSizeTest extends \PHPUnit_Framework_TestCase {
        protected $inch = array(
            array("31x10.5R15","265/75R15",31,10.5,15,265,75),
            array("33x12.5 R16.5","320/65 R16.5",33,12.5,16.5,320,65)
            );
        
        protected $metric = array(
            array("215/75R15","27.5x8.5 R15",27.5,8.5,15,215,75),
            array("305/70 R16","33x12 R16",33,12,16,305,70)
            );
        
        protected $pneumo = array(
            array("1100х400-20","-",43.5,15.5,20,400,75),
            array("1150х620-22,5","-",45.5,24.5,22,620,50)
            );

        protected $fullsize = array(
            array("195R16","195/80R16",28.5,7.5,16,195,80),
            );
        
        protected $inchfullsize = array(
            array("7.50-16","",28,7.5,16,190,80),
        );
        
        protected $american = array(
            array("Q78-16","",35.5,11,16,280,90),
            array("Q78-15","",35.5,11,15,280,95)
            );

        protected $reverse = array(
            array("15/39.5-16.5","",39.5,15,16.5,380,75),
            array("19.5/44 -16.5LT","",44,19.5,16.5,495,70)
            );
        
        
    protected function setUp() {
        //$sys = new System($config);
    }

    protected function tearDown() {
          
    }

    public function testIsInchSizeTypicalPositive() {
        $this->assertTrue(InchTyreSize::checkSize("31x10.5R15"));
    }
    
    public function testIsInchSizeWithWhiteSpacePositive() {
        $this->assertTrue(InchTyreSize::checkSize("  35x10.5 R22 "));
    }
    
    public function testIsInchSizeDiagonalPositive() {
        $this->assertTrue(InchTyreSize::checkSize("36x10-16"));
    }
    
    public function testIsInchSizeHummerWheelPositive() {
        $this->assertTrue(InchTyreSize::checkSize("37x12.5-16.5"));
    }
    
    public function testIsInchSizeCommaPositive() {
        $this->assertTrue(InchTyreSize::checkSize("LT31x10,5 R17"));
    }
    
     
    public function testIsInchSizeBigXPositive() {
        $this->assertTrue(InchTyreSize::checkSize("BFG 30X9.50R15"));
    }
    
    public function testIsInchSizeCrazyDelimPositive() {
        $this->assertTrue(InchTyreSize::checkSize(" 30/9.50R15"));
    }
        
    public function testIsInchSizeThreeDigitHeigthNegative() {
        $this->assertFalse(InchTyreSize::checkSize("300x9.50R15"));
    }
    
    public function testIsInchWithMetricNegative() {
        $this->assertFalse(InchTyreSize::checkSize("285/75R16"));
    }
    
    public function testIsInchEmptyStringNegative() {
        $this->assertFalse(InchTyreSize::checkSize(""));
    }

    /*
    protected function checkSizesList($list,$class){
        foreach ($list as $size){
            assert(count($size) == 7 );
            $inch_size = new $class($size[ORIGINAL]);
            $add = "failed in $class for ". $size[ORIGINAL];
            $this->assertEquals($size[INCH_H],$inch_size->getInch_H(),"Inch H ".$add);
            $this->assertEquals($size[INCH_W],$inch_size->getInch_W(),"Inch W failed ".$add);
            $this->assertEquals($size[DISK],$inch_size->getDisk(),"Disk failed ".$add);
            $this->assertEquals($size[METRIC_H],$inch_size->getCm_H(),"Cm H failed ".$add);
            $this->assertEquals($size[METRIC_W],$inch_size->getCm_W(),"Cm W failed ".$add);
        }
    }
    */
     protected function checkSizesList($list,$class){
        $class = __NAMESPACE__.'\\'.$class;
        foreach ($list as $size){
            assert(count($size) == 7 );
            $inch_size = new $class($size[ORIGINAL]);
            $add = "failed in $class for ". $size[ORIGINAL];
            $this->assertEquals($size[INCH_H],$inch_size->getInch_H(),"Inch H ".$add);
            $this->assertEquals($size[INCH_W],$inch_size->getInch_W(),"Inch W failed ".$add);
            $this->assertEquals($size[DISK],$inch_size->getDisk(),"Disk failed ".$add);
            $this->assertEquals($size[METRIC_H],$inch_size->getCm_H(),"Cm H failed ".$add);
            $this->assertEquals($size[METRIC_W],$inch_size->getCm_W(),"Cm W failed ".$add);
        }
    }


    public function testInchSizeConstructor() {
        $this->checkSizesList($this->inch, 'InchTyreSize');
    }

    public function testInchSizeGetRadialCord(){
        $size = new InchTyreSize("31x10.5R15");
        $this->assertEquals(TyreSize::CORD_RADIAL,$size->getCord());
    }
    
    public function testInchSizeGetDiagonalCord(){
        $size = new InchTyreSize("36x12.5-16");
        $this->assertEquals(TyreSize::CORD_DIAGONAL,$size->getCord());
    }
    
    
    public function testInchSizeGetMetricName(){
        $size = new InchTyreSize("36x12.5-15");
        $this->assertEquals('320/85-15',$size->getMetricName());
    }
    
    public function testInchSizeGetInchName(){
        $size = new InchTyreSize("36x12.5-15");
        $this->assertEquals("36x12.5-15",$size->getInchName());
    }
    
    public function testIsInchSizeProcomp() {
        
        $this->assertTrue(InchTyreSize::checkSize("38.50X14.50R18"));
    }
    
    
    
    public function testIsMetircSizePositive() {
        $this->assertTrue(MetricTyreSize::checkSize("315/70R17"));
    }

    public function testIsMetircSizeNegative() {
        $this->assertFalse(MetricTyreSize::checkSize("35x12.5R15"));
    }

    
    public function testIsMetircSizeEmptyStringNegative() {
        $this->assertFalse(MetricTyreSize::checkSize(""));
    }
    
    public function testMetircSizeConstructor() {
        $this->checkSizesList($this->metric, 'MetricTyreSize');
    }

    public function testMetircSizeGetRadialCord(){
        $size = new MetricTyreSize("215/75R15");
        $this->assertEquals(TyreSize::CORD_RADIAL,$size->getCord());
    }
    
    public function testMetircSizeGetInchName(){
        $size = new MetricTyreSize("305/65R17");
        $this->assertEquals('32.5x12R17',$size->getInchName());
    }

    
    
    public function testMetircSizeGetDiagonalCord(){
        $size = new MetricTyreSize("285/75-16");
        $this->assertEquals(TyreSize::CORD_DIAGONAL,$size->getCord());
    }
    

    public function testIsPneumoSize() {
        $this->assertTrue(PneumoTyreSize::checkSize("1020х420-18"));
    }

    public function testPneumoSizeConstructor() {
        $this->checkSizesList($this->pneumo, 'PneumoTyreSize');
    }

    public function testPneumoGetCord(){
        $this->setExpectedException('Exception');
        $size = new PneumoTyreSize("1020х420-18");
        //try{
        
            $size->getCord();
//            $this->assertTrue(false);
//        } catch (Exception $ex) {
//            $this->assertTrue(true);
//        }                
    }
    
    public function testIsFullProfileSizePodsitive() {
        $this->assertTrue(FullProfileTyreSize::checkSize("195R16"));
    }
    
    public function testIsFullProfileSizeNegative() {
        $this->assertFalse(FullProfileTyreSize::checkSize("195/80R16"));
    }
    
    public function testFullProfileSizeConstructor() {
        $this->checkSizesList($this->fullsize, 'FullProfileTyreSize');
    }
    
    public function testIsFullProfileInchSizePositive() {
        $this->assertTrue(FullProfileInchTyreSize::checkSize("7.50-16LT"));
    }
    
    
    public function testIsFullProfileInchSizeNegative() {
        $this->assertFalse(FullProfileInchTyreSize::checkSize("195R16"));
    }
    
    public function testFullProfileInchSizeConstructor() {
        $this->checkSizesList($this->inchfullsize, 'FullProfileInchTyreSize');
    }

    

    public function testFullProfileGetRadialCord(){
        $size = new FullProfileTyreSize("205R16");
        $this->assertEquals(TyreSize::CORD_RADIAL,$size->getCord());
    }
    
    public function testIsAmericanSizePositive() {
        $this->assertTrue(AmericanTyreSize::checkSize("Q78-15"));
    }
    
    public function testFullAmericanSizeConstructor() {
        $this->checkSizesList($this->fullsize, 'FullProfileTyreSize');
    }
    
    public function testFullProfileGetDiagonalCord(){
        $size = new AmericanTyreSize("Q78-15");
        $this->assertEquals(TyreSize::CORD_DIAGONAL,$size->getCord());
    }
    
    public function testIsReverseInchSizePositive() {
        $this->assertTrue(ReverseInchTyreSize::checkSize("	18/39.5-15"));
        $this->assertTrue(ReverseInchTyreSize::checkSize("19.5/44 -16.5LT"));
        $this->assertTrue(ReverseInchTyreSize::checkSize("15/38.5-16.5LT"));
     
    }
    
    public function testIsReverseInchSizeLowWidthPositive() {
        $this->assertTrue(ReverseInchTyreSize::checkSize("	9/34-16LT")); // Narrow ss
    }
    
    public function testIsReverseInchSizeNegative(){
        $this->assertFalse(ReverseInchTyreSize::checkSize("185/65-15"));
        $this->assertFalse(ReverseInchTyreSize::checkSize("35/12.5R16"));
        
    }
    
    public function testReverseInchSizeConstructor() {
        $this->checkSizesList($this->reverse, 'ReverseInchTyreSize');
    }
    
     public function testReverseInchSizeGetDiagonalCord(){
        $size = new ReverseInchTyreSize("19.5/44 -16.5LT");
        $this->assertEquals(TyreSize::CORD_DIAGONAL,$size->getCord());
    }
    
    /*
    public function testExtractDiskInch() {
        $disk = TyreSize::extractDisk("36x12.5-16");
        $this->assertEquals(16,$disk);
    }
    
    
    public function testExtractDiskMetric() {
        $disk = TyreSize::extractDisk("255/60R22");
        $this->assertEquals(22,$disk);
    }

    public function testExtractDiskHummer() {
        $disk = TyreSize::extractDisk("35x12,5R16.5");
        $this->assertEquals(16.5,$disk);
    }

    public function testExtractDiskPneumo() {
        $disk = TyreSize::extractDisk("1020х420-18");
        $this->assertEquals(18,$disk);
    }

    public function testIsPneumoSize() {
        $this->assertTrue(TyreSize::isPneumoSize("1020х420-18"));
    }
*/
    
    public function testFactoryMetric() {
        $size = TyreSize::parseSize('215/75R15');
        $this->assertTrue(is_object($size));
        $this->assertEquals(__NAMESPACE__.'\\'.'MetricTyreSize',get_class($size));
    }

    public function testFactoryInch() {
        $size = TyreSize::parseSize('35/12.50R15LT');
        $this->assertEquals(__NAMESPACE__.'\\'.'InchTyreSize',get_class($size));
    }
    
    public function testFactoryPneumo() {
        $size = TyreSize::parseSize("1020х420-22.5");
        $this->assertEquals(__NAMESPACE__.'\\'.'PneumoTyreSize',get_class($size));
    }

    
    public function testFactoryFullSize() {
        $size = TyreSize::parseSize("195R16");
        $this->assertEquals(__NAMESPACE__.'\\'.'FullProfileTyreSize',get_class($size));
    }

    public function testFactoryAmerican() {
        $size = TyreSize::parseSize('Q78-16');
        $this->assertEquals(__NAMESPACE__.'\\'.'AmericanTyreSize',get_class($size));
    }
    
    public function testFactoryReverseInch() {
        $size = TyreSize::parseSize('15/38.5-16.5');
        $this->assertEquals(__NAMESPACE__.'\\'.'ReverseInchTyreSize',get_class($size));
    }
    
    public function testConstructorNegative() {
        $this->setExpectedException('Exception');        
        $size = TyreSize::parseSize('blablabla');
    }
    
    
    public function testCreatingInchSize(){
        
    }
    
    
}